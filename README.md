# SMM Hub

Основное backend-приложение

### Сборка
`mvn package`

Результатом выполнения будет являться zip-файл с дистрибутивом в корневой папке проекта

## Запуск
В Unix среде: `sh start.sh`

Веб интерфейс запускается на порту 9000, если не укзано иное

## Запуск и дебаг
VM Options =  -classpath "./web/target/classes;./starter/target/classes;./datasources/target/classes;./scheduler/target/classes;./database/target/classes;./common/target/classes;./services/target/classes;E:/Dev/smmhub/dev/lib/*;C:/Program Files (x86)/JetBrains/IntelliJ IDEA 2016.3/lib/idea_rt.jar;"
binary_roles = 34 - admin + regular user