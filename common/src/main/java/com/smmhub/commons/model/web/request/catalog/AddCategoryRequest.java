package com.smmhub.commons.model.web.request.catalog;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author nix (05.03.2017)
 */

@NoArgsConstructor
@Getter
@Setter
public class AddCategoryRequest {

	@NotNull
	private String name;

	private Long parentid;
}
