package com.smmhub.commons.model.web.response;

import com.smmhub.commons.enums.BackendAPIResponseCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BackendResponse {

	private int code = BackendAPIResponseCode.OK.getCode();
	private Object data;
	private Long time;
	private String text;

	private BackendResponse(String text) {
		this.text = text;
	}

	private BackendResponse(Object data) {
		this.data = data;
	}

	private BackendResponse(BackendAPIResponseCode code, String text) {
		this.code = code.ordinal();
		this.text = text;
	}

	private BackendResponse(BackendAPIResponseCode code, String text, Object data) {
		this.code = code.getCode();
		this.text = text;
		this.data = data;
	}

	public static BackendResponse of(String text) {
		return new BackendResponse(text);
	}

	public static BackendResponse of(Object data) {
		return new BackendResponse(data);
	}

	public static BackendResponse of(BackendAPIResponseCode code, String text) {
		return new BackendResponse(code, text);
	}

	public static BackendResponse of(BackendAPIResponseCode code, String text, Object data) {
		return new BackendResponse(code, text, data);
	}
}
