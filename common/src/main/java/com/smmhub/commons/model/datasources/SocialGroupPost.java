package com.smmhub.commons.model.datasources;

import com.smmhub.commons.enums.SocialNetworkType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.List;

/**
 * @author nix (27.04.2017)
 */

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class SocialGroupPost {

	private String id;
	private String fromId;
	private String groupId;

	// formId - это ID группы?
	private boolean isPostedFromGroup = true;

	private Date date;

	private String text;

	private Integer likes;
	private Integer reposts;
	private Integer comments;

	private List<PostAttachment> attachments;

	private SocialNetworkType networkType;
	private Object wrappedClass;

	private boolean pinned = false;

	public static enum AttachmentType {
		PHOTO,
		VIDEO,
		AUDIO,
		URL,
		POLL,
		LINK
	}

	@Getter
	@Setter
	public static class PostAttachment {
		private AttachmentType attachmentType;
		private Object attach;
	}

}
