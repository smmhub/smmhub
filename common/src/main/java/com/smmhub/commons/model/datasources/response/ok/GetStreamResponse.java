package com.smmhub.commons.model.datasources.response.ok;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author nix (28.04.2017)
 */

@NoArgsConstructor
@Getter
@Setter
public class GetStreamResponse extends OkPaginationResponse {

	private Entity entities;

	@NoArgsConstructor
	@Getter
	@Setter
	public static class Entity {

		private GroupPhoto[] group_photos;
		private MediaTopic[] media_topics;

		@NoArgsConstructor
		@Getter
		@Setter
		public static class GroupPhoto {
			private String pic_max;
			private String picgif;
			private String ref;
		}

		@NoArgsConstructor
		@Getter
		@Setter
		public static class MediaTopic {
			private Media[] media;
			private String id;
			private Long created_ms;
			private DiscussionSummary discussion_summary;
			private LikeSummary like_summary;
			private ReshareSummary reshare_summary;

			@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
			@JsonSubTypes({
					@JsonSubTypes.Type(name = "text", value = MediaText.class),
					@JsonSubTypes.Type(name = "photo", value = MediaPhoto.class),
					@JsonSubTypes.Type(name = "movie", value = MediaMovie.class),
					@JsonSubTypes.Type(name = "poll", value = MediaPoll.class),
					@JsonSubTypes.Type(name = "link", value = MediaLink.class),
					@JsonSubTypes.Type(name = "music", value = MediaMusic.class)
			})
			public static abstract class Media {
			}

			@NoArgsConstructor
			@Getter
			@Setter
			public static class MediaText extends Media {
				private String text;
			}

			@NoArgsConstructor
			@Getter
			@Setter
			public static class MediaPhoto extends Media {
				private String[] photo_refs;
			}

			@NoArgsConstructor
			@Getter
			@Setter
			public static class MediaMovie extends Media {
				//todo
			}

			@NoArgsConstructor
			@Getter
			@Setter
			public static class MediaMusic extends Media {
				//todo
			}

			@NoArgsConstructor
			@Getter
			@Setter
			public static class MediaPoll extends Media {
				// todo
			}

			@NoArgsConstructor
			@Getter
			@Setter
			public static class MediaLink extends Media {
				// todo
			}

			@NoArgsConstructor
			@Getter
			@Setter
			public static class DiscussionSummary {
				private Integer comments_count;
			}

			@NoArgsConstructor
			@Getter
			@Setter
			public static class LikeSummary {
				private Integer count;
			}

			@NoArgsConstructor
			@Getter
			@Setter
			public static class ReshareSummary {
				private Integer count;
			}
		}
	}
}
