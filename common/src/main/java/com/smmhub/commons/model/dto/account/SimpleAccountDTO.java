package com.smmhub.commons.model.dto.account;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author nix (28.03.2017)
 */

@Getter
@Setter
@NoArgsConstructor
public class SimpleAccountDTO extends AccountDTO {
	private static final long serialVersionUID = 8619495002377372411L;
	private boolean emailConfirmed;
}
