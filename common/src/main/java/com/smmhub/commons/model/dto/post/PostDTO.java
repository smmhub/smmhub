package com.smmhub.commons.model.dto.post;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.smmhub.commons.enums.Language;
import com.smmhub.commons.enums.SocialNetworkType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Nikolay Viguro, 19.09.17
 */

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class PostDTO implements Serializable {

    private static final long serialVersionUID = -9055627009463658524L;

    private String id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date posted;

    private SocialNetworkType socialNetworkType;
    private String text;

    private Language language;

    private Set<Long> categoryIds;

    private String groupPostId;
    private String groupId;
    private String groupName;

    private Integer likes;
    private Integer reposts;
    private Integer comments;

    private Set<String> images = new HashSet<>();

    // признак отмодерированности
    private Boolean moderated = false;

    // признак того, что байесов фильтр пометил его, как возможный рекламный пост
    private Boolean filtered = false;
}
