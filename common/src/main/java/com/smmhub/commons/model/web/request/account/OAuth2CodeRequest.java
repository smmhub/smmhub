package com.smmhub.commons.model.web.request.account;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author nix (04.05.2017)
 */
@NoArgsConstructor
@Getter
@Setter
public class OAuth2CodeRequest {

	@NotNull
	private String code;
}
