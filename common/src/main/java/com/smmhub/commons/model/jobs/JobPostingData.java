package com.smmhub.commons.model.jobs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Nikolay Viguro, 13.11.17
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JobPostingData {
    private String userId; // id for getting from account dto cache
    private Long jobId; // database id of executing job
    private String nextPostId; // next post
}
