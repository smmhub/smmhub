package com.smmhub.commons.model.datasources.response.ok;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author nix (28.04.2017)
 */

@NoArgsConstructor
@Getter
@Setter
public class ProfileInfoResponse {
	private String uid;
	private String name;
	private String gender;
	private String pic50x50;
	private String birthday;
	private String email;
}
