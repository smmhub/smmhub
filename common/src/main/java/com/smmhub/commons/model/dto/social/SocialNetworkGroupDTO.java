package com.smmhub.commons.model.dto.social;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.smmhub.commons.model.dto.job.JobDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * @author Nikolay Viguro at 06.03.17
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SocialNetworkGroupDTO implements Serializable {

	private static final long serialVersionUID = 8050905464302601969L;
	private String socialGroupId;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createdDate;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date lastPostedAt;

	private String name;
	private String logo;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date tokenExpired;

	// тут будет храниться расписание, когда надо постить в группу
	private Set<JobDTO> jobs;

	private Boolean enabled;
	private Boolean banned;
}
