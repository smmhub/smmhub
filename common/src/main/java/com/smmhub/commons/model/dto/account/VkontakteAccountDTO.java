package com.smmhub.commons.model.dto.account;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author nix (28.03.2017)
 */

@Getter
@Setter
@NoArgsConstructor
public class VkontakteAccountDTO extends AccountDTO {
	private static final long serialVersionUID = 3964081623019384041L;

	@JsonIgnore
	private Integer userId;

	@JsonIgnore
	private String token;
}
