package com.smmhub.commons.model.datasources.response.ok;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Nikolay Viguro <nikolay.viguro@loyaltyplant.com>, 02.05.17
 */

@Getter
@Setter
public class OkPaginationResponse {
    protected String anchor;
}
