package com.smmhub.commons.model.dto.category;

import lombok.*;

import java.io.Serializable;

/**
 * @author Nikolay Viguro at 06.03.17
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class CatalogCategoryWithoutChildsDTO implements Serializable {
	private static final long serialVersionUID = 5266971365649162658L;
	private Long id;
	private String name;
}
