package com.smmhub.commons.model.datasources;

import com.smmhub.commons.enums.SocialNetworkType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author nix (27.04.2017)
 */

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class SocialGroup {

	private String id;
	private String name;
	private String photo;
	private String description;

	private boolean verified = false;

	private String country;
	private String city;

	private Integer membersCount;

	private SocialNetworkType networkType;
	private Object wrappedClass;
}
