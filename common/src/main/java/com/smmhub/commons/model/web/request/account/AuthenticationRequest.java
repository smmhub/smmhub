package com.smmhub.commons.model.web.request.account;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;

/**
 * @author nix (25.02.2017)
 */
@NoArgsConstructor
@Getter
@Setter
public class AuthenticationRequest {

	@NotNull
	@Email
	private String email;
	@NotNull
	private String password;
}
