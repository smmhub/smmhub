package com.smmhub.commons.model.web.request.posts;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * @author nix (08.03.2017)
 */

@NoArgsConstructor
@Getter
@Setter
public class GetPostsRequest {
	@NotNull
	@Size(min = 1)
	private Set<Long> categoryIds;
	private Integer page = 1;
	private Integer postsInPage = 25;
}
