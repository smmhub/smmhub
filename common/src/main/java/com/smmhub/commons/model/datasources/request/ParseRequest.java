package com.smmhub.commons.model.datasources.request;

import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.commons.model.dto.category.CatalogGroupDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author nix (04.03.2017)
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ParseRequest {
	private SocialNetworkType network;
	private List<CatalogGroupDTO> groups;
}
