package com.smmhub.commons.model.datasources.response.ok;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author nix (17.05.2017)
 */

@NoArgsConstructor
@Getter
@Setter
public class UploadUrlResponse {
	private Long expires_ms;
	private String[] photo_ids;
	private String upload_url;
}
