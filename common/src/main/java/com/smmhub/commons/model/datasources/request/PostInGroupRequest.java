package com.smmhub.commons.model.datasources.request;

import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.commons.model.jobs.JobPostingData;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author nix (13.11.2017)
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PostInGroupRequest {
	private SocialNetworkType network;
	private JobPostingData jobPostingData;
}
