package com.smmhub.commons.model.machinelearning;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Nikolay Viguro, 19.09.17
 */

@Setter
@Getter
@AllArgsConstructor
@ToString
public class ClassifyResult {
    private double ham;
    private double spam;
    private boolean spamDetected = false;
}
