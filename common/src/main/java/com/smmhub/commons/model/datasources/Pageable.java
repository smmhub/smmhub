package com.smmhub.commons.model.datasources;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Collection;

/**
 * @author nix (02.05.2017)
 */

@Getter
@AllArgsConstructor
public class Pageable<T> {
	private final Collection<T> data;
	private final Object anchor;
	private final boolean hasMore;
}
