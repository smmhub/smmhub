package com.smmhub.commons.model.dto.account;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smmhub.commons.enums.AccountLoginType;
import com.smmhub.commons.enums.AccountType;
import com.smmhub.commons.model.dto.social.SocialNetworkDTO;
import com.smmhub.commons.serialization.MoneySerialize;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.EnumSet;
import java.util.Set;

/**
 * @author Nikolay Viguro at 13.09.17
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class AccountDTO implements Serializable, UserDetails {
	private static final long serialVersionUID = -7134899955728477694L;
	protected String email;
	protected String displayName;
	protected AccountLoginType loginType;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	protected Date regDate;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	protected Date lastPasswordReset;

	protected String photoUrl;
	protected String birthday;

	@JsonSerialize(using = MoneySerialize.class)
	protected BigDecimal balance = BigDecimal.ZERO;

	protected Set<SocialNetworkDTO> socialNetworks;

	// Spring security stuff below this line
	@JsonIgnore
	protected String password;

	@JsonIgnore
	private Boolean enabled = true;

	@JsonIgnore
	private Long binaryRoles = 0L;

	@JsonIgnore
	private Set<AccountType> roles;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		roles = EnumSet.noneOf(AccountType.class);
		for (AccountType role : AccountType.values())
			if ((binaryRoles & (1 << role.ordinal())) != 0)
				roles.add(role);
		return roles;
	}

	@Override
	@JsonIgnore
	public String getPassword() {
		return "{bcrypt}" + password;
	}

	@Override
	public String getUsername() {
		return email + "-" + loginType.name();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}
}
