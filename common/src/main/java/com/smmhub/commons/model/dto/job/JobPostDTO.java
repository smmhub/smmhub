package com.smmhub.commons.model.dto.job;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.smmhub.commons.enums.JobPostingResultStatus;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Nikolay Viguro, 14.12.17
 */

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class JobPostDTO implements Serializable {
	private static final long serialVersionUID = 5254514407021368398L;
    private Long id;
    private Date posted;
    private String postId;
	private JobPostingResultStatus status;
	private String extendedInfo;
}
