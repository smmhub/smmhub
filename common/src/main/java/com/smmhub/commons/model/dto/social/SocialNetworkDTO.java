package com.smmhub.commons.model.dto.social;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.smmhub.commons.enums.SocialNetworkType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * @author Nikolay Viguro at 06.03.17
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SocialNetworkDTO implements Serializable {
	private static final long serialVersionUID = 8790100478072412894L;
	private SocialNetworkType socialNetworkType;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createdDate;

	private String userId;
	private String token;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date tokenExpired;

	private Set<SocialNetworkGroupDTO> socialNetworkGroups;

}
