package com.smmhub.commons.model.dto.category;

import com.smmhub.commons.enums.Language;
import com.smmhub.commons.enums.SocialNetworkType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author nix (24.02.2017)
 */

@NoArgsConstructor
@Getter
@Setter
public class CatalogGroupDTO implements Serializable {
	private static final long serialVersionUID = 7473354507434576578L;
	private Long id;
	private CatalogCategoryDTO catalogCategory;
	private SocialNetworkType socialNetworkType;
	private Date date;
	private String name;
	private String groupId;
	private Language language;
	private Date lastParsedPostDate;
	private boolean enabled = true;
}
