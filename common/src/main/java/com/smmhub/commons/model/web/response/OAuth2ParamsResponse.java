package com.smmhub.commons.model.web.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author nix (25.02.2017)
 */
@AllArgsConstructor
@Getter
@Setter
public class OAuth2ParamsResponse {
	@NotNull
	private Object clientId;
	@NotNull
	private String callbackUrl;
}
