package com.smmhub.commons.model.web.request.posts;

import com.smmhub.commons.enums.MachineLearningResult;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author nix (08.03.2017)
 */

@NoArgsConstructor
@Getter
@Setter
public class TrainMachineLearningRequest {
	@NotNull
	private MachineLearningResult type;

	@NotNull
	private String esPostId;
}
