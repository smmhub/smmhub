package com.smmhub.commons.model.datasources.response;

import com.smmhub.commons.enums.ParseStatus;
import com.smmhub.commons.enums.SocialNetworkType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

/**
 * @author nix (04.03.2017)
 */

@NoArgsConstructor
@Getter
@Setter
public class ParseResponse {

	private SocialNetworkType network;
	private Map<String, GroupParseStatus> statuses;

	@NoArgsConstructor
	@Getter
	@Setter
	public static class GroupParseStatus {
		private ParseStatus status;
		private String errorMessage;
	}
}
