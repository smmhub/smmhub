package com.smmhub.commons.model.web.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author nix (25.02.2017)
 */
@AllArgsConstructor
@Getter
@Setter
public class TokenResponse {
	@NotNull
	private String token;
}
