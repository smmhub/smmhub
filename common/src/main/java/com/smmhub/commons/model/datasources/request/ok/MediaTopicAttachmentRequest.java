package com.smmhub.commons.model.datasources.request.ok;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nikolay Viguro, 17.05.17
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MediaTopicAttachmentRequest {
    private List<Object> media = new ArrayList<>();

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Photo {
        private String type = "photo";
        private List<PhotoId> list = new ArrayList<>();

        @Getter
        @Setter
        @NoArgsConstructor
        @AllArgsConstructor
        public static class PhotoId {
            private String id;
        }
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Text {
        private String type = "text";
        private String text;
    }
}
