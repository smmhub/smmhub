package com.smmhub.commons.model.web.request.account;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author nix (25.02.2017)
 */
@NoArgsConstructor
@Getter
@Setter
public class SignupRequest {

	@NotNull
	@Email
	private String email;

	@NotNull
	@Size(min = 5, max = 60)
	private String password;

	@NotNull
	@Size(min = 3, max = 60)
	private String displayName;

	@NotNull
	@Size(min = 10)
	private String recaptchaResponse;
}
