package com.smmhub.commons.model.datasources;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author nix (02.05.2017)
 */


public class Pagination {

	private static final int DEFAULT_COUNT = 50;
	@Getter
	private final Direction direction;
	@Getter
	private final int count;
	@Getter
	private String anchor;
	@Getter
	private int offset;

	public Pagination() {
		this(0, Direction.FORWARD, DEFAULT_COUNT);
	}

	public Pagination(String anchor, int count) {
		this(anchor, Direction.FORWARD, count);
	}

	public Pagination(Integer offset, int count) {
		this(offset, Direction.FORWARD, count);
	}

	public Pagination(String anchor) {
		this(anchor, Direction.FORWARD, DEFAULT_COUNT);
	}

	public Pagination(int count) {
		this(0, Direction.FORWARD, DEFAULT_COUNT);
	}

	public Pagination(String anchor, Direction direction, int count) {
		if (direction == null) {
			throw new IllegalArgumentException("direction is null");
		}
		if (count <= 0) {
			throw new IllegalArgumentException("count is not positive [" + count + "]");
		}

		this.anchor = anchor;
		this.direction = direction;
		this.count = count;
	}

	public Pagination(int offset, Direction direction, int count) {
		if (direction == null) {
			throw new IllegalArgumentException("direction is null");
		}
		if (count <= 0) {
			throw new IllegalArgumentException("count is not positive [" + count + "]");
		}
		if (offset < 0) {
			throw new IllegalArgumentException("offset is not positive [" + count + "]");
		}

		this.offset = offset;
		this.direction = direction;
		this.count = count;
	}

	public Map<String, String> asParamsMap() {
		Map<String, String> params = new HashMap<>();

		if (anchor != null) {
			params.put("anchor", anchor);
		}
		params.put("direction", direction.toString());
		params.put("count", Integer.toString(count));

		return params;
	}

	public String asQueryString() {
		String ret = "&direction=" + direction.toString() + "&count=" + count;

		if (anchor != null) {
			ret = ret.concat("&anchor=" + anchor);
		}

		return ret;
	}

	public enum Direction {
		FORWARD,
		BACKWARD,
		AROUND
	}
}