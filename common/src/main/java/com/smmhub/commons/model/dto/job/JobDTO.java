package com.smmhub.commons.model.dto.job;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.smmhub.commons.model.dto.category.CatalogCategoryDTO;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * @author Nikolay Viguro, 13.11.17
 */

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
public class JobDTO implements Serializable {
    private static final long serialVersionUID = 4021206531193474225L;
    private Long id;
    private Date created;
    private Date updated;
    private String title;
    private String description;
    private Class klass;
    private String data;
    private String cron;
    private Date oneTimeRun;
    private Boolean enabled = true;
    private Boolean finished = false;
    private Integer retries = 0;

    @JsonIgnore
    private Set<JobPostDTO> jobPosts;

    private Set<CatalogCategoryDTO> categories;
}
