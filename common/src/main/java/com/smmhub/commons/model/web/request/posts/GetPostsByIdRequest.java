package com.smmhub.commons.model.web.request.posts;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * @author nix (28.12.2017)
 */

@NoArgsConstructor
@Getter
@Setter
public class GetPostsByIdRequest {
    @NotNull
    private Set<String> postIds;
    private Integer page = 1;
    private Integer postsInPage = 25;
}
