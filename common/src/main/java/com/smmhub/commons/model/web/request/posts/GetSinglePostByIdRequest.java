package com.smmhub.commons.model.web.request.posts;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author nix (28.12.2017)
 */

@NoArgsConstructor
@Getter
@Setter
public class GetSinglePostByIdRequest {
    @NotNull
    private String postId;
}
