package com.smmhub.commons.model.web.request.social;

import com.smmhub.commons.model.dto.job.JobDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author nix (15.11.2017)
 */

@NoArgsConstructor
@Getter
@Setter
public class UpdateJobRequest {

	@NotNull
	private String groupid;

	@NotNull
	private JobDTO job;
}
