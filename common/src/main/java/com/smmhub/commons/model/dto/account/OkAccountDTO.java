package com.smmhub.commons.model.dto.account;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author nix (13.09.2017)
 */

@Getter
@Setter
@NoArgsConstructor
public class OkAccountDTO extends AccountDTO {
	private static final long serialVersionUID = -5436689323654203906L;

	@JsonIgnore
	private Long userId;

	@JsonIgnore
	private String token;
}
