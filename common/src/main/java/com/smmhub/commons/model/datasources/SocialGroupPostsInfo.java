package com.smmhub.commons.model.datasources;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @author nix (27.04.2017)
 */

@Getter
@Setter
@ToString
public class SocialGroupPostsInfo {
	private Integer totalPosts;
	private List<SocialGroupPost> groupPosts;
}
