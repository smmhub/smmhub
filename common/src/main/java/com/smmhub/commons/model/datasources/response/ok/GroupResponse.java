package com.smmhub.commons.model.datasources.response.ok;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author nix (28.04.2017)
 */

@NoArgsConstructor
@Getter
@Setter
public class GroupResponse {
	private String uid;
	private String name;
	private String description;
	private String picAvatar;
	private Attrs attrs;
	private Integer members_count;
	private String access_type;
	private String country;
	private String city;

	@NoArgsConstructor
	@Getter
	@Setter
	public static class Attrs {
		private String flags;
	}
}
