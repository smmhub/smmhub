package com.smmhub.commons.model.datasources;

import com.smmhub.commons.enums.SocialNetworkType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author nix (27.04.2017)
 */

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class SocialProfile {

	private String userId;
	private String accessToken;
	private boolean male = true;
	private String birthday;
	private String displayName;
	private String photoUrl;
	private String email;

	private SocialNetworkType networkType;
	private Object wrappedClass;
}
