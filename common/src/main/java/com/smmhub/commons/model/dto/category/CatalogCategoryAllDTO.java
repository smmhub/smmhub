package com.smmhub.commons.model.dto.category;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Set;

/**
 * @author Nikolay Viguro at 06.03.17
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CatalogCategoryAllDTO implements Serializable {
	private static final long serialVersionUID = 5014254937813291626L;
	private Long id;
	private String name;
	private Boolean userPrivate;
	private Long accountId;
	private Set<CatalogCategoryAllDTO> childs;
}
