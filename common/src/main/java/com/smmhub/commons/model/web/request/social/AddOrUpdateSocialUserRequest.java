package com.smmhub.commons.model.web.request.social;

import com.smmhub.commons.enums.SocialNetworkType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author nix (25.02.2017)
 */
@NoArgsConstructor
@Getter
@Setter
public class AddOrUpdateSocialUserRequest {

	@NotNull
	private String userid;

	@NotNull
	private String accesstoken;

	@NotNull
	private SocialNetworkType socialnetwork;
}
