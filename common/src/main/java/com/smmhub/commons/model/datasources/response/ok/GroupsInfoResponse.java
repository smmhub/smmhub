package com.smmhub.commons.model.datasources.response.ok;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author nix (28.04.2017)
 */

@NoArgsConstructor
@Getter
@Setter
public class GroupsInfoResponse extends OkPaginationResponse {
	private List<Group> groups;

	@NoArgsConstructor
	@Getter
	@Setter
	public static class Group {
		private String groupId;
		private String userId;
	}
}
