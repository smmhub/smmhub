package com.smmhub.commons.consts;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Nikolay Viguro at 29.03.17
 */

@Configuration
@PropertySource("classpath:socialkeys.properties")
public class SocialCredentialsConsts {

	// VKontakte

	@Value("${vk.login.appid:0}")
	public Integer vkLoginAppID;

	@Value("${vk.login.appsecret:notset}")
	public String vkLoginAppSecret;

	@Value("${vk.login.callback:notset}")
	public String vkLoginCallback;

	@Value("${vk.data.appid:0}")
	public Integer vkDataAppID;

	@Value("${vk.data.appsecret:notset}")
	public String vkDataAppSecret;

	@Value("${vk.data.callback:notset}")
	public String vkDataCallback;

	@Value("${vk.test.user:notset}")
	public String vkTestUserId;

	@Value("${vk.test.accesstoken:notset}")
	public String vkTestAccessToken;

	@Value("${vk.test.group:notset}")
	public String vkTestGroup;

	// OK

	@Value("${ok.login.appid:0}")
	public String okLoginAppID;

	@Value("${ok.login.pubkey:notset}")
	public String okLoginAppPubKey;

	@Value("${ok.login.appsecret:notset}")
	public String okLoginAppSecret;

	@Value("${ok.login.callback:notset}")
	public String okLoginCallback;

	@Value("${ok.data.appid:0}")
	public String okDataAppID;

	@Value("${ok.data.pubkey:notset}")
	public String okDataAppPubKey;

	@Value("${ok.data.appsecret:notset}")
	public String okDataAppSecret;

	@Value("${ok.data.callback:notset}")
	public String okDataCallback;

	@Value("${ok.test.accesstoken:notset}")
	public String okTestAccessToken;

	@Value("${ok.test.secretkey:notset}")
	public String okTestSecretKey;

	@Value("${ok.test.group:notset}")
	public String okTestGroup;
}
