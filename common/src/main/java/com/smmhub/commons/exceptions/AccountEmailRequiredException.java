package com.smmhub.commons.exceptions;

/**
 * @author nix (24.02.2017)
 */
public class AccountEmailRequiredException extends Exception {
	public AccountEmailRequiredException() {
		super("Account email required!");
	}
}
