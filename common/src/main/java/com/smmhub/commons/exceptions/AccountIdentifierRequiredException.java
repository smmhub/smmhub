package com.smmhub.commons.exceptions;

/**
 * @author nix (24.02.2017)
 */
public class AccountIdentifierRequiredException extends Exception {
	public AccountIdentifierRequiredException() {
		super("Account id required!");
	}
}
