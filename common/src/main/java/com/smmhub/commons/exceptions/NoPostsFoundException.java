package com.smmhub.commons.exceptions;

/**
 * @author nix (23.12.2017)
 */

public class NoPostsFoundException extends Exception {
	public NoPostsFoundException(String message) {
		super(message);
	}
}
