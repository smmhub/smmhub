package com.smmhub.commons.exceptions;

/**
 * @author nix (24.02.2017)
 */
public class APIException extends Exception {
	public APIException(String message) {
		super(message);
	}
}
