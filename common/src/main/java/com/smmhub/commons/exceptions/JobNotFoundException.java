package com.smmhub.commons.exceptions;

/**
 * @author nix (25.05.2017)
 */
public class JobNotFoundException extends Exception {
	public JobNotFoundException() {
		super("Job not found!");
	}
}
