package com.smmhub.commons.exceptions;

/**
 * @author nix (24.02.2017)
 */
public class APIRequestException extends Exception {
	public APIRequestException(String message) {
		super(message);
	}
}
