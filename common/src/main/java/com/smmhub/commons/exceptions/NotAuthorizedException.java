package com.smmhub.commons.exceptions;

/**
 * @author nix (25.02.2017)
 */
public class NotAuthorizedException extends Exception {
	public NotAuthorizedException() {
		super("Not authorized!");
	}
}
