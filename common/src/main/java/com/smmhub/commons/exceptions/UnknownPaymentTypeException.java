package com.smmhub.commons.exceptions;

/**
 * @author nix (24.02.2017)
 */
public class UnknownPaymentTypeException extends Exception {
	public UnknownPaymentTypeException() {
		super("Unknown payment type!");
	}
}
