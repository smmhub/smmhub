package com.smmhub.commons.exceptions;

/**
 * @author nix (24.02.2017)
 */
public class AccountNotFoundException extends Exception {
	public AccountNotFoundException() {
		super("Account not found!");
	}
}
