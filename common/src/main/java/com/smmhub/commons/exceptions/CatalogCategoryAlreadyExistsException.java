package com.smmhub.commons.exceptions;

/**
 * @author nix (24.02.2017)
 */
public class CatalogCategoryAlreadyExistsException extends Exception {
	public CatalogCategoryAlreadyExistsException() {
		super("Catalog category already exists!");
	}
}
