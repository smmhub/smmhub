package com.smmhub.commons.exceptions;

/**
 * @author nix (24.02.2017)
 */
public class CatalogCategoryNotFoundException extends Exception {
	public CatalogCategoryNotFoundException() {
		super("Catalog category not found!");
	}
}
