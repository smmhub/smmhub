package com.smmhub.commons.exceptions;

/**
 * @author nix (24.02.2017)
 */
public class AccountAlreadyExistsException extends Exception {
	public AccountAlreadyExistsException() {
		super("Account already exists!");
	}
}
