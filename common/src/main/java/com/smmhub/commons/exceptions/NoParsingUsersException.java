package com.smmhub.commons.exceptions;

/**
 * @author nix (25.02.2017)
 */
public class NoParsingUsersException extends Exception {
	public NoParsingUsersException() {
		super("Parsing users for this social network not found");
	}
}
