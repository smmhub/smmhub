package com.smmhub.commons.exceptions.posting;

import com.smmhub.commons.exceptions.APIRequestException;

/**
 * @author nix (23.12.2017)
 */

public class EmptyPostingDataException extends APIRequestException {
	public EmptyPostingDataException(String message) {
		super(message);
	}
}
