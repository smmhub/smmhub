package com.smmhub.commons.exceptions;

/**
 * @author nix (24.02.2017)
 */
public class NotRootCatalogCategoryException extends Exception {
	public NotRootCatalogCategoryException() {
		super("Catalog category root has parent. This is not root!");
	}
}
