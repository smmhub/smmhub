package com.smmhub.commons.exceptions;

/**
 * @author nix (24.02.2017)
 */
public class AccountPasswordRequiredException extends Exception {
	public AccountPasswordRequiredException() {
		super("Account email required!");
	}
}
