package com.smmhub.commons.exceptions;

/**
 * @author nix (24.02.2017)
 */
public class InsufficientMoneyException extends Exception {
	public InsufficientMoneyException() {
		super("Insufficient money!");
	}
}
