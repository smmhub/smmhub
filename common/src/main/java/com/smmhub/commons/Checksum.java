package com.smmhub.commons;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author nix (09.03.2017)
 */
public class Checksum {

	public static String checksum(Object obj) throws IOException, NoSuchAlgorithmException {

		if (obj == null) {
			return "";
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(obj);
		oos.close();

		MessageDigest m = MessageDigest.getInstance("SHA1");
		m.update(baos.toByteArray());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1, digest);
		StringBuilder hashtext = new StringBuilder(bigInt.toString(16));

		while (hashtext.length() < 32) {
			hashtext.insert(0, "0");
		}

		return hashtext.toString();
	}

}
