package com.smmhub.commons.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author nix (24.02.2017)
 */

public abstract class AbstractRunningService implements RunningService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	@PostConstruct
	public abstract void onStartup() throws InterruptedException;

	@Override
	@PreDestroy
	public abstract void onShutdown();

	@Override
	@PostConstruct
	public abstract void subscribe() throws Exception;

	@Override
	@Async
	public abstract void run() throws Exception;
}
