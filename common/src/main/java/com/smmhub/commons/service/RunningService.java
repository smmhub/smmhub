package com.smmhub.commons.service;

/**
 * @author nix (24.02.2017)
 */

public interface RunningService {

	void onStartup() throws InterruptedException;

	void onShutdown();

	void subscribe() throws Exception;

	void run() throws Exception;

}
