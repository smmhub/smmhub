package com.smmhub.commons.cache.impl;

import com.smmhub.commons.cache.DistributedBackendCache;
import com.smmhub.commons.cache.config.CacheConfig;
import com.smmhub.commons.enums.SocialNetworkType;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

/**
 * @author nix (28.04.2017)
 */

@Component
public class ExecuteTimeCacheImpl implements DistributedBackendCache<SocialNetworkType, Long> {

	@Override
	@CacheEvict(cacheNames = CacheConfig.EXECUTE_TIME, beforeInvocation = true, key = "#key.name()")
	@CachePut(cacheNames = CacheConfig.EXECUTE_TIME, key = "#key.name()")
	public Long put(SocialNetworkType key, Long value) {
		return value;
	}

	@Override
	@Cacheable(value = CacheConfig.EXECUTE_TIME, sync = true, key = "#key.name()")
	public Long get(SocialNetworkType key) {
		return 0L;
	}

	@Override
	@CacheEvict(cacheNames = CacheConfig.EXECUTE_TIME, beforeInvocation = true, key = "#key.name()")
	public void evict(SocialNetworkType key) {
	}
}
