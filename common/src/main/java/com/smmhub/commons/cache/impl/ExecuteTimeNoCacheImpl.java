package com.smmhub.commons.cache.impl;

import com.smmhub.commons.cache.DistributedBackendCache;
import com.smmhub.commons.cache.config.CacheConfig;
import com.smmhub.commons.enums.SocialNetworkType;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author nix (28.04.2017)
 */

@Component
@Profile("nocache")
public class ExecuteTimeNoCacheImpl implements DistributedBackendCache<SocialNetworkType, Long> {

	// будет юзаться если выключен кеш
	private final Map<SocialNetworkType, Long> nocache = new HashMap<>();

	@Override
	@CacheEvict(cacheNames = CacheConfig.EXECUTE_TIME, beforeInvocation = true, key = "#key.name()")
	@CachePut(cacheNames = CacheConfig.EXECUTE_TIME, key = "#key.name()")
	public Long put(SocialNetworkType key, Long value) {
		return nocache.put(key, value);
	}

	@Override
	@Cacheable(value = CacheConfig.EXECUTE_TIME, sync = true, key = "#key.name()")
	public Long get(SocialNetworkType key) {
		return nocache.get(key) == null ? 0L : nocache.get(key);
	}

	@Override
	@CacheEvict(cacheNames = CacheConfig.EXECUTE_TIME, beforeInvocation = true, key = "#key.name()")
	public void evict(SocialNetworkType key) {
		nocache.remove(key);
	}
}
