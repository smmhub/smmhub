package com.smmhub.commons.cache.config;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

/**
 * @author nix (26.02.2017)
 */

@Configuration
@EnableCaching
public class CacheConfig {
	public static final String USER_IDS = "userIDs";
	public static final String ACCOUNTS = "accounts";
	public static final String API_LOCKS = "apilocks";
	public static final String EXECUTE_TIME = "executeapitime";
}
