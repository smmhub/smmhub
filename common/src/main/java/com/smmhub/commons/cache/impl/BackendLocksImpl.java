package com.smmhub.commons.cache.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.smmhub.commons.cache.DistributedBackendLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

import static com.smmhub.commons.cache.config.CacheConfig.API_LOCKS;

/**
 * @author nix (07.03.2017)
 */

@Component
public class BackendLocksImpl implements DistributedBackendLock<String> {

	@Autowired
	private HazelcastInstance instance;
	private IMap locks;

	@PostConstruct
	private void post() {
		locks = instance.getMap(API_LOCKS);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void lock(String key) {
		locks.put(key, "");
		locks.lock(key);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void lock(String key, Integer seconds) {
		locks.put(key, "");
		locks.lock(key, seconds, TimeUnit.SECONDS);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void lock(String key, Integer seconds, TimeUnit unit) {
		locks.put(key, "");
		locks.lock(key, seconds, unit);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void unlock(String key) {
		locks.unlock(key);
		locks.remove(key);
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean isLocked(String key) {
		if (locks.isLocked(key)) {
			return true;
		} else {
			if (locks.containsKey(key)) {
				return true;
			}
		}
		return false;
	}
}
