package com.smmhub.commons.cache.impl;

import com.smmhub.commons.cache.DistributedBackendLock;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author nix (28.04.2017)
 */

@Component
@Profile("nocache")
public class BackendLocksNoCacheImpl implements DistributedBackendLock<String> {

	private Map<String, ReentrantLock> locks = new HashMap<>();

	@Override
	public void lock(String key) {
		lock(key, null);
	}

	@Override
	public void lock(String key, Integer seconds) {
		lock(key, seconds, TimeUnit.SECONDS);
	}

	@Override
	public void lock(String key, Integer seconds, TimeUnit unit) {
		if(locks.get(key) == null) {
			ReentrantLock lock = locks.put(key, new ReentrantLock());
			lock.lock();
		}
		else {
			ReentrantLock lock = locks.get(key);
			lock.lock();
		}
	}

	@Override
	public void unlock(String key) {
		if(locks.get(key) != null) {
			ReentrantLock lock = locks.get(key);
			lock.unlock();
		}
	}

	@Override
	public boolean isLocked(String key) {
		ReentrantLock lock = locks.get(key);
		return lock.isLocked();
	}
}
