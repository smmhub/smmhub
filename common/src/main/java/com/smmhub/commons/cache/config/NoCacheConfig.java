package com.smmhub.commons.cache.config;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * @author nix (26.02.2017)
 */

@Configuration
@EnableCaching
@Profile("nocache")
public class NoCacheConfig {

	@Bean
	public CacheManager cacheManager() {
		return new NoOpCacheManager();
	}
}
