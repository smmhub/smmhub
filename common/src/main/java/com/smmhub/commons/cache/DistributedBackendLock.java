package com.smmhub.commons.cache;

import java.util.concurrent.TimeUnit;

/**
 * @author nix (28.04.2017)
 */
public interface DistributedBackendLock<K> {
	void lock(K key);
	void lock(K key, Integer seconds);
	void lock(String key, Integer seconds, TimeUnit unit);
	void unlock(K key);
	boolean isLocked(K key);
}
