package com.smmhub.commons.cache;

/**
 * @author nix (07.03.2017)
 */
public interface DistributedBackendCache<K, V> {
	V put(K key, V value);
	V get(K key);
	void evict(K key);
}
