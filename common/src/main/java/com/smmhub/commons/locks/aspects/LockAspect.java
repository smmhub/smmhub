package com.smmhub.commons.locks.aspects;

import com.smmhub.commons.cache.DistributedBackendLock;
import com.smmhub.commons.locks.CustomSPELParser;
import com.smmhub.commons.locks.LockableEntity;
import com.smmhub.commons.locks.annotations.Lock;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

/**
 * @author nix on 18.09.2017.
 */

@Aspect
@Component
@Slf4j
public class LockAspect {

	@Autowired
	private DistributedBackendLock<String> backendLocks;

	@Around(value = "execution(* *(..)) && @annotation(lock)")
	public Object logTask(ProceedingJoinPoint pjp, Lock lock) throws Throwable {

		String key = lock.value();

		if (StringUtils.hasText(key)) {
			MethodSignature signature = (MethodSignature) pjp.getSignature();

			try {
				key = CustomSPELParser.getDynamicValue(signature.getParameterNames(), pjp.getArgs(), key);
			}
			catch (Exception ex) {
				Method method = signature.getMethod();
				log.error("Error in SPEL key passed for locks service! Method: {}", method);
				return null;
			}
		} else {
			Method method = ((MethodSignature) pjp.getSignature()).getMethod();
			log.error("No key passed for locks service! Method: {}", method);
			return null;
		}

		if(!lock.type().equals(LockableEntity.NONE)) {
			key = lock.type() + "-" + key;
		}

		if(!lock.timeUnit().equals(TimeUnit.SECONDS)) {
			backendLocks.lock(key, lock.timeout(), lock.timeUnit());
		} else {
			backendLocks.lock(key, lock.timeout());
		}

		try {
			return pjp.proceed();
		} finally {
			backendLocks.unlock(key);
		}
	}
}