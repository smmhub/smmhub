package com.smmhub.commons.locks.annotations;

import com.smmhub.commons.locks.LockableEntity;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * @author nix (18.09.2017)
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Lock {
    String value() default "";
    int timeout() default 10;
    TimeUnit timeUnit() default TimeUnit.SECONDS;
    LockableEntity type() default LockableEntity.NONE;
}
