package com.smmhub.commons.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * @author nix (24.02.2017)
 */

public class MoneySerialize extends JsonSerializer<BigDecimal> {

	@Override
	public void serialize(BigDecimal value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
		// "банковское" округление
		jgen.writeString(value.setScale(2, BigDecimal.ROUND_HALF_EVEN).toString());
	}
}
