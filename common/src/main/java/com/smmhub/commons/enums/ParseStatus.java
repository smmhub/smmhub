package com.smmhub.commons.enums;

/**
 * @author nix (04.03.2017)
 */
public enum ParseStatus {
	OK,
	ERROR
}
