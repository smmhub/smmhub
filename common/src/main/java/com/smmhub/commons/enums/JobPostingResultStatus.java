package com.smmhub.commons.enums;

/**
 * @author nix (23.12.2017)
 */
public enum JobPostingResultStatus {
	SUCCESS,
	FAILED,
	SKIPPED,
	UNKNOWN
}
