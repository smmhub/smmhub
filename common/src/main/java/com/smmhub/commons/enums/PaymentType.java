package com.smmhub.commons.enums;

public enum PaymentType {
	PLUS,
	MINUS,
	UNKNOWN
}
