package com.smmhub.commons.enums;

/**
 * @author nix (01.03.2017)
 */
public enum AccountLoginType {
	FORM,
	VK,
	OK,
	FB,
	GOOGLE
}
