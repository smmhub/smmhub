package com.smmhub.commons.enums;

/**
 * @author nix (06.05.2017)
 */

public enum Language {
	RUSSIAN,
	ENGLISH
}
