package com.smmhub.commons.enums;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author nix (24.02.2017)
 */
public enum AccountType implements GrantedAuthority {

	BANNED, // зобанен
	REGULAR_USER, // самый обычный чувак
	PRO_USER, // чувак, заплативший бабла за допфункционал
	VIP_USER, // это ваще суперчел, которому мы пожизненно за что то должны
	MODERATOR, // на перспективу - будет модерировать посты
	ADMIN; // это мы :)

	@Override
	public String getAuthority() {
		return toString();
	}
}
