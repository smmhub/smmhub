package com.smmhub.commons.enums;

/**
 * @author Lycan on 25.02.2017.
 */
public enum SocialNetworkType {
    FB,
    VK,
    OK,
    TWITTER,
    TELEGRAMM,
    VIBER,
    GOOGLE_PLUS,
}
