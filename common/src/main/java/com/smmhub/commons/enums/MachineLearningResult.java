package com.smmhub.commons.enums;

import lombok.Getter;

/**
 * @author nix (18.09.2017)
 */
public enum MachineLearningResult {
	HAM("ham"),
	SPAM("spam");

	@Getter
	private String text;

	MachineLearningResult(String text) {
		this.text = text;
	}

	public static MachineLearningResult get(String text) {
		if(text.toLowerCase().equals("ham")) {
			return HAM;
		} else if (text.toLowerCase().equals("spam")) {
			return SPAM;
		} else {
			return null;
		}
	}
}
