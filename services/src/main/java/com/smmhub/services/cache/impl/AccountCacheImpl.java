package com.smmhub.services.cache.impl;

import com.smmhub.commons.cache.DistributedBackendCache;
import com.smmhub.commons.cache.config.CacheConfig;
import com.smmhub.commons.enums.AccountLoginType;
import com.smmhub.commons.model.dto.account.AccountDTO;
import com.smmhub.commons.model.dto.account.OkAccountDTO;
import com.smmhub.commons.model.dto.account.SimpleAccountDTO;
import com.smmhub.commons.model.dto.account.VkontakteAccountDTO;
import com.smmhub.database.model.sql.accounts.Account;
import com.smmhub.database.model.sql.accounts.types.OkAccount;
import com.smmhub.database.model.sql.accounts.types.SimpleAccount;
import com.smmhub.database.model.sql.accounts.types.VkontakteAccount;
import com.smmhub.services.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

/**
 * @author nix (07.03.2017)
 */

@Component
@Slf4j
public class AccountCacheImpl implements DistributedBackendCache<String, AccountDTO> {

	@Autowired
	private AccountService accountService;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	@CacheEvict(cacheNames = CacheConfig.ACCOUNTS, beforeInvocation = true, key = "#id")
	@CachePut(cacheNames = CacheConfig.ACCOUNTS, key = "#id")
	public AccountDTO put(String id, AccountDTO value) {
		return value;
	}

	@Override
	@Cacheable(value = CacheConfig.ACCOUNTS, sync = true, key = "#id")
	@SuppressWarnings("Duplicates")
	public AccountDTO get(String id) {
		try {
			String[] parsed = id.split("-");

			if (parsed.length != 2) {
				log.error("Incorrect ID passed to cache: {}", id);
				return null;
			}

			String email = parsed[0];
			AccountLoginType type = AccountLoginType.valueOf(parsed[1]);

			Account account = accountService.getAccount(email, type);

			AccountDTO accountDTO;
			if (account instanceof VkontakteAccount) {
				accountDTO = modelMapper.map(account, VkontakteAccountDTO.class);
			} else if (account instanceof OkAccount) {
				accountDTO = modelMapper.map(account, OkAccountDTO.class);
			} else if (account instanceof SimpleAccount) {
				accountDTO = modelMapper.map(account, SimpleAccountDTO.class);
			} else {
				throw new IllegalArgumentException("Unknown type of account!");
			}

			if (accountDTO != null) {
				return accountDTO;
			} else {
				return null;
			}

		} catch (Exception e) {
			return null;
		}
	}

	@Override
	@CacheEvict(cacheNames = CacheConfig.ACCOUNTS, beforeInvocation = true, key = "#id")
	public void evict(String id) {
	}
}
