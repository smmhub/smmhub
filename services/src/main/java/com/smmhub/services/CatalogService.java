package com.smmhub.services;

import com.smmhub.commons.exceptions.CatalogCategoryNotFoundException;
import com.smmhub.commons.exceptions.NotRootCatalogCategoryException;
import com.smmhub.commons.model.dto.account.AccountDTO;
import com.smmhub.database.model.sql.CatalogCategory;

/**
 * @author nix (04.03.2017)
 */
public interface CatalogService {

	/**
	 * Метод сохранения категории в каталог
	 *
	 * @param category категория каталога
	 * @return сущность каталога
	 * @throws IllegalArgumentException при переданных некорректных данных
	 */
	CatalogCategory saveCategory(CatalogCategory category) throws IllegalArgumentException;

	/**
	 * Метод получения категории из каталога
	 *
	 * @param name имя категория в каталоге
	 * @throws CatalogCategoryNotFoundException если категория каталога не найдена
	 */
	CatalogCategory getCategory(String name) throws CatalogCategoryNotFoundException, IllegalArgumentException;

	/**
	 * Метод получения категории из каталога
	 *
	 * @param id id категория в каталоге
	 * @throws CatalogCategoryNotFoundException если категория каталога не найдена
	 */
	CatalogCategory getCategory(Long id) throws CatalogCategoryNotFoundException, IllegalArgumentException;

	/**
	 * Метод удаления категории из каталога
	 *
	 * @param category категория каталога
	 * @throws CatalogCategoryNotFoundException если категория каталога не найдена
	 */
	void removeCategory(CatalogCategory category) throws CatalogCategoryNotFoundException;

	/**
	 * Метод возврата всего каталога в виде дерева с первого элемента
	 * @throws NotRootCatalogCategoryException если элемент не является вершиной дерева
	 */
	CatalogCategory getAllCatalog() throws NotRootCatalogCategoryException;

	/**
	 * Метод возврата всего каталога в виде дерева с первого элемента для конкретного
	 * пользователя. Включает в себя созданные им категории, которые не видят другие пользователи
	 * @throws NotRootCatalogCategoryException если элемент не является вершиной дерева
	 */
	CatalogCategory getAllCatalog(AccountDTO accountDTO) throws NotRootCatalogCategoryException;

}
