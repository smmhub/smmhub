package com.smmhub.services;

import com.smmhub.commons.enums.MachineLearningResult;
import com.smmhub.commons.model.machinelearning.ClassifyResult;

/**
 * @author Nikolay Viguro, 20.09.17
 */

public interface MachineLearningService {
    void addTrainingData(String message, MachineLearningResult classValue) throws Exception;
    ClassifyResult classifyMessage(String message) throws Exception;
}
