package com.smmhub.services.impl;

import com.smmhub.commons.cache.DistributedBackendCache;
import com.smmhub.commons.enums.AccountLoginType;
import com.smmhub.commons.enums.AccountType;
import com.smmhub.commons.enums.PaymentType;
import com.smmhub.commons.exceptions.*;
import com.smmhub.commons.locks.LockableEntity;
import com.smmhub.commons.locks.annotations.Lock;
import com.smmhub.commons.model.dto.account.AccountDTO;
import com.smmhub.commons.model.dto.account.OkAccountDTO;
import com.smmhub.commons.model.dto.account.SimpleAccountDTO;
import com.smmhub.commons.model.dto.account.VkontakteAccountDTO;
import com.smmhub.database.dao.sql.PaymentDAO;
import com.smmhub.database.dao.sql.accounts.AccountDAO;
import com.smmhub.database.dao.sql.accounts.OkAccountDAO;
import com.smmhub.database.dao.sql.accounts.SimpleAccountDAO;
import com.smmhub.database.dao.sql.accounts.VkontakteAccountDAO;
import com.smmhub.database.model.sql.Payment;
import com.smmhub.database.model.sql.accounts.Account;
import com.smmhub.database.model.sql.accounts.types.OkAccount;
import com.smmhub.database.model.sql.accounts.types.SimpleAccount;
import com.smmhub.database.model.sql.accounts.types.VkontakteAccount;
import com.smmhub.services.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;

/**
 * @author nix (24.02.2017)
 */

@Service
@Slf4j
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountDAO accountDAO;

	@Autowired
	private PaymentDAO paymentDAO;

	@Autowired
	private SimpleAccountDAO simpleAccountDAO;

	@Autowired
	private VkontakteAccountDAO vkontakteAccountDAO;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private OkAccountDAO okAccountDAO;

	@Autowired
	private DistributedBackendCache<String, AccountDTO> accountBackendCache;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	@Transactional
	@Lock(value = "#account.id", type = LockableEntity.ACCOUNT)
	public void removeAccount(Account account) throws AccountNotFoundException {

		Account accountDb = accountDAO.findById(account.getId()).orElseGet(null);;

		if (accountDb == null)
			throw new AccountNotFoundException();

		accountDAO.delete(accountDb);
	}

	@Override
	@Transactional
	public Account addAccount(Account account) throws AccountNotFoundException,
			AccountEmailRequiredException, AccountPasswordRequiredException {

		if (account == null)
			throw new AccountNotFoundException();

		account.setAuthority(Collections.singleton(AccountType.REGULAR_USER));
		account.setPassword(passwordEncoder.encode(account.getPassword()));

		Account accountSaved;
		if (account instanceof SimpleAccount) {
			SimpleAccount simpleAccount = (SimpleAccount) account;

			if (StringUtils.isEmpty(simpleAccount.getPassword()))
				throw new AccountPasswordRequiredException();

			if (StringUtils.isEmpty(simpleAccount.getEmail()))
				throw new AccountEmailRequiredException();

			simpleAccount.setLoginType(AccountLoginType.FORM);

			accountSaved = accountDAO.save(simpleAccount);
		} else if (account instanceof VkontakteAccount) {

			VkontakteAccount vkontakteAccount = (VkontakteAccount) account;
			vkontakteAccount.setLoginType(AccountLoginType.VK);

			accountSaved = accountDAO.save(vkontakteAccount);
		} else if (account instanceof OkAccount) {

			OkAccount okAccount = (OkAccount) account;
			okAccount.setLoginType(AccountLoginType.OK);

			accountSaved = accountDAO.save(okAccount);
		} else {
			log.error("Unknown account type {}, while saving", account.getClass());
			throw new IllegalArgumentException("Unknown account type while saving!");
		}

		getOrUpdateAccountInCache(accountSaved);
		return accountSaved;
	}

	@Override
	@Transactional
	@Lock(value = "#account.id", type = LockableEntity.ACCOUNT)
	public Account changeAccountPassword(Account account) throws AccountNotFoundException,
			AccountEmailRequiredException, AccountPasswordRequiredException {

		if (account == null)
			throw new AccountNotFoundException();

		if (!(account instanceof SimpleAccount))
			throw new IllegalArgumentException("Not SimpleAccount type!");

		SimpleAccount simpleAccount = (SimpleAccount) account;

		if (StringUtils.isEmpty(simpleAccount.getPassword()))
			throw new AccountPasswordRequiredException();

		if (StringUtils.isEmpty(simpleAccount.getEmail()))
			throw new AccountEmailRequiredException();

		if (simpleAccount.getId() != 0) {
			account = accountDAO.findById(account.getId()).orElseGet(null);;

			if (account == null)
				throw new AccountNotFoundException();

			if (!(account instanceof SimpleAccount))
				throw new IllegalArgumentException("Not SimpleAccount type!");

			simpleAccount = (SimpleAccount) account;
		}

		simpleAccount.setPassword(passwordEncoder.encode(simpleAccount.getPassword()));

		Account accountSaved = accountDAO.save(simpleAccount);
		getOrUpdateAccountInCache(accountSaved);

		return accountSaved;
	}

	@Override
	@Transactional
	@Lock(value = "#account.id", type = LockableEntity.ACCOUNT)
	public Account saveAccount(Account account) {

		Account accountSaved;
		if(account instanceof VkontakteAccount) {
			accountSaved = vkontakteAccountDAO.save((VkontakteAccount)account);
		} else if(account instanceof SimpleAccount) {
			accountSaved = simpleAccountDAO.save((SimpleAccount)account);
		} else if (account instanceof OkAccount) {
			accountSaved = okAccountDAO.save((OkAccount) account);
		} else {
			throw new IllegalArgumentException("Save account type not implemented! Check and fix!");
		}

		getOrUpdateAccountInCache(accountSaved);

		return accountSaved;
	}

	@Override
	@Transactional(readOnly = true)
	@Lock(value = "#account.id", type = LockableEntity.ACCOUNT)
	public Account getAccount(Long id) throws AccountIdentifierRequiredException, AccountNotFoundException {

		if (id == null || id == 0L)
			throw new AccountIdentifierRequiredException();

		Account accountDb = accountDAO.findById(id).orElseGet(null);;

		if (accountDb == null)
			throw new AccountNotFoundException();

		return accountDb;
	}

	@Override
	@Transactional(readOnly = true)
	@Lock(value = "#email", type = LockableEntity.ACCOUNT)
	public Account getAccount(String email, AccountLoginType socialNetwork) throws AccountIdentifierRequiredException,
			AccountNotFoundException {

		if (StringUtils.isEmpty(email))
			throw new AccountIdentifierRequiredException();

		Account accountDb = accountDAO.findByEmailAndLoginType(email, socialNetwork);

		if (accountDb == null)
			throw new AccountNotFoundException();

		return accountDb;
	}

	@Override
	@Transactional(readOnly = true)
	public Collection<Account> allAccounts() {
		return (Collection<Account>) accountDAO.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	@Lock(value = "#email", type = LockableEntity.ACCOUNT)
	public boolean checkPassword(String email, String password) throws AccountIdentifierRequiredException,
			AccountNotFoundException, AccountPasswordRequiredException {

		if (StringUtils.isEmpty(email))
			throw new AccountIdentifierRequiredException();

		if (StringUtils.isEmpty(password))
			throw new AccountPasswordRequiredException();

		SimpleAccount accountDb = (SimpleAccount) accountDAO.findByEmail(email);

		if (accountDb == null)
			throw new AccountNotFoundException();

		return passwordEncoder.matches(password, accountDb.getPassword());
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	@Lock(value = "#account.id", type = LockableEntity.ACCOUNT)
	public BigDecimal addMoney(Account account, BigDecimal amount) throws AccountNotFoundException, UnknownPaymentTypeException,
			AccountIdentifierRequiredException {

		if (account.getId() == null || account.getId() == 0)
			throw new AccountIdentifierRequiredException();

		try {
			return processMoney(account.getId(), PaymentType.PLUS, amount);
		} catch (InsufficientMoneyException ignored) {
			// не throw'ится
			return null;
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	@Lock(value = "#account.id", type = LockableEntity.ACCOUNT)
	public BigDecimal spentMoney(Account account, BigDecimal amount) throws AccountNotFoundException,
			UnknownPaymentTypeException, InsufficientMoneyException, AccountIdentifierRequiredException {

		if (account.getId() == null || account.getId() == 0)
			throw new AccountIdentifierRequiredException();

		return processMoney(account.getId(), PaymentType.MINUS, amount);
	}

	@Override
	@SuppressWarnings("Duplicates")
	public AccountDTO getOrUpdateAccountInCache(Account account) {
		AccountDTO accountDTO;
		if (account instanceof VkontakteAccount) {
			accountDTO = modelMapper.map(account, VkontakteAccountDTO.class);
		} else if (account instanceof OkAccount) {
			accountDTO = modelMapper.map(account, OkAccountDTO.class);
		} else if (account instanceof SimpleAccount) {
			accountDTO = modelMapper.map(account, SimpleAccountDTO.class);
		} else {
			throw new IllegalArgumentException("Unknown type of account!");
		}

		accountBackendCache.put(account.getEmail() + "-" + account.getLoginType().name(), accountDTO);

		return accountDTO;
	}

	// Сервисный метод, реализующий операции со счетом
	private BigDecimal processMoney(Long userId, PaymentType type, BigDecimal amount)
			throws AccountNotFoundException, UnknownPaymentTypeException,
			InsufficientMoneyException {

		Account account = accountDAO.findById(userId).orElseGet(null);;

		if (type.equals(PaymentType.PLUS))
			account.setBalance(account.getBalance().add(amount));
		else if (type.equals(PaymentType.MINUS)) {
			BigDecimal result = account.getBalance().add(amount.negate());

			// кредитов не выдаем
			if (result.compareTo(BigDecimal.ZERO) < 0)
				throw new InsufficientMoneyException();

			account.setBalance(result);
		} else {
			// Если вдруг, каким то чудом
			throw new UnknownPaymentTypeException();
		}

		Payment payment = new Payment(account, type, amount);
		paymentDAO.save(payment);

		// Сохраняем в БД
		Account accountSaved = accountDAO.save(account);

		getOrUpdateAccountInCache(accountSaved);

		return accountSaved.getBalance();
	}
}
