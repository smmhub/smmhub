package com.smmhub.services.impl;

import com.smmhub.services.ImageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;

/**
 * @author nix (30.11.2017)
 */

@Service
@Slf4j
public class ImageServiceImpl implements ImageService {

    @Value("${images.datadir:data/images}")
    private String imagesDatadir;

    @Override
    public ImageService.Result getImage(String url) {

        ImageInputStream iis = null;
        try {
            String imageFile = imagesDatadir + "/" + new Date().getTime() + new Random().nextInt();
            Files.copy(new URL(url).openStream(), Paths.get(imageFile), StandardCopyOption.REPLACE_EXISTING);
            File image = new File(imageFile);

            if (!image.exists()) {
                log.error("Readed image from URL {} is empty", url);
                return null;
            }

            iis = ImageIO.createImageInputStream(image);
            Iterator<ImageReader> iterator = ImageIO.getImageReaders(iis);

            ImageReader reader = iterator.next();
            String formatName = reader.getFormatName();
            ImageService.ImageMimeType type;

            switch (formatName.toLowerCase()) {
                case "jpeg":
                    type = ImageMimeType.JPEG;
                    break;
                case "png":
                    type = ImageMimeType.PNG;
                    break;
                case "bmp":
                    type = ImageMimeType.BMP;
                    break;
                case "gif":
                    type = ImageMimeType.GIF;
                    break;
                default:
                    type = ImageMimeType.UNKNOWN;
                    break;
            }

            return ImageService.Result.builder().file(image).mime(type).build();
        } catch (NullPointerException | IOException e) {
            log.error("Can't read image from URL {}", url);
            return null;
        }
        finally {
            try {
                if(iis != null) {
                    iis.close();
                }
            } catch (IOException e) {
                log.error("Error closing iis", e);
            }
        }
    }

    @Override
    public Set<ImageService.Result> getImages(Set<String> urls) {
        Set<ImageService.Result> images = new HashSet<>();
        urls.forEach(url -> images.add(getImage(url)));
        return images;
    }

    @Override
    public void removeImage(File file) {
        try {
            Files.delete(Paths.get(file.getAbsolutePath()));
        } catch (IOException e) {
            log.error("Error deleting file {}", file.getAbsolutePath());
        }
    }

    @Override
    public void removeImages(Set<File> files) {
        files.forEach(this::removeImage);
    }
}
