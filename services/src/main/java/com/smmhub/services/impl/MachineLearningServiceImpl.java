package com.smmhub.services.impl;

import com.smmhub.commons.enums.MachineLearningResult;
import com.smmhub.commons.model.machinelearning.ClassifyResult;
import com.smmhub.database.dao.sql.MachineLearningDAO;
import com.smmhub.database.model.sql.TrainingEntry;
import com.smmhub.services.MachineLearningService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import weka.classifiers.bayes.NaiveBayesMultinomialUpdateable;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Nikolay Viguro, 19.09.17
 */

@Service
@Slf4j
public class MachineLearningServiceImpl implements MachineLearningService {

    @Autowired
    private MachineLearningDAO machineLearningDAO;

    @Value("${machinelearning.required.trained.messages:500L}")
    private Long neededTrained;

    private Instances trainingData;
    private StringToWordVector filter;
    private NaiveBayesMultinomialUpdateable classifier;
    private ArrayList<String> classValues;
    private ArrayList<Attribute> attributes;
    private Long trainingCount = 0L;
    private boolean upToDate;
    private boolean setup;

    @PostConstruct
    public void initialize() throws Exception {
        this.filter = new StringToWordVector();
        this.classifier = new NaiveBayesMultinomialUpdateable();

        this.attributes = new ArrayList<>(2);
        this.attributes.add(new Attribute("text", (List<String>) null));

        this.trainingCount = machineLearningDAO.count();

        this.classValues = new ArrayList<>(2);
        this.classValues.add(MachineLearningResult.HAM.getText());
        this.classValues.add(MachineLearningResult.SPAM.getText());

        setupAfterCategorysAdded();

        for(TrainingEntry entry : machineLearningDAO.findAll()) {
            loadTrainingData(entry.getMessage(), entry.getType());
        }

        log.debug("Loading train data successful");

        if(trainingCount < neededTrained) {
            log.debug("Required trained objects for service startup: {}/{}", trainingCount, neededTrained);
        }
    }

    @Override
    public void addTrainingData(String message, MachineLearningResult classValue) throws Exception {
        log.debug("Adding train data started");
        if (!setup) {
            throw new IllegalStateException("Must use setup first");
        }
        message = message.toLowerCase();
        String classValueString = classValue.getText();
        Instance instance = makeInstance(message, trainingData);
        instance.setClassValue(classValueString);

        trainingData.add(instance);

        TrainingEntry entry = new TrainingEntry();
        entry.setCreated(new Date());
        entry.setMessage(message);
        entry.setType(classValue);
        machineLearningDAO.save(entry);

        this.trainingCount++;
        upToDate = false;

        log.debug("Successful adding train data");

        if(trainingCount < neededTrained) {
            log.debug("Required training data: {}/{}", trainingCount, neededTrained);
        }
    }

    private void loadTrainingData(String message, MachineLearningResult classValue) throws Exception {
        if (!setup) {
            throw new IllegalStateException("Must use setup first");
        }
        message = message.toLowerCase();
        String classValueString = classValue.getText();
        Instance instance = makeInstance(message, trainingData);
        instance.setClassValue(classValueString);

        trainingData.add(instance);
        upToDate = false;
    }

    @Override
    public ClassifyResult classifyMessage(String message) throws Exception {
        message = message.toLowerCase();

        if (!setup) {
            throw new IllegalStateException("Must use setup first");
        }

        if(this.trainingCount < neededTrained) {
            return new ClassifyResult(0.0D, 0.0D, false);
        }

        if (trainingData.numInstances() == 0) {
            throw new IllegalStateException("No classifier training data available");
        }

        buildIfNeeded();

        Instances testset = trainingData.stringFreeStructure();
        Instance testInstance = makeInstance(message, testset);

        filter.input(testInstance);
        Instance filteredInstance = filter.output();

        double[] result = classifier.distributionForInstance(filteredInstance);

        return new ClassifyResult(result[0], result[1], result[0] < 0.5D);
    }

    private void buildIfNeeded() throws Exception {
        if (!upToDate) {
            filter.setInputFormat(trainingData);
            Instances filteredData = Filter.useFilter(trainingData, filter);
            classifier.buildClassifier(filteredData);
            upToDate = true;
        }
    }

    private Instance makeInstance(String text, Instances data) {
        Instance instance = new DenseInstance(2);
        Attribute messageAtt = data.attribute("text");
        instance.setValue(messageAtt, messageAtt.addStringValue(text));
        instance.setDataset(data);
        return instance;
    }

    private void setupAfterCategorysAdded() throws Exception {
        attributes.add(new Attribute("class", classValues));
        trainingData = new Instances(
                "MessageClassificationProblem",
                attributes,
                trainingCount == 0L ? 100 : trainingCount.intValue());
        trainingData.setClassIndex(trainingData.numAttributes() - 1);

        setup = true;
    }
}
