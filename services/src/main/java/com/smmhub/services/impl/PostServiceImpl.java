package com.smmhub.services.impl;

import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.commons.exceptions.NoPostsFoundException;
import com.smmhub.database.dao.elastic.PostDAO;
import com.smmhub.database.model.elastic.Post;
import com.smmhub.database.model.sql.accounts.Account;
import com.smmhub.services.PostService;
import io.jsonwebtoken.lang.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * @author nix (26.02.2017)
 */

@Service
public class PostServiceImpl implements PostService {

	private final PostDAO postDAO;
	private final ElasticsearchTemplate searchTemplate;

	@Autowired
	public PostServiceImpl(PostDAO postDAO, ElasticsearchTemplate searchTemplate) {
		this.postDAO = postDAO;
		this.searchTemplate = searchTemplate;
	}

	@Override
	public Post savePost(Post post) {
		return postDAO.save(post);
	}

	@Override
	public void removePost(Post post) {
		postDAO.delete(post);
	}

	@Override
	public Post findById(String id) {
		return postDAO.findById(id).orElse(null);
	}

    @Override
    public Page<Post> findByIds(Set<String> ids, Pageable pageable) {
        return postDAO.findByIdIn(ids, pageable);
    }

	@Override
	public Post findBySocialPostId(String postId) {
		return postDAO.findByGroupPostId(postId);
	}

	@Override
	public Post findByDatabasePostId(String postId) {
		return postDAO.findById(postId).orElseGet(null);
	}

	@Override
	public Post findByTextMD5(String md5) {
		return postDAO.findByTextMD5(md5);
	}

	@Override
	public Page<Post> findPostByCategories(Set<Long> categoryIds, Pageable pageable) {
		return postDAO.findByCategoryIdsInOrderByPostedDesc(categoryIds, pageable);
	}

	@Override
	public Post findRandomPostInCategories(Set<Long> categoryIds, Set<String> skipPostIds) throws NoPostsFoundException {
		int randomIndex = new Random().nextInt(Integer.MAX_VALUE);
		int retry = 0, maxRetires = 100;
		Post ret = null;

		while (retry <= maxRetires) {
			List<Post> posts = postDAO.getRandomPostByCategoryIds(categoryIds, randomIndex, PageRequest.of(1, 10)).getContent();

			if (Collections.isEmpty(posts)) {
				throw new NoPostsFoundException("No posts found by specified categories");
			}

			for (Post post : posts) {
				if (!skipPostIds.contains(post.getId())) {
					ret = post;
					break;
				}
			}

			if (ret != null) {
				break;
			}

			retry++;
		}

		if (ret == null) {
			throw new NoPostsFoundException("No posts found by specified categories. Maximum retries reached!");
		}

		return ret;
	}

	@Override
	public List<Post> findByKeyword(String keywords, SocialNetworkType network, int page) {
		return null;
	}

	@Override
	public List<Post> findNewForAccoutByKeyword(Account account, String keywords, SocialNetworkType network, int page) {
		return null;
	}

	@Override
	public boolean markAsPostedForAccount(Account account, Post post, SocialNetworkType network) {
		return false;
	}
}
