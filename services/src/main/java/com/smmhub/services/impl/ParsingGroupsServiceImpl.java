package com.smmhub.services.impl;

import com.google.common.collect.Lists;
import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.commons.exceptions.NoParsingUsersException;
import com.smmhub.database.dao.sql.ParsingGroupsDAO;
import com.smmhub.database.dao.sql.ParsingUserDAO;
import com.smmhub.database.model.sql.CatalogGroup;
import com.smmhub.database.model.sql.ParsingUser;
import com.smmhub.services.ParsingGroupsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * @author nix (08.03.2017)
 */

@Service
public class ParsingGroupsServiceImpl implements ParsingGroupsService {

	private final ParsingUserDAO parsingUserDAO;
	private final ParsingGroupsDAO parsingGroupsDAO;

	@Autowired
	public ParsingGroupsServiceImpl(ParsingUserDAO parsingUserDAO, ParsingGroupsDAO parsingGroupsDAO) {
		this.parsingUserDAO = parsingUserDAO;
		this.parsingGroupsDAO = parsingGroupsDAO;
	}

	@Override
	public ParsingUser getRandomParsingUser(SocialNetworkType socialNetworkType) throws NoParsingUsersException {

		List<ParsingUser> users = Lists.newArrayList(parsingUserDAO.findBySocialNetworkType(socialNetworkType));

		if (users.size() == 0) {
			throw new NoParsingUsersException();
		} else if (users.size() == 1) {
			return users.get(0);
		}

		Collections.shuffle(users);

		return users.get(0);
	}

	@Override
	public Set<CatalogGroup> getGroupsByNetwork(SocialNetworkType socialNetworkType) {
		return parsingGroupsDAO.findBySocialNetworkType(socialNetworkType);
	}

	@Override
	public CatalogGroup getGroup(Long groupId) {
		return parsingGroupsDAO.findById(groupId).orElseGet(null);
	}

	@Override
	public CatalogGroup getGroup(String groupId) {
		return parsingGroupsDAO.findByGroupId(groupId);
	}

	@Override
	public CatalogGroup saveGroup(CatalogGroup group) {
		return parsingGroupsDAO.save(group);
	}
}
