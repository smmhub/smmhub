package com.smmhub.services.impl;

import com.smmhub.commons.exceptions.CatalogCategoryNotFoundException;
import com.smmhub.commons.exceptions.NotRootCatalogCategoryException;
import com.smmhub.commons.model.dto.account.AccountDTO;
import com.smmhub.database.dao.sql.CatalogDAO;
import com.smmhub.database.model.sql.CatalogCategory;
import com.smmhub.services.CatalogService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Iterator;

/**
 * @author nix (04.03.2017)
 */

@Service
public class CatalogServiceImpl implements CatalogService {

	private final CatalogDAO catalogDAO;

	@Autowired
	public CatalogServiceImpl(CatalogDAO catalogDAO) {
		this.catalogDAO = catalogDAO;
	}

	@Override
	@Transactional
	public CatalogCategory saveCategory(CatalogCategory category) throws IllegalArgumentException {

		if (category == null)
			throw new IllegalArgumentException();

		return catalogDAO.save(category);
	}

	@Override
	@Transactional(readOnly = true)
	public CatalogCategory getCategory(String name) throws CatalogCategoryNotFoundException, IllegalArgumentException {

		if (StringUtils.isEmpty(name))
			throw new IllegalArgumentException();

		CatalogCategory catalogCategoryDb = catalogDAO.findByName(name);

		if (catalogCategoryDb == null)
			throw new CatalogCategoryNotFoundException();

		return catalogCategoryDb;
	}

	@Override
	@Transactional(readOnly = true)
	public CatalogCategory getCategory(Long id) throws CatalogCategoryNotFoundException, IllegalArgumentException {

		if (id == null || id == 0)
			throw new IllegalArgumentException("Catalog category id is null or 0");

		CatalogCategory catalogCategoryDb = catalogDAO.findById(id).orElseGet(null);

		if (catalogCategoryDb == null)
			throw new CatalogCategoryNotFoundException();

		return catalogCategoryDb;
	}

	@Override
	@Transactional
	public void removeCategory(CatalogCategory category) throws CatalogCategoryNotFoundException {

		if (category == null || category.getId() == null)
			throw new CatalogCategoryNotFoundException();

		CatalogCategory catalogCategoryDb = catalogDAO.findById(category.getId()).orElseGet(null);

		if (catalogCategoryDb == null)
			throw new CatalogCategoryNotFoundException();

		catalogDAO.delete(catalogCategoryDb);
	}

	@Override
	@Transactional(readOnly = true)
	public CatalogCategory getAllCatalog() throws NotRootCatalogCategoryException {
		CatalogCategory root = catalogDAO.findById(1L).orElseGet(null);

		if (root.getParent() != null)
			throw new NotRootCatalogCategoryException();

		return root;
	}

	@Override
	@Transactional(readOnly = true)
	public CatalogCategory getAllCatalog(AccountDTO accountDTO) throws NotRootCatalogCategoryException {
		CatalogCategory root = catalogDAO.findById(1L).orElseGet(null);

		if (root.getParent() != null)
			throw new NotRootCatalogCategoryException();

		removeForeignPrivateCategories(root, accountDTO);
		return root;
	}

	private void removeForeignPrivateCategories(CatalogCategory root, AccountDTO account) {
		String identFromAccount = account.getEmail() + "-" + account.getLoginType().name();
		Iterator iter = root.getChilds().iterator();

		while (iter.hasNext()) {
			CatalogCategory category = (CatalogCategory) iter.next();

			if(category.getUserPrivate() && category.getAccount() != null) {
				String identFromCategory = category.getAccount().getEmail() + "-" + category.getAccount().getLoginType().name();

				if(!identFromCategory.equals(identFromAccount)) {
					iter.remove();
				}
			} else {
				if(!CollectionUtils.isEmpty(category.getChilds())) {
					removeForeignPrivateCategories(category, account);
				}
			}
		}
	}
}
