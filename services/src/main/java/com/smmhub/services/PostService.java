package com.smmhub.services;

import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.commons.exceptions.NoPostsFoundException;
import com.smmhub.database.model.elastic.Post;
import com.smmhub.database.model.sql.accounts.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

/**
 * @author nix (26.02.2017)
 */
public interface PostService {

	Post savePost(Post post);

	void removePost(Post post);

	Post findById(String id);

    Page<Post> findByIds(Set<String> ids, Pageable pageable);

	Post findBySocialPostId(String postId);

	Post findByDatabasePostId(String postId);

	Post findByTextMD5(String md5);

	Page<Post> findPostByCategories(Set<Long> categoryIds, Pageable pageable);

	Post findRandomPostInCategories(Set<Long> categoryIds, Set<String> skipPostIds) throws NoPostsFoundException;

	List<Post> findByKeyword(String keywords, SocialNetworkType network, int page);

	List<Post> findNewForAccoutByKeyword(Account account, String keywords, SocialNetworkType network, int page);

	boolean markAsPostedForAccount(Account account, Post post, SocialNetworkType network);
}
