package com.smmhub.services;

import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.commons.exceptions.NoParsingUsersException;
import com.smmhub.database.model.sql.CatalogGroup;
import com.smmhub.database.model.sql.ParsingUser;

import java.util.Set;

/**
 * @author nix (08.03.2017)
 */
public interface ParsingGroupsService {
	ParsingUser getRandomParsingUser(SocialNetworkType socialNetworkType) throws NoParsingUsersException;

	Set<CatalogGroup> getGroupsByNetwork(SocialNetworkType socialNetworkType);

	CatalogGroup getGroup(Long groupId);

	CatalogGroup getGroup(String groupId);
	CatalogGroup saveGroup(CatalogGroup group);
}
