package com.smmhub.services;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.io.File;
import java.util.Set;

/**
 * @author Nikolay Viguro, 30.11.17
 */

public interface ImageService {
    ImageService.Result getImage(String url);
    Set<ImageService.Result> getImages(Set<String> urls);
    void removeImage(File file);
    void removeImages(Set<File> files);

    @Builder
    @Getter
    class Result {
        private ImageMimeType mime;
        private File file;
    }

    @Getter
    @AllArgsConstructor
    enum ImageMimeType {

        JPEG("image/jpeg", "jpeg"),
        BMP("image/bmp", "bmp"),
        PNG("image/png", "png"),
        GIF("image/gif", "gif"),
        UNKNOWN("image/gif", "gif");

        private String type;
        private String extension;
    }
}
