package com.smmhub.services;

import com.smmhub.commons.enums.AccountLoginType;
import com.smmhub.commons.exceptions.*;
import com.smmhub.commons.model.dto.account.AccountDTO;
import com.smmhub.database.model.sql.accounts.Account;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * @author nix (24.02.2017)
 */
public interface AccountService {

	/**
	 * Метод удаления пользовтеля
	 *
	 * @param account account пользователя
	 * @throws AccountNotFoundException при указании пустого имени
	 */
	void removeAccount(Account account) throws AccountNotFoundException;

	/**
	 * Мотод cохранения аккаунта
	 *
	 * @param account аккаунт пользователя
	 * @return сущность пользователя
	 */
	Account saveAccount(Account account);

	/**
	 * Метод cохранения НОВОГО аккаунта
	 *
	 * @param account аккаунт пользователя
	 * @return сущность пользователя
	 * @throws AccountEmailRequiredException    при указании пустого email
	 * @throws AccountPasswordRequiredException при указании пустого password
	 * @throws AccountNotFoundException         если аккаунт не найден
	 */
	Account addAccount(Account account) throws AccountNotFoundException,
			AccountEmailRequiredException, AccountPasswordRequiredException;

	/**
	 * Метод генерации хэшкода из строки пароля и сохранения в аккаунт
	 *
	 * @param account аккаунт пользователя
	 * @return сущность пользователя
	 * @throws AccountEmailRequiredException    при указании пустого email
	 * @throws AccountPasswordRequiredException при указании пустого password
	 * @throws AccountNotFoundException         если аккаунт не найден
	 */
	Account changeAccountPassword(Account account) throws AccountNotFoundException,
			AccountEmailRequiredException, AccountPasswordRequiredException;

	/**
	 * Метод генерации хэшкода из строки пароля и сохранения в аккаунт
	 *
	 * @param username    username пользователя
	 * @param password password пользователя
	 * @return совпадают ли пароли
	 * @throws AccountPasswordRequiredException при указании пустого password
	 * @throws AccountNotFoundException         если аккаунт не найден
	 */
	boolean checkPassword(String username, String password) throws AccountIdentifierRequiredException,
			AccountNotFoundException, AccountPasswordRequiredException;

	/**
	 * Мотод загрузки определенного аккаунта
	 *
	 * @param id внутренний id пользователя в БД
	 * @return сущность пользователя
	 * @throws AccountIdentifierRequiredException при указании пустого id
	 * @throws AccountNotFoundException      если аккаунт не найден
	 */
	Account getAccount(Long id) throws AccountIdentifierRequiredException, AccountNotFoundException;

	/**
	 * Мотод загрузки определенного аккаунта по user id из соцсети или по email (если AccountLoginType == EMAIL)
	 *
	 * @param email email пользователя в соцсети
	 * @param socialNetwork   соцсеть
	 * @return сущность пользователя
	 * @throws AccountIdentifierRequiredException при указании пустого id
	 * @throws AccountNotFoundException           если аккаунт не найден
	 */
	Account getAccount(String email, AccountLoginType socialNetwork) throws AccountIdentifierRequiredException,
			AccountNotFoundException;

	/**
	 * Метод получения всех пользователей системы
	 *
	 * @return коллекция сущностей пользователей
	 */
	Collection<Account> allAccounts();

	/**
	 * Обновить данные аккаунта в кеше и получить DTO
	 *
	 * @param account account аккаунта
	 * @return DTO аккаунта
	 */
	AccountDTO getOrUpdateAccountInCache(Account account);

	/**
	 * Пополнить счет
	 *
	 * @param account account аккаунта
	 * @param amount сумма пополнения
	 * @return текущий баланс
	 * @throws AccountNotFoundException    если аккаунт не найден в системе
	 * @throws UnknownPaymentTypeException не используется
	 */
	BigDecimal addMoney(Account account, BigDecimal amount) throws AccountNotFoundException, UnknownPaymentTypeException, AccountIdentifierRequiredException;

	/**
	 * Списать со счета
	 *
	 * @param account account аккаунта
	 * @param amount сумма снятия
	 * @return текущий баланс
	 * @throws AccountNotFoundException   если аккаунт не найден в системе
	 * @throws InsufficientMoneyException если баланс счета в результате операции будет меньше нуля
	 */
	BigDecimal spentMoney(Account account, BigDecimal amount) throws AccountNotFoundException,
			UnknownPaymentTypeException, InsufficientMoneyException, AccountIdentifierRequiredException;

}
