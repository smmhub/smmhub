package com.smmhub.services;

import org.springframework.mobile.device.Device;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;

/**
 * @author Nikolay Viguro <nikolay.viguro@loyaltyplant.com>, 19.09.17
 */
public interface TokenUtils {
    String getUsernameFromToken(String token);

    Date getCreatedDateFromToken(String token);

    Date getExpirationDateFromToken(String token);

    String getAudienceFromToken(String token);

    String generateToken(UserDetails userDetails, Device device);

    Boolean canTokenBeRefreshed(String token, Date lastPasswordReset);

    String refreshToken(String token);

    Boolean validateToken(String token, UserDetails userDetails);
}
