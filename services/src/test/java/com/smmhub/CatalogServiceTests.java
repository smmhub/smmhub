package com.smmhub;

import com.smmhub.commons.exceptions.CatalogCategoryNotFoundException;
import com.smmhub.database.model.sql.CatalogCategory;
import com.smmhub.services.CatalogService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class CatalogServiceTests {

	@Autowired
	private CatalogService catalogService;

	private CatalogCategory category1, category2;

	@Before
	public void setUp() {

		category1 = new CatalogCategory("Parent1");
		category2 = new CatalogCategory("Parent2");

		category1 = catalogService.saveCategory(category1);
		category2 = catalogService.saveCategory(category2);

		category1.getChilds().add(new CatalogCategory(category1, "Child1ForParent1"));
		category1.getChilds().add(new CatalogCategory(category1, "Child2ForParent1"));
		category1.getChilds().add(new CatalogCategory(category1, "Child3ForParent1"));

		category2.getChilds().add(new CatalogCategory(category2, "Child1ForParent2"));
		category2.getChilds().add(new CatalogCategory(category2, "Child2ForParent2"));

		category1 = catalogService.saveCategory(category1);
		category2 = catalogService.saveCategory(category2);
	}

	@After
	public void tearDown() throws CatalogCategoryNotFoundException {

		try {
			catalogService.getCategory(category1.getName());
		} catch (CatalogCategoryNotFoundException ex) {
			return;
		}

		catalogService.removeCategory(category1);
		catalogService.removeCategory(category2);
	}

	@Test
	public void checkLoadFromDatabase() {
		Assert.assertNotNull(category1.getId());
		Assert.assertNotNull(category2.getId());
		Assert.assertEquals(category1.getName(), "Parent1");
		Assert.assertEquals(category2.getName(), "Parent2");
	}

	@Test(expected = CatalogCategoryNotFoundException.class)
	public void checkNotExistedCategory() throws CatalogCategoryNotFoundException {
		catalogService.getCategory("blablabla");
	}

	@Test(expected = IllegalArgumentException.class)
	public void checkEmptyCategoryNamePassed() throws CatalogCategoryNotFoundException {
		catalogService.getCategory("");
	}

	@Test
	public void checkChildExistedCategory() throws CatalogCategoryNotFoundException {
		CatalogCategory child = catalogService.getCategory("Child2ForParent1");
		Assert.assertEquals(child.getParent().getName(), "Parent1");
	}

	@Test
	public void checkChildsSize() throws CatalogCategoryNotFoundException {
		CatalogCategory parent1 = catalogService.getCategory("Parent1");
		CatalogCategory parent2 = catalogService.getCategory("Parent2");
		Assert.assertEquals(parent1.getChilds().size(), 3);
		Assert.assertEquals(parent2.getChilds().size(), 2);
	}

	@Test
	public void checkCanDelete() throws CatalogCategoryNotFoundException {
		catalogService.removeCategory(category1);
		catalogService.removeCategory(category2);
	}
}