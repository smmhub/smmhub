package com.smmhub;

import com.smmhub.commons.enums.AccountLoginType;
import com.smmhub.commons.exceptions.*;
import com.smmhub.database.model.sql.accounts.Account;
import com.smmhub.database.model.sql.accounts.types.SimpleAccount;
import com.smmhub.services.AccountService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Collection;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("tests")
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class AccountServiceTests {

	@Autowired
	private AccountService accountService;

	private SimpleAccount account1, account2;

	@Before
	public void setUp() throws Exception {
		account1 = new SimpleAccount("user1@smmhub.com");
		account1.setPassword("test1");
		account1.setLoginType(AccountLoginType.FORM);
		account2 = new SimpleAccount("user2@smmhub.com");
		account2.setPassword("test2");
		account2.setLoginType(AccountLoginType.FORM);

		account1 = (SimpleAccount) accountService.addAccount(account1);
		account2 = (SimpleAccount) accountService.addAccount(account2);
		accountService.addMoney(account1, new BigDecimal("100.00"));
		accountService.addMoney(account2, new BigDecimal("150.00"));
	}

	@After
	public void tearDown() throws AccountNotFoundException {

		Collection<Account> accounts = accountService.allAccounts();

		for (Account acc : accounts) {
			accountService.removeAccount(acc);
		}
	}

	@Test
	public void checkLoadFromDatabase() throws AccountNotFoundException, AccountEmailRequiredException, AccountIdentifierRequiredException {
		Assert.assertNotNull(account1);
		Assert.assertNotNull(account2);
		Assert.assertEquals(account1.getEmail(), "user1@smmhub.com");
		Assert.assertEquals(account2.getEmail(), "user2@smmhub.com");
	}

	@Test(expected = AccountNotFoundException.class)
	public void checkNotExistedUser() throws AccountNotFoundException, AccountEmailRequiredException, AccountIdentifierRequiredException {
		accountService.getAccount("olololo@smmhub.com", AccountLoginType.FORM);
	}

	public void checkEmptyEmailPassed() throws AccountNotFoundException, AccountEmailRequiredException, AccountIdentifierRequiredException {
		Assert.assertNull(accountService.getAccount(null));
	}

	@Test
	public void checkCollectionSize() {
		Collection<Account> accounts = accountService.allAccounts();
		Assert.assertEquals(accounts.size(), 2);
	}

	@Test
	public void checkCanDelete() {
		Collection<Account> accounts = accountService.allAccounts();

		Assert.assertEquals(accounts.size(), 2);

		accounts.forEach(
				account -> {
					try {
						accountService.removeAccount(account);
					} catch (AccountNotFoundException e) {
						Assert.fail();
					}
				}
		);

		accounts = accountService.allAccounts();
		Assert.assertEquals(accounts.size(), 0);
	}

	@Test
	public void addMoney() throws AccountNotFoundException, UnknownPaymentTypeException, InsufficientMoneyException,
			AccountEmailRequiredException, AccountIdentifierRequiredException {

		BigDecimal balance1 = accountService.addMoney(account1, new BigDecimal("30.00"));
		Assert.assertEquals(balance1, new BigDecimal("130.00"));
	}

	@Test
	public void spentMoney() throws AccountNotFoundException, UnknownPaymentTypeException, InsufficientMoneyException,
			AccountEmailRequiredException, AccountIdentifierRequiredException {

		BigDecimal balance1 = accountService.spentMoney(account1, new BigDecimal("30.00"));
		BigDecimal balance2 = accountService.spentMoney(account2, new BigDecimal("55.00"));

		Assert.assertEquals(balance1, new BigDecimal("70.00"));
		Assert.assertEquals(balance2, new BigDecimal("95.00"));
	}

	@Test(expected = InsufficientMoneyException.class)
	public void checkNoMoney() throws AccountNotFoundException, UnknownPaymentTypeException, InsufficientMoneyException,
			AccountEmailRequiredException, AccountIdentifierRequiredException {

		accountService.spentMoney(account1, new BigDecimal("1030.00"));
	}

	@Test
	public void checkPasswordsEquals() throws Exception {
		Assert.assertTrue(accountService.checkPassword(account1.getEmail(), "test1"));
	}
}