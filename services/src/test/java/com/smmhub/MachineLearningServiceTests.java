package com.smmhub;

import com.smmhub.commons.enums.MachineLearningResult;
import com.smmhub.commons.model.machinelearning.ClassifyResult;
import com.smmhub.services.MachineLearningService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("tests")
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class MachineLearningServiceTests {

	@Autowired
	private MachineLearningService bayesService;

	@Before
	public void setUp() throws Exception {

		for(int i = 1; i <= 20; i++) {
			bayesService.addTrainingData("спам", MachineLearningResult.SPAM);
			bayesService.addTrainingData("совсем спам", MachineLearningResult.SPAM);
			bayesService.addTrainingData("вообще спам бля", MachineLearningResult.SPAM);
			bayesService.addTrainingData("виагра ххх", MachineLearningResult.SPAM);
			bayesService.addTrainingData("ЗВОНИ СЕЙЧАС!!!", MachineLearningResult.SPAM);
		}

		for(int i = 1; i <= 20; i++) {
			bayesService.addTrainingData("как дела?", MachineLearningResult.HAM);
			bayesService.addTrainingData("все хорошо", MachineLearningResult.HAM);
			bayesService.addTrainingData("крутая штука", MachineLearningResult.HAM);
			bayesService.addTrainingData("нейронная сеть", MachineLearningResult.HAM);
			bayesService.addTrainingData("Гоша нифига писать не хочет", MachineLearningResult.HAM);
		}
	}

	@Test
	public void checkHamSpamConfidence() throws Exception {
		ClassifyResult result = bayesService.classifyMessage("спам тут");
		log.info("Spam message: {}", result);
		Assert.assertEquals(result.isSpamDetected(), true);

		result = bayesService.classifyMessage("Гоша пишет код? О_О");
		log.info("Ham message: {}", result);
		Assert.assertEquals(result.isSpamDetected(), false);
	}

}