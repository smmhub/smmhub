package com.smmhub;

import com.smmhub.commons.consts.SocialCredentialsConsts;
import com.smmhub.commons.exceptions.APIException;
import com.smmhub.commons.exceptions.APIRequestException;
import com.smmhub.commons.model.datasources.*;
import com.smmhub.datasources.SocialAPI;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.util.Collections;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("tests")
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.NONE)
@Slf4j
public class VkAPITests {

	@Autowired
	@Qualifier("vk")
	private SocialAPI vkApi;

	@Autowired
	private SocialCredentialsConsts socialCredentialsConsts;

	@Before
	public void setUp() throws APIException, APIRequestException {
		vkApi.init(socialCredentialsConsts.vkTestUserId, socialCredentialsConsts.vkTestAccessToken);
		Assert.assertNotNull(vkApi.getAccessToken());
	}

	@Test
	public void gettingUserProfile() throws APIException, APIRequestException {
		SocialProfile profile = vkApi.getProfileInfo();
		Assert.assertNotNull(profile.getUserId());
	}

	@Test
	public void gettingUserOwnGroups() throws APIException, APIRequestException {
		Pageable<SocialGroup> groupList = vkApi.getOwnedGroups(new Pagination());
		Assert.assertNotEquals(groupList.getData().size(), 0);
	}

	@Test
	public void gettingPostFromGroup() throws APIException, APIRequestException {
		Pageable<SocialGroupPost> groupPostsInfo = vkApi.getGroupPosts(socialCredentialsConsts.vkTestGroup, new Pagination());
		Assert.assertNotEquals(groupPostsInfo.getData().size(), 0);
	}

	@Test
	public void addPost() throws APIException, APIRequestException {
		SocialGroupPost post = new SocialGroupPost();
		post.setFromId(socialCredentialsConsts.vkTestUserId);
		post.setGroupId(socialCredentialsConsts.vkTestGroup);
		post.setText("Тестовый пост на стену группы");

		ClassLoader classLoader = getClass().getClassLoader();

		SocialGroupPost.PostAttachment attachment1 = new SocialGroupPost.PostAttachment();
		attachment1.setAttachmentType(SocialGroupPost.AttachmentType.PHOTO);

		File attFile1 = new File(classLoader.getResource("1.JPG").getFile());
		attachment1.setAttach(attFile1);

		post.setAttachments(Collections.singletonList(attachment1));

		//Integer postId = (Integer) vkApi.addPost(post);

		//Assert.assertNotNull(postId);
	}
}