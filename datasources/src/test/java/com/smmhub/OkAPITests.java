package com.smmhub;

import com.smmhub.commons.consts.SocialCredentialsConsts;
import com.smmhub.commons.exceptions.APIException;
import com.smmhub.commons.exceptions.APIRequestException;
import com.smmhub.commons.model.datasources.*;
import com.smmhub.datasources.SocialAPI;
import com.smmhub.services.ImageService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.util.Collections;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("tests")
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.NONE)
@Slf4j
public class OkAPITests {

	@Autowired
	@Qualifier("ok")
	private SocialAPI okApi;

	@Autowired
	private SocialCredentialsConsts socialCredentialsConsts;

	@Before
	public void setUp() throws APIException, APIRequestException {
		okApi.init(null, socialCredentialsConsts.okTestAccessToken, true);
		Assert.assertNotNull(okApi.getAccessToken());
	}

	@Test
	public void gettingUserProfile() throws APIException, APIRequestException {
		SocialProfile profile = okApi.getProfileInfo();
		Assert.assertNotNull(profile.getUserId());
	}

	@Test
	public void gettingUserOwnGroups() throws APIException, APIRequestException {
		Pageable<SocialGroup> groupList = okApi.getOwnedGroups(new Pagination());
		Assert.assertNotEquals(groupList.getData().size(), 0);
	}

	@Test
	public void gettingPostFromGroup() throws APIException, APIRequestException {
		Pageable<SocialGroupPost> groupPostsInfo = okApi.getGroupPosts(socialCredentialsConsts.okTestGroup, new Pagination());
		Assert.assertNotEquals(groupPostsInfo.getData().size(), 0);
	}

	@Test
	public void addPost() throws APIException, APIRequestException {
		SocialGroupPost post = new SocialGroupPost();
		post.setGroupId(socialCredentialsConsts.okTestGroup);
		post.setText("Тестовый пост на стену группы OK");

		ClassLoader classLoader = getClass().getClassLoader();

		SocialGroupPost.PostAttachment attachment1 = new SocialGroupPost.PostAttachment();
		attachment1.setAttachmentType(SocialGroupPost.AttachmentType.PHOTO);

		File attFile1 = new File(classLoader.getResource("1.JPG").getFile());
		ImageService.Result attachResult = ImageService.Result.builder()
				.file(attFile1)
				.mime(ImageService.ImageMimeType.JPEG)
				.build();

		attachment1.setAttach(attachResult);

		post.setAttachments(Collections.singletonList(attachment1));

		String postId = (String) okApi.addPost(post);

		Assert.assertNotNull(postId);
	}
}