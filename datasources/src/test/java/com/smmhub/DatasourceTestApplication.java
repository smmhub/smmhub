package com.smmhub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication()
@EnableAutoConfiguration()
public class DatasourceTestApplication {
	public static void main(String[] args) {
		SpringApplication.run(DatasourceTestApplication.class, args);
	}
}