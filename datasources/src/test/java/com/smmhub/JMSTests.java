package com.smmhub;

import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.commons.model.datasources.request.ParseRequest;
import com.smmhub.commons.model.dto.category.CatalogGroupDTO;
import com.smmhub.datasources.parsers.vk.VkontakteParserWorker;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("tests")
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.NONE)
public class JMSTests {

	@Autowired(required = false)
	private JmsTemplate jmsTemplate;

	@Mock
	private VkontakteParserWorker vkontakteParserWorker;

	@Captor
	private ArgumentCaptor<ParseRequest> requestArgumentCaptor;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testSendRequest() {

		List<CatalogGroupDTO> groups = new ArrayList<>();
		CatalogGroupDTO group = new CatalogGroupDTO();
		group.setName("test group");
		groups.add(group);

		ParseRequest request = new ParseRequest(SocialNetworkType.VK, groups);
		jmsTemplate.convertAndSend("parser.vk", request);

		// verify
		//verify(vkontakteParserWorker).receiveMessage(requestArgumentCaptor.capture());
		//assertEquals(request.getGroups().size(), 1);
	}
}