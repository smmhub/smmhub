package com.smmhub.datasources.socialapi;

import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.datasources.annotations.APILock;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.concurrent.Callable;

/**
 * @author Nikolay Viguro, 12.05.17
 */

@Component
public class APIGuard {

    @Setter
    @Getter
    private SocialNetworkType type;

    @APILock
    public Object sendAPIRequest(Callable<Object> callable) throws Exception {
        return callable.call();
    }
}
