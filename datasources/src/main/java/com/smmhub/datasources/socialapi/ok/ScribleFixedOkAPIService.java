package com.smmhub.datasources.socialapi.ok;

import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.model.*;
import com.github.scribejava.core.oauth.OAuth20Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.apache.commons.codec.digest.DigestUtils.md5Hex;

/**
 * @author Nikolay Viguro <nikolay.viguro@loyaltyplant.com>, 02.05.17
 */
public class ScribleFixedOkAPIService extends OAuth20Service {

    public ScribleFixedOkAPIService(DefaultApi20 api, OAuthConfig config) {
        super(api, config);
    }

    @Override
    public void signRequest(OAuth2AccessToken accessToken, OAuthRequest request) {
        //sig = lower(md5( sorted_request_params_composed_string + md5(access_token + application_secret_key)))
            final String tokenDigest = md5Hex(accessToken.getAccessToken() + getConfig().getApiSecret());

            ParameterList queryParams = request.getQueryStringParams();
            ParameterList bodyParams = request.getBodyParams();
            queryParams.addAll(bodyParams);
            Collections.sort(queryParams.getParams());

            List<String> params = new ArrayList<>();
            for(Parameter param : queryParams.getParams()) {
                params.add(param.getKey().concat("=").concat(param.getValue()));
            }

            final StringBuilder builder = new StringBuilder();
            for (String param : params) {
                builder.append(param);
            }

            String url = builder.toString();

            final String sigSource = url + tokenDigest;
            request.addQuerystringParameter("sig", md5Hex(sigSource).toLowerCase());

            super.signRequest(accessToken, request);

    }
}
