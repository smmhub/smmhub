package com.smmhub.datasources.socialapi.ok;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.smmhub.commons.consts.SocialCredentialsConsts;
import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.commons.exceptions.APIException;
import com.smmhub.commons.exceptions.APIRequestException;
import com.smmhub.commons.exceptions.posting.EmptyPostingDataException;
import com.smmhub.commons.model.datasources.*;
import com.smmhub.commons.model.datasources.request.ok.MediaTopicAttachmentRequest;
import com.smmhub.commons.model.datasources.response.ok.*;
import com.smmhub.datasources.SocialAPI;
import com.smmhub.datasources.annotations.APILock;
import com.smmhub.datasources.socialapi.APIGuard;
import com.smmhub.services.ImageService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.CharEncoding;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author nix (28.04.2017)
 */

@Component("okAPI")
@Scope("prototype") // Нам нужен всегда новый экземпляр этого бина, т.к. accessToken будет разный
@Qualifier("ok")
@Slf4j
public class OkAPI implements SocialAPI {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private APIGuard apiGuard;

	@Autowired
	private SocialCredentialsConsts socialCredentialsConsts;

	private OAuth2AccessToken accessToken;
	private boolean initialized = false;
	private Gson gson = new GsonBuilder().disableHtmlEscaping().create();
	private OAuth20Service service;

	private Pattern TOKEN_IN_UPLOAD_PATTERN = Pattern.compile("\"token\":\"(.*?)\"");

	private static final long MAX_IMAGE_SIZE = 20_971_520L; // 20 Mb

	@Override
	public Object init(Object userId, Object code) throws APIRequestException, APIException {
		service = new ServiceBuilder()
				.apiKey(socialCredentialsConsts.okLoginAppID)
				.apiSecret(socialCredentialsConsts.okLoginAppSecret)
				.callback(socialCredentialsConsts.okLoginCallback)
				.scope("GET_EMAIL;VALUABLE_ACCESS;LONG_ACCESS_TOKEN;GROUP_CONTENT")
				.build(ScribleFixedOkAPI.instance());
		try {
			this.accessToken = service.getAccessToken(code.toString());
		} catch (IllegalStateException | IOException | InterruptedException | ExecutionException e) {
			throw new APIRequestException("Something goes wrong with OK init " + e.getMessage());
		}
		apiGuard.setType(SocialNetworkType.OK);
		initialized = true;
		return service;
	}

	@Override
	public Object init(Object userId, Object accessToken, boolean force) throws APIRequestException, APIException {
		service = new ServiceBuilder()
				.apiKey(socialCredentialsConsts.okLoginAppID)
				.apiSecret(socialCredentialsConsts.okLoginAppSecret)
				.callback(socialCredentialsConsts.okLoginCallback)
				.scope("GET_EMAIL;VALUABLE_ACCESS;LONG_ACCESS_TOKEN;GROUP_CONTENT")
				.build(ScribleFixedOkAPI.instance());

		this.accessToken = new OAuth2AccessToken((String) accessToken);
		apiGuard.setType(SocialNetworkType.OK);
		initialized = true;
		return service;
	}

	@Override
	public Object init(Object userId, OAuth20Service service, Object code) throws APIRequestException, APIException {
		try {
			this.service = service;
			this.accessToken = service.getAccessToken(code.toString());
		} catch (IllegalStateException | IOException | InterruptedException | ExecutionException e) {
			throw new APIRequestException("Something goes wrong with OK init " + e.getMessage());
		}
		apiGuard.setType(SocialNetworkType.OK);
		initialized = true;
		return service;
	}

	@Override
	@APILock
	public Pageable<SocialGroup> getOwnedGroups(Pagination pagination) throws APIRequestException, APIException {

		if (pagination == null || pagination.getCount() > 50 || pagination.getCount() <= 0) {
			throw new IllegalArgumentException("pagination is incorrect");
		}

		List<SocialGroup> data = new ArrayList<>();

		String url = "https://api.ok.ru/fb.do" +
				"?application_key=" + socialCredentialsConsts.okDataAppPubKey +
				"&format=json" +
				"&method=group.getUserGroupsV2";

		if (!StringUtils.isEmpty(pagination.getAnchor())) {
			url = url.concat(pagination.asQueryString());
		}

		GroupsInfoResponse groupsInfoResponse;
		try {
			// 1. Сначала узнаем все группы, которые есть у юзера
			OAuthRequest request = new OAuthRequest(
					Verb.GET,
					url

			);
			service.signRequest(accessToken, request);
			Response response = service.execute(request);
			groupsInfoResponse = gson.fromJson(response.getBody(), GroupsInfoResponse.class);

			// 2. Теперь узнаем подробную инфу о группах
			OAuthRequest requestGroup = new OAuthRequest(
					Verb.POST,
					"https://api.ok.ru/fb.do" +
							"?application_key=" + socialCredentialsConsts.okDataAppPubKey +
							"&fields=DESCRIPTION,DELETE_ALLOWED,EDIT_ALLOWED,ACCESS_TYPE,PIC_AVATAR,UID,NAME,MEMBERS_COUNT,COUNTRY,CITY" +
							"&format=json" +
							"&method=group.getInfo"
			);

			// 3. Собираем все UID групп в один запрос
			Set<String> uids = new HashSet<>();
			for(GroupsInfoResponse.Group grp : groupsInfoResponse.getGroups()) {
				uids.add(grp.getGroupId());
			}
			requestGroup.addBodyParameter("uids", String.join(",", uids));

			service.signRequest(accessToken, requestGroup);
			Response responseGroup = service.execute(requestGroup);

			Type listType = new TypeToken<List<GroupResponse>>() {}.getType();
			List<GroupResponse> groupResponses = gson.fromJson(responseGroup.getBody(), listType);

			// 4. Парсим ответ
			for(GroupResponse full : groupResponses) {

				// Attrs в ответе, похоже, являются косвенным признаком того, что группа админится юзером
				if(full.getAttrs() != null) {
					SocialGroup group = new SocialGroup();

					group.setId(full.getUid());
					group.setName(full.getName());
					group.setPhoto(full.getPicAvatar());
					group.setDescription(full.getDescription());
					group.setMembersCount(full.getMembers_count());
					group.setCountry(full.getCountry());
					group.setCity(full.getCity());

					group.setNetworkType(SocialNetworkType.OK);
					group.setWrappedClass(full);

					data.add(group);
				}
			}

		} catch (IllegalStateException | InterruptedException | ExecutionException | IOException e) {
			throw new APIRequestException("API request error " + e.getMessage());
		}

		boolean hasNext = data.size() < pagination.getCount();
		return new Pageable<>(data, groupsInfoResponse.getAnchor(), hasNext);
	}

	@Override
	@APILock
	public Pageable<SocialGroupPost> getGroupPosts(String groupId, Pagination pagination) throws APIRequestException, APIException {

		if (pagination == null || pagination.getCount() > 50 || pagination.getCount() <= 0) {
			throw new IllegalArgumentException("pagination is incorrect");
		}

		List<SocialGroupPost> data = new ArrayList<>();

		String url = "https://api.ok.ru/fb.do" +
				"?application_key=" + socialCredentialsConsts.okDataAppPubKey +
				"&format=json" +
				"&gid=" + groupId +
				"&fields=feed.*,media_topic.*,group_photo.*" +
				"&patterns=POST" +
				"&method=stream.get";

		if (!StringUtils.isEmpty(pagination.getAnchor())) {
			url = url.concat(pagination.asQueryString());
		}

		GetStreamResponse streamResponse;
		try {
			OAuthRequest request = new OAuthRequest(
					Verb.GET,
					url
			);
			service.signRequest(accessToken, request);
			Response responseStream = service.execute(request);
			streamResponse = objectMapper.readValue(responseStream.getBody(), GetStreamResponse.class);

			Map<String, GetStreamResponse.Entity.GroupPhoto> photoMap = new HashMap<>();

			if(streamResponse.getEntities() != null) {
				for (GetStreamResponse.Entity.GroupPhoto photo : streamResponse.getEntities().getGroup_photos()) {
					photoMap.put(photo.getRef(), photo);
				}
			}
			else {
				throw new APIException("No entity in answer. Probably API in maintenance");
			}

			for(GetStreamResponse.Entity.MediaTopic media : streamResponse.getEntities().getMedia_topics()) {
				List<SocialGroupPost.PostAttachment> attachments = new ArrayList<>();
				boolean failedPost = false;

				SocialGroupPost post = new SocialGroupPost();
				post.setId(media.getId());
				post.setDate(new Date(media.getCreated_ms()));
				post.setComments(media.getDiscussion_summary().getComments_count());
				post.setLikes(media.getLike_summary().getCount());
				post.setReposts(media.getReshare_summary().getCount());

				if(media.getMedia() != null) {
					for (GetStreamResponse.Entity.MediaTopic.Media object : media.getMedia()) {
						if (object instanceof GetStreamResponse.Entity.MediaTopic.MediaText) {
							GetStreamResponse.Entity.MediaTopic.MediaText text = (GetStreamResponse.Entity.MediaTopic.MediaText) object;
							post.setText(text.getText());
						} else if (object instanceof GetStreamResponse.Entity.MediaTopic.MediaPhoto) {
							GetStreamResponse.Entity.MediaTopic.MediaPhoto photos = (GetStreamResponse.Entity.MediaTopic.MediaPhoto) object;

							for (String photoRef : photos.getPhoto_refs()) {
								GetStreamResponse.Entity.GroupPhoto groupPhoto = photoMap.get(photoRef);

								if (groupPhoto != null) {
									SocialGroupPost.PostAttachment attachment = new SocialGroupPost.PostAttachment();
									attachment.setAttachmentType(SocialGroupPost.AttachmentType.PHOTO);
									if(StringUtils.isEmpty(groupPhoto.getPicgif())) {
										if (checkImageSize(groupPhoto.getPic_max()) > MAX_IMAGE_SIZE) {
											failedPost = true;
											break;
										} else {
											attachment.setAttach(groupPhoto.getPic_max());
										}
									} else {
										if (checkImageSize(groupPhoto.getPicgif()) > MAX_IMAGE_SIZE) {
											failedPost = true;
											break;
										} else {
											attachment.setAttach(groupPhoto.getPicgif());
										}
									}

									if (attachment.getAttach() != null) {
										attachments.add(attachment);
									}
								}
							}
						} else {
							log.error("Unknown type mapping!");
						}
					}
				}

				post.setNetworkType(SocialNetworkType.OK);
				post.setAttachments(attachments);

				if(!failedPost) {
					data.add(post);
				}
			}
		} catch (IllegalStateException | InterruptedException | ExecutionException | IOException e) {
			throw new APIRequestException("API request error " + e.getMessage());
		}

		boolean hasNext = data.size() < pagination.getCount();
		return new Pageable<>(data, streamResponse.getAnchor(), hasNext);
	}

	@Override
	@APILock
	public SocialProfile getProfileInfo() throws APIRequestException, APIException {
		SocialProfile ret = new SocialProfile();

		ret.setAccessToken(accessToken.getAccessToken());

		try {
			OAuthRequest request = new OAuthRequest(
					Verb.GET,
					"https://api.ok.ru/fb.do" +
							"?application_key=" + socialCredentialsConsts.okDataAppPubKey +
							"&fields=EMAIL,NAME,GENDER,PIC50X50,BIRTHDAY" +
							"&format=json" +
							"&method=users.getCurrentUser"
			);
			service.signRequest(accessToken, request);
			Response response = service.execute(request);
			ProfileInfoResponse profileInfoResponse = gson.fromJson(response.getBody(), ProfileInfoResponse.class);

			ret.setUserId(profileInfoResponse.getUid());
			ret.setBirthday(profileInfoResponse.getBirthday());
			ret.setDisplayName(profileInfoResponse.getName());
			ret.setPhotoUrl(profileInfoResponse.getPic50x50());
			ret.setMale(profileInfoResponse.getGender().equals("male"));
			ret.setEmail(profileInfoResponse.getEmail());

			ret.setNetworkType(SocialNetworkType.OK);
			ret.setWrappedClass(profileInfoResponse);

		} catch (IllegalStateException | InterruptedException | ExecutionException | IOException e) {
			throw new APIRequestException("API request error " + e.getMessage());
		}

		return ret;
	}

	@Override
	public Object addPost(SocialGroupPost post) throws APIRequestException, APIException {

		String groupId = post.getGroupId();
		List<String> attach = new ArrayList<>();
		int count = 0;

		try {
			if (!CollectionUtils.isEmpty(post.getAttachments())) {
				for (SocialGroupPost.PostAttachment postAttachment : post.getAttachments()) {
					if (postAttachment.getAttachmentType().equals(SocialGroupPost.AttachmentType.PHOTO)
							&& postAttachment.getAttach() != null && postAttachment.getAttach() instanceof ImageService.Result) {
						count += 1;
					}
				}

				if(count > 0) {
					OAuthRequest uploadUrlRequest = new OAuthRequest(
							Verb.GET,
							"https://api.ok.ru/fb.do" +
									"?application_key=" + socialCredentialsConsts.okDataAppPubKey +
									"&gid=" + groupId +
									"&count=" + count +
									"&format=json" +
									"&method=photosV2.getUploadUrl"
					);
					service.signRequest(accessToken, uploadUrlRequest);
					Response uploadUrlresponse = (Response) apiGuard.sendAPIRequest(() -> service.execute(uploadUrlRequest));
					UploadUrlResponse uploadUrlResponse = gson.fromJson(uploadUrlresponse.getBody(), UploadUrlResponse.class);

					MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
					int i = 1;

					for (SocialGroupPost.PostAttachment postAttachment : post.getAttachments()) {
						if (postAttachment.getAttachmentType().equals(SocialGroupPost.AttachmentType.PHOTO)
								&& postAttachment.getAttach() != null && postAttachment.getAttach() instanceof ImageService.Result) {

							ImageService.Result attachResult = (ImageService.Result) postAttachment.getAttach();

							entityBuilder.addBinaryBody(
									"pic" + i,
									attachResult.getFile(),
									ContentType.create(attachResult.getMime().getType()),
									"pic" + i + "." + attachResult.getMime().getExtension());

							i++;
						}
					}

					HttpClient httpClient = HttpClientBuilder.create().build();
					HttpEntity entity = entityBuilder.build();
					HttpPost httpPost = new HttpPost(uploadUrlResponse.getUpload_url());
					httpPost.setEntity(entity);
					HttpResponse httpUploadResponse = httpClient.execute(httpPost);

					String uploadResponse = EntityUtils.toString(httpUploadResponse.getEntity())
							.replaceAll("\\\\u003d", "=");
					Matcher tokens = TOKEN_IN_UPLOAD_PATTERN.matcher(uploadResponse);

					if (tokens.find()) {
						attach.add(tokens.group(1));
					}
				}
			}

			MediaTopicAttachmentRequest attachmentRequest = new MediaTopicAttachmentRequest();

			// Добавляем фотки, если есть
			List<MediaTopicAttachmentRequest.Photo.PhotoId> photoIds = new ArrayList<>();
			for(String token : attach) {
				photoIds.add(new MediaTopicAttachmentRequest.Photo.PhotoId(token));
			}

			if(!CollectionUtils.isEmpty(photoIds)) {
				attachmentRequest.getMedia().add(new MediaTopicAttachmentRequest.Photo("photo", photoIds));
			}

			// Добавляем текст, если есть
			if(!StringUtils.isEmpty(post.getText())) {
				attachmentRequest.getMedia().add(new MediaTopicAttachmentRequest.Text("text", post.getText()));
			}

			if(CollectionUtils.isEmpty(attachmentRequest.getMedia())) {
				throw new EmptyPostingDataException("No attachments (photo or text) found in post request! Not sending");
			}

			String attachment = gson.toJson(attachmentRequest);
			String url = "https://api.ok.ru/fb.do" +
					"?application_key=" + socialCredentialsConsts.okDataAppPubKey +
					"&attachment=" +
					URLEncoder.encode(
							attachment,
							CharEncoding.UTF_8) +
					"&gid=" + groupId +
					"&type=GROUP_THEME" +
					"&format=json" +
					"&method=mediatopic.post";

			if (!post.isPostedFromGroup()) {
				url += "&uid=" + post.getFromId();
			}

			OAuthRequest postRequest = new OAuthRequest(
					Verb.GET,
					url
			);

			service.signRequest(accessToken, postRequest);
			Response postResponse = (Response) apiGuard.sendAPIRequest(() -> service.execute(postRequest));

			if (!StringUtils.isEmpty(postResponse.getHeader("invocation-error"))) {
				throw new APIRequestException("Invocation error: " + postResponse.getHeader("invocation-error"));
			}

			return postResponse.getBody();

		} catch (EmptyPostingDataException e) {
			throw new EmptyPostingDataException("API request error: " + e.getMessage());
		} catch (UnsupportedEncodingException e) {
			throw new APIRequestException("Unsupported encoding exception: " + e.getMessage());
		} catch (ClientProtocolException e) {
			throw new APIRequestException("Client protocol exception: " + e.getMessage());
		} catch (IOException e) {
			throw new APIRequestException("IO exception: " + e.getMessage());
		} catch (Exception e) {
			throw new APIRequestException("Generic API request error: " + e.getMessage());
		}
	}

	@Override
	public boolean isInitialized() {
		return initialized;
	}

	@Override
	public Object getLowLevelAccess() {
		return service;
	}

	@Override
	public void setLowLevelAccess(Object lowLevelAccess) {
		service = (OAuth20Service) lowLevelAccess;
	}

	@Override
	public SocialNetworkType getSocialNetworkType() {
		return SocialNetworkType.OK;
	}

	@Override
	public String getAccessToken() {
		return accessToken.getAccessToken();
	}

	private long checkImageSize(String urlString) {
		try {
			URL url = new URL(urlString);
			System.setProperty("http.agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0");
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod("HEAD");
			return (urlConnection).getContentLengthLong();
		} catch (IOException e) {
			log.error("Error while fetching image size. URL: {}", urlString);
		}
		return Long.MAX_VALUE;
	}
}
