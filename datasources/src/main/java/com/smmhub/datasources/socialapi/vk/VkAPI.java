package com.smmhub.datasources.socialapi.vk;

import com.github.scribejava.core.oauth.OAuth20Service;
import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.commons.exceptions.APIException;
import com.smmhub.commons.exceptions.APIRequestException;
import com.smmhub.commons.model.datasources.*;
import com.smmhub.datasources.SocialAPI;
import com.smmhub.datasources.annotations.APILock;
import com.smmhub.datasources.socialapi.APIGuard;
import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiAccessException;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ApiParamServerException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.groups.GroupFull;
import com.vk.api.sdk.objects.groups.responses.GetExtendedResponse;
import com.vk.api.sdk.objects.photos.Photo;
import com.vk.api.sdk.objects.photos.PhotoUpload;
import com.vk.api.sdk.objects.photos.responses.WallUploadResponse;
import com.vk.api.sdk.objects.users.UserXtrCounters;
import com.vk.api.sdk.objects.wall.WallpostAttachment;
import com.vk.api.sdk.objects.wall.WallpostAttachmentType;
import com.vk.api.sdk.objects.wall.WallpostFull;
import com.vk.api.sdk.objects.wall.responses.PostResponse;
import com.vk.api.sdk.queries.groups.GroupsGetFilter;
import com.vk.api.sdk.queries.users.UserField;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.io.File;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author nix (05.03.2017)
 */

@Component("vkAPI")
@Scope("prototype") // Нам нужен всегда новый экземпляр этого бина, т.к. accessToken будет разный
@Qualifier("vk")
@Slf4j
public class VkAPI implements SocialAPI {

	@Autowired
	private APIGuard apiGuard;

	@Getter
	private Integer userId;
	@Getter
	private String accessToken;

	private VkApiClient vk;
	private UserActor actor;
	private boolean initialized = false;

	@PostConstruct
	public void post() {
		TransportClient transportClient = HttpTransportClient.getInstance();
		vk = new VkApiClient(transportClient);
	}

	@Override
	public Object init(Object userId, Object accessToken) throws APIRequestException, APIException {
		this.userId = Integer.valueOf(userId.toString());
		this.accessToken = (String) accessToken;
		actor = new UserActor(this.userId, this.accessToken);
		apiGuard.setType(SocialNetworkType.VK);
		initialized = true;

		return vk;
	}

	@Override
	public Object init(Object userId, Object accessToken, boolean forceInit) throws APIRequestException, APIException {
		return null;
	}

	@Override
	public Object init(Object userId, OAuth20Service service, Object code) throws APIRequestException, APIException {
		return null;
	}

	@Override
	@APILock
	public Pageable<SocialGroup> getOwnedGroups(Pagination pagination) throws APIRequestException, APIException {

		if (pagination == null || pagination.getCount() > 200 || pagination.getCount() <= 0) {
			throw new IllegalArgumentException("pagination is incorrect");
		}

		List<SocialGroup> data = new ArrayList<>();

		try {
			GetExtendedResponse response = vk.groups().getExtended(actor)
					.userId(userId)
					.count(pagination.getCount())
					.filter(GroupsGetFilter.MODER)
					.execute();

			for (GroupFull full : response.getItems()) {
				SocialGroup group = new SocialGroup();

				group.setId(full.getId());
				group.setName(full.getName());
				group.setPhoto(full.getPhoto50());
				group.setDescription(full.getDescription());
				group.setMembersCount(full.getMembersCount());
				group.setCountry(full.getCountry() != null ? full.getCountry().getTitle() : null);
				group.setCity(full.getCity() != null ? full.getCity().getTitle() : null);
				group.setVerified(full.isVerified());

				group.setNetworkType(SocialNetworkType.VK);
				group.setWrappedClass(response);

				data.add(group);
			}
		} catch (ClientException e) {
			throw new APIRequestException("Client error " + e.getMessage());
		} catch (ApiException e) {
			throw new APIException("API error " + e.getMessage());
		}

		boolean hasNext = data.size() < pagination.getCount();
		return new Pageable<>(data, pagination.getOffset(), hasNext);
	}

	@Override
	@APILock
	public Pageable<SocialGroupPost> getGroupPosts(String groupId, Pagination pagination) throws APIRequestException, APIException {

		if (pagination == null || pagination.getCount() > 100 || pagination.getCount() <= 0) {
			throw new IllegalArgumentException("pagination is incorrect");
		}

		if (pagination.getOffset() < 0) {
			throw new IllegalArgumentException("Invalid offset number for getting group posts");
		}

		List<SocialGroupPost> data = new ArrayList<>();

		try {
			com.vk.api.sdk.objects.wall.responses.GetExtendedResponse response = vk.wall().getExtended(actor)
					// для групп должно быть с минусом
					.ownerId(-Integer.valueOf(groupId))
					.offset(pagination.getOffset())
					.count(pagination.getCount())
					.execute();

			for (WallpostFull full : response.getItems()) {
				SocialGroupPost post = new SocialGroupPost();

				post.setId(full.getId().toString());
				post.setDate(Date.from(Instant.ofEpochSecond(full.getDate())));
				post.setFromId(full.getFromId().toString());
				post.setText(full.getText());
				post.setLikes(full.getLikes().getCount());
				post.setReposts(full.getReposts().getCount());
				post.setComments(full.getComments().getCount());
				post.setPinned(full.getIsPinned() != null && full.getIsPinned() == 1);

				if (!CollectionUtils.isEmpty(full.getAttachments())) {
					List<SocialGroupPost.PostAttachment> attachments = new ArrayList<>(full.getAttachments().size());

					for (WallpostAttachment attachment : full.getAttachments()) {
						SocialGroupPost.PostAttachment postAttachment = new SocialGroupPost.PostAttachment();

						// TODO: 27.04.2017 добавить типы
						if (attachment.getType().equals(WallpostAttachmentType.PHOTO)) {
							postAttachment.setAttachmentType(SocialGroupPost.AttachmentType.PHOTO);

							String photo;
							if (attachment.getPhoto().getPhoto2560() != null) {
								photo = attachment.getPhoto().getPhoto2560();
							} else if (attachment.getPhoto().getPhoto1280() != null) {
								photo = attachment.getPhoto().getPhoto1280();
							} else if (attachment.getPhoto().getPhoto807() != null) {
								photo = attachment.getPhoto().getPhoto807();
							} else if (attachment.getPhoto().getPhoto604() != null) {
								photo = attachment.getPhoto().getPhoto604();
							} else if (attachment.getPhoto().getPhoto130() != null) {
								photo = attachment.getPhoto().getPhoto130();
							} else if (attachment.getPhoto().getPhoto75() != null) {
								photo = attachment.getPhoto().getPhoto75();
							} else {
								log.error("No image url for attachment. Post id #{}", post.getId());
								continue;
							}

							postAttachment.setAttach(photo);
							attachments.add(postAttachment);
						}
					}
					post.setAttachments(attachments);

					post.setNetworkType(SocialNetworkType.VK);
					post.setWrappedClass(full);
				}
				data.add(post);
			}
		} catch (ClientException e) {
			throw new APIRequestException("Client error " + e.getMessage());
		} catch (ApiException e) {
			throw new APIException("API error " + e.getMessage());
		}

		boolean hasNext = data.size() < pagination.getCount();
		return new Pageable<>(data, pagination.getOffset(), hasNext);
	}

	@Override
	@APILock
	public SocialProfile getProfileInfo() throws APIRequestException, APIException {
		SocialProfile ret = new SocialProfile();

		ret.setUserId(userId.toString());
		ret.setAccessToken(accessToken);

		try {
			UserXtrCounters response = vk.users().get(actor)
					.userIds(userId.toString())
					.fields(
							UserField.SEX,
							UserField.BDATE,
							UserField.SCREEN_NAME,
							UserField.PHOTO_50,
							UserField.CITY,
							UserField.NICKNAME
					)
					.execute().get(0);

			// 1 - female, 2 - male, 3 - unknown
			ret.setMale(response.getSex().getValue() == 2);
			ret.setPhotoUrl(response.getPhoto50());
			ret.setBirthday(response.getBdate());
			ret.setDisplayName(response.getFirstName() + " " + response.getLastName());

			ret.setNetworkType(SocialNetworkType.VK);
			ret.setWrappedClass(response);
		} catch (ClientException e) {
			throw new APIRequestException("Client error " + e.getMessage());
		} catch (ApiException e) {
			throw new APIException("API error " + e.getMessage());
		}
		return ret;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Object addPost(SocialGroupPost post) throws APIRequestException, APIException {

		try {
			Integer groupId = Integer.valueOf(post.getGroupId());
			List<String> attach = new ArrayList<>();

			if (!CollectionUtils.isEmpty(post.getAttachments())) {
				PhotoUpload photoUpload = (PhotoUpload) apiGuard.sendAPIRequest(
						() -> vk.photos().getWallUploadServer(actor).groupId(groupId).execute()
				);

				for (SocialGroupPost.PostAttachment postAttachment : post.getAttachments()) {
					if (postAttachment.getAttachmentType().equals(SocialGroupPost.AttachmentType.PHOTO)
							&& postAttachment.getAttach() != null && postAttachment.getAttach() instanceof File) {

						WallUploadResponse response = (WallUploadResponse) apiGuard.sendAPIRequest(
								() -> vk.upload().photoWall(
										photoUpload.getUploadUrl(),
										(File) postAttachment.getAttach()).execute()
						);
						List<Photo> photos = (List<Photo>) apiGuard.sendAPIRequest(
								() -> vk.photos()
										.saveWallPhoto(actor, response.getPhoto())
										.server(response.getServer())
										.hash(response.getHash())
										.groupId(Integer.valueOf(post.getGroupId()))
										.userId(Integer.valueOf(post.getFromId()))
										.execute()
						);

						for (Photo photo : photos) {
							attach.add("photo" + photo.getOwnerId() + "_" + photo.getId());
						}
					}
				}
			}

			Integer ownerId;
			if (post.isPostedFromGroup()) {
				ownerId = -groupId;
			} else {
				ownerId = Integer.valueOf(post.getFromId());
			}

			PostResponse postResponse = (PostResponse) apiGuard.sendAPIRequest(() ->
				vk.wall().post(actor)
						.attachments(attach)
						.fromGroup(post.isPostedFromGroup())
						.ownerId(ownerId)
						.message(post.getText())
						.execute());

			return postResponse.getPostId();

		} catch (NumberFormatException e) {
			throw new APIException("Number format exception: " + e.getMessage());
		} catch (Exception e) {
			if(e instanceof ClientException) {
				throw new APIRequestException("Client error: " + e.getMessage());
			}
			else if(e instanceof ApiAccessException) {
				throw new APIRequestException("API access error: " + e.getMessage());
			}
			else if(e instanceof ApiParamServerException) {
				throw new APIRequestException("API paramerters invalid: " + e.getMessage());
			}
			else if(e instanceof ApiException) {
				throw new APIException("API error: " + e.getMessage());
			}
			else {
				log.error("Unknown API error!", e);
				throw new APIException("Unknown API error: " + e.getMessage());
			}
		}
	}

	@Override
	public boolean isInitialized() {
		return initialized;
	}

	@Override
	public Object getLowLevelAccess() {
		return vk;
	}

	@Override
	public void setLowLevelAccess(Object lowLevelAccess) {
		vk = (VkApiClient) lowLevelAccess;
	}

	@Override
	public SocialNetworkType getSocialNetworkType() {
		return SocialNetworkType.VK;
	}
}
