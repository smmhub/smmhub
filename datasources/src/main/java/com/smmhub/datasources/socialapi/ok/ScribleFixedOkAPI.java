package com.smmhub.datasources.socialapi.ok;

import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.builder.api.OAuth2SignatureType;
import com.github.scribejava.core.model.OAuthConfig;
import com.github.scribejava.core.oauth.OAuth20Service;

/**
 * @author Nikolay Viguro <nikolay.viguro@loyaltyplant.com>, 02.05.17
 */
public class ScribleFixedOkAPI extends DefaultApi20 {

    protected ScribleFixedOkAPI() {
    }

    private static class InstanceHolder {
        private static final ScribleFixedOkAPI INSTANCE = new ScribleFixedOkAPI();
    }

    public static ScribleFixedOkAPI instance() {
        return ScribleFixedOkAPI.InstanceHolder.INSTANCE;
    }

    @Override
    public String getAccessTokenEndpoint() {
        return "https://api.ok.ru/oauth/token.do";
    }

    @Override
    protected String getAuthorizationBaseUrl() {
        return "https://connect.ok.ru/oauth/authorize";
    }

    @Override
    public OAuth20Service createService(OAuthConfig config) {
        return new ScribleFixedOkAPIService(this, config);
    }

    @Override
    public OAuth2SignatureType getSignatureType() {
        return OAuth2SignatureType.BEARER_URI_QUERY_PARAMETER;
    }
}
