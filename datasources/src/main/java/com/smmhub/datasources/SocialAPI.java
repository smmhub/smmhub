package com.smmhub.datasources;

import com.github.scribejava.core.oauth.OAuth20Service;
import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.commons.exceptions.APIException;
import com.smmhub.commons.exceptions.APIRequestException;
import com.smmhub.commons.model.datasources.*;

/**
 * @author nix (27.04.2017)
 *         Интерфейс верхнего уровня абстракции для работы с соцсетями
 */
public interface SocialAPI {

	/**
	 * Метод инициализации подключения к социальной сети
	 *
	 * @param userId      идентификатор юзера в соцсети
	 * @param accessToken токен юзера
	 * @return любой объект необходимый к дальнейшем
	 * @throws APIRequestException исключение
	 * @throws APIException исключение
	 */
	Object init(Object userId, Object accessToken) throws APIRequestException, APIException;

	/**
	 * Метод форсированного инициализации подключения к социальной сети
	 *
	 * @param userId          идентификатор юзера в соцсети
	 * @param accessToken     токен юзера
	 * @param forceInitialize флаг форсированной инициализации
	 * @return любой объект необходимый к дальнейшем
	 * @throws APIRequestException исключение
	 * @throws APIException        исключение
	 */
	Object init(Object userId, Object accessToken, boolean forceInitialize) throws APIRequestException, APIException;

	/**
	 * Метод форсированного инициализации подключения к социальной сети
	 *
	 * @param userId          идентификатор юзера в соцсети
	 * @param service       сконфигурированный сервис OAuth2
	 * @param code код
	 * @return любой объект необходимый к дальнейшем
	 * @throws APIRequestException исключение
	 * @throws APIException        исключение
	 */
	Object init(Object userId, OAuth20Service service, Object code) throws APIRequestException, APIException;


	/**
	 * Получить список администрируемых групп пользователя
	 *
	 * @return список групп
	 * @param pagination смещение (может быть как число, так и анчор (для Одноклассников) )
	 * @throws APIRequestException исключение
	 * @throws APIException исключение
	 */
	Pageable<SocialGroup> getOwnedGroups(Pagination pagination) throws APIRequestException, APIException;

	/**
	 * Получить список постов из указанной группы
	 *
	 * @param groupId идентификатор группы в соцсети
	 * @param pagination смещение (может быть как число, так и анчор (для Одноклассников) )
	 * @return список постов и их общее количество в группе
	 * @throws APIRequestException исключение
	 * @throws APIException исключение
	 */
	Pageable<SocialGroupPost> getGroupPosts(String groupId, Pagination pagination) throws APIRequestException, APIException;

	/**
	 * Получить информацию о профиле текущего юзера
	 *
	 * @return профиль
	 * @throws APIRequestException исключение
	 * @throws APIException исключение
	 */
	SocialProfile getProfileInfo() throws APIRequestException, APIException;

	/**
	 * Метод добавления поста в группу соцсети
	 *
	 * @param post данные о публикумом посте
	 * @throws APIRequestException исключение
	 * @throws APIException        исключение
	 */
	Object addPost(SocialGroupPost post) throws APIRequestException, APIException;

	/**
	 * Проверка инициализации подключения к соцсети
	 *
	 * @return состояние инициализации
	 */
	boolean isInitialized();

	/**
	 * Костыль для прямого доступа к API
	 *
	 * @return API
	 */
	Object getLowLevelAccess();

	/**
	 * Костыль для прямого доступа к API
	 */
	void setLowLevelAccess(Object lowLevelAccess);

	/**
	 * Получить тип соцсети
	 * @return тип соцсети
	 */
	SocialNetworkType getSocialNetworkType();

	/**
	 * Метод возвращает access token учетной записи с соцсети
	 * @return токен
	 */
	String getAccessToken();
}
