package com.smmhub.datasources.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author nix (07.05.2017)
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface APILock {
}