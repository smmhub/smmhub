package com.smmhub.datasources.services;

import com.smmhub.commons.service.AbstractRunningService;
import com.smmhub.commons.service.RunningService;
import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.broker.BrokerService;
import org.springframework.stereotype.Service;

/**
 * @author nix (17.11.2017)
 */

@Service("jmsBrokerManager")
@Slf4j
public class JMSBrokerManagerService extends AbstractRunningService implements RunningService {
	private BrokerService broker;
	private static final String ADDRESS = "vm://activemq-broker-smmhub";

	@Override
	@SuppressWarnings("Duplicates")
	public void onStartup() throws InterruptedException {
		log.info("JMS broker manager is initializing...");

		try {
			broker = new BrokerService();
			broker.setBrokerName("activemq-broker-smmhub");
			broker.setDataDirectory("dev/amqp");
			broker.addConnector(ADDRESS);
			broker.setUseJmx(false);
			broker.setUseShutdownHook(false);
			broker.start();
		} catch (Exception e) {
			log.error("Error while starting up AMQP broker: ", e);
		}
	}

	@Override
	public void onShutdown() {
		log.info("JMS broker manager is stopping...");

		try {
			broker.stop();
		} catch (Exception e) {
			log.error("Can't stop AMQP broker: ", e);
		}
	}

	@Override
	public void subscribe() throws Exception {

	}

	@Override
	public void run() throws Exception {

	}
}
