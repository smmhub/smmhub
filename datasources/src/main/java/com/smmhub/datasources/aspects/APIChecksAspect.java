package com.smmhub.datasources.aspects;

import com.smmhub.commons.cache.DistributedBackendCache;
import com.smmhub.commons.cache.DistributedBackendLock;
import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.datasources.SocialAPI;
import com.smmhub.datasources.annotations.APILock;
import com.smmhub.datasources.socialapi.APIGuard;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author nix on 30.03.2017.
 */

@Slf4j
@Aspect
@Component
public class APIChecksAspect {

	@Autowired
	private DistributedBackendLock<String> locks;

	@Autowired
	private DistributedBackendCache<SocialNetworkType, Long> executeTimes;

	@Around("execution(@com.smmhub.datasources.annotations.APILock * *(..)) && @annotation(apiLock)")
	public Object checks(ProceedingJoinPoint pjp, APILock apiLock) throws Throwable {

		long startTime = System.currentTimeMillis();
		boolean initialized = false;
		SocialNetworkType type = null;

		Object target = pjp.getTarget();
		if (target instanceof SocialAPI) {
			initialized = ((SocialAPI) target).isInitialized();
			type = ((SocialAPI) target).getSocialNetworkType();
		}
		else if(target instanceof APIGuard) {
			initialized = true;
			type = ((APIGuard) target).getType();
		}

		if (!initialized || type == null) {
			log.error("{} API not initialized yet or type error happens!", type != null ? type.name() : "Undefined");
			return null;
		}

		// Лочим, максимум на 30 секунд
		// Лок - distributed средствами hazelcast, ибо парсеры под нагрузкой будут вынесены на отдельные машины
		locks.lock(type.name(), 30);

		try {
			while (!isExecutePermitted(type)) {
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					log.error("Parser wait interrupted!");
					return null;
				}
			}

			Object ret = pjp.proceed();

			// OK возвращает ошибки в ответе в заголовке
			/*if(ret instanceof Response && type.equals(SocialNetworkType.OK)) {
				Response resp = (Response) ret;

				if(resp.getHeader("invocation-error") != null) {
					throw new APIException("Invocation error! Code: " + resp.getHeader("invocation-error"));
				}
			}*/

			return ret;
		}
		finally {
			if(locks.isLocked(type.name())) {
				locks.unlock(type.name());
			}
			long endTime = System.currentTimeMillis();
			executeTimes.put(type, endTime);
			log.info("Method " + pjp.getSignature().getName() + " running time: " + (endTime - startTime) + "ms");
		}
	}

	private boolean isExecutePermitted(SocialNetworkType networkType) {
		long now = System.currentTimeMillis();
		long last = executeTimes.get(networkType);

		switch (networkType) {
			// API OK разрешает безлимит, но может ругаться при флуде, поэтому не наглеем
			case OK:
			case VK: {
				// API VK разрешает 3 выполнения методов в секунду
				return now - last > 333;
			}
			default:
				log.error("Unknown network type: {}", networkType.name());
				return false;
		}
	}
}