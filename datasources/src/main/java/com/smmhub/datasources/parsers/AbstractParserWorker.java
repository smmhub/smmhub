package com.smmhub.datasources.parsers;

import com.smmhub.commons.Checksum;
import com.smmhub.commons.cache.DistributedBackendLock;
import com.smmhub.commons.model.datasources.SocialGroupPost;
import com.smmhub.commons.model.datasources.request.ParseRequest;
import com.smmhub.commons.model.machinelearning.ClassifyResult;
import com.smmhub.database.model.elastic.Post;
import com.smmhub.database.model.sql.CatalogCategory;
import com.smmhub.database.model.sql.CatalogGroup;
import com.smmhub.services.MachineLearningService;
import com.smmhub.services.ParsingGroupsService;
import com.smmhub.services.PostService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Nikolay Viguro
 */

@Component
@Slf4j
public abstract class AbstractParserWorker {

	@Autowired
	protected ParsingGroupsService parsingGroupsService;

	@Autowired
	protected DistributedBackendLock<String> locks;

	@Autowired
	private PostService postService;

	@Autowired
	protected MachineLearningService machineLearningService;

	@Async
	public abstract void receiveMessage(ParseRequest request) throws Exception;

	protected boolean parseAndInsert(Collection<SocialGroupPost> res, CatalogGroup group) throws IOException, NoSuchAlgorithmException {
		if (res.size() == 0) {
			return true;
		}

		for (SocialGroupPost item : res) {
			// если пост прибит к верху - пропускаем, там обычно треш и угар
			if (item.isPinned()) {
				continue;
			}

			// пропускаем кривые пустые посты
			if (CollectionUtils.isEmpty(item.getAttachments()) && StringUtils.isEmpty(item.getText())) {
				continue;
			}

			if (group.getLastParsedPostDate().after(item.getDate())) {
				return true;
			}

			try {
				if (postService.findBySocialPostId(item.getId()) != null) {
					log.debug("[{}] Post #{} already saved in ES", group.getName(), item.getId());
					continue;
				}
			} catch (IllegalArgumentException ex) {
				log.error("[{}] Possible duplicates found in ES", group.getName(), ex);
				continue;
			}

			String textMD5 = "";

			// тут мы проверяем есть ли такие же посты уже в базе по MD5 от текста
			// если пост - только картинка или длина текста меньше 100 символов, то эта проверка не канает
			if (!StringUtils.isEmpty(item.getText()) && item.getText().length() > 100) {
				textMD5 = Checksum.checksum(item.getText());
				Post checkDuplicate = postService.findByTextMD5(textMD5);

				// если есть, то проверяем, не надо ли добавить новых категорий к посту
				if (checkDuplicate != null) {
					boolean updated = false;

					for (Long catId : getParentIds(group.getCatalogCategory())) {
						if (!checkDuplicate.getCategoryIds().contains(catId)) {
							log.debug("[{}] Adding category {} to post #{}", group.getName(), catId, item.getId());
							checkDuplicate.getCategoryIds().add(catId);
							updated = true;
						}
					}

					if (updated) {
						postService.savePost(checkDuplicate);
					} else {
						log.debug("[{}] Duplicate post #{} found by text MD5", group.getName(), item.getId());
					}

					continue;
				}
			}

			Set<Long> categoryIds = getParentIds(group.getCatalogCategory());

			Post post = new Post(group.getSocialNetworkType(), item.getText());
			post.setPosted(item.getDate());
			post.setGroupId(group.getGroupId());
			post.setGroupPostId(String.valueOf(item.getId()));
			post.setGroupName(group.getName());
			post.setCategoryIds(categoryIds);
			post.setLikes(item.getLikes());
			post.setReposts(item.getReposts());
			post.setComments(item.getComments());
			post.setTextMD5(textMD5);
			post.setLanguage(group.getLanguage());

			if (item.getAttachments() != null) {
				for (SocialGroupPost.PostAttachment attachment : item.getAttachments()) {
					if (attachment.getAttachmentType().equals(SocialGroupPost.AttachmentType.PHOTO)
							&& attachment.getAttach() instanceof String) {
						//Image image = imageService.addImage(new URL(attachment.getUrl()));
						post.getImages().add((String) attachment.getAttach());
					}
				}
			}

			try {
				if(!StringUtils.isEmpty(item.getText())) {
					ClassifyResult result = machineLearningService.classifyMessage(item.getText());
					if (result.isSpamDetected()) {
						post.setFiltered(true);
					}
				}
			} catch (Exception e) {
				log.error("Exception happens while trying to classify message", e);
			}

			postService.savePost(post);
			log.debug("[{}] Post #{} saved in ES", group.getName(), item.getId());
		}

		return false;
	}

	protected LocalDate toLocalDate(Date date) {
		return date != null ? LocalDate.fromDateFields(date) : null;
	}

	private Set<Long> getParentIds(CatalogCategory category) {
		Set<Long> ret = new HashSet<>();
		ret.add(category.getId());

		while (category != null) {
			category = category.getParent();
			if (category != null)
				ret.add(category.getId());
		}

		return ret;
	}
}
