package com.smmhub.datasources.parsers.vk;

import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.commons.exceptions.APIException;
import com.smmhub.commons.exceptions.APIRequestException;
import com.smmhub.commons.exceptions.NoParsingUsersException;
import com.smmhub.commons.model.datasources.Pageable;
import com.smmhub.commons.model.datasources.Pagination;
import com.smmhub.commons.model.datasources.SocialGroupPost;
import com.smmhub.commons.model.datasources.request.ParseRequest;
import com.smmhub.commons.model.dto.category.CatalogGroupDTO;
import com.smmhub.database.model.sql.CatalogGroup;
import com.smmhub.database.model.sql.ParsingUser;
import com.smmhub.datasources.SocialAPI;
import com.smmhub.datasources.parsers.AbstractParserWorker;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Iterator;
import java.util.Objects;

/**
 * @author Nikolay Viguro
 */

@Component
@Slf4j
public class VkontakteParserWorker extends AbstractParserWorker {

	private static final String LOCK_NAME_BASE = "VK-GrabberJob-Group-";

	@Autowired
	@Qualifier("vk")
	private SocialAPI vkAPI;

	@JmsListener(destination = "parser.vk", containerFactory = "jmsFactory")
	@Async
	public void receiveMessage(ParseRequest request) throws Exception {

		log.info("Get VK parser request");

		if (!request.getNetwork().equals(SocialNetworkType.VK)) {
			log.error("Get VK parser request, but social network type is {}", request.getNetwork());
			return;
		}

		if (request.getGroups().size() == 0) {
			log.error("Get VK parser request, but no groups inside or no groups to parse");
			return;
		}

		ParsingUser user;
		try {
			user = parsingGroupsService.getRandomParsingUser(SocialNetworkType.VK);
		} catch (NoParsingUsersException e) {
			log.error("No such users to parse VK");
			return;
		}

		vkAPI.init(user.getUserId(), user.getToken());

		for (CatalogGroupDTO group : request.getGroups()) {

			// обновляем инфу о группе
			final CatalogGroup updatedGroup = parsingGroupsService.getGroup(group.getId());

			Thread groupParserThread = new Thread(() -> {
				if (locks.isLocked(LOCK_NAME_BASE + updatedGroup.getGroupId())) {
					log.warn("Get VK parser request, but group #{} already locked (another job is still running)", group.getGroupId());
					return;
				} else {
					locks.lock(LOCK_NAME_BASE + updatedGroup.getGroupId());
				}

				try {
					log.info("[{}] Start parsing group", updatedGroup.getName());

					Date lastPostDate = updatedGroup.getLastParsedPostDate();
					if (lastPostDate == null) {
						lastPostDate = new Date(0);
					}

					Pageable<SocialGroupPost> posts = vkAPI.getGroupPosts(updatedGroup.getGroupId(), new Pagination(10));

					if (posts.getData().size() == 0) {
						log.warn("[{}] No posts in group. Skipping.", updatedGroup.getName());
						return;
					}

					Date lastPostInGroup = null;
					int offset = 0;
					boolean hasPosts = true;

					Iterator<SocialGroupPost> iterator = posts.getData().iterator();
					SocialGroupPost post;
					while ((post = iterator.next()) != null) {
						if (post.isPinned()) {
							offset++;
						} else {
							lastPostInGroup = post.getDate();
							break;
						}
					}

					if (Objects.isNull(lastPostInGroup)) {
						log.info("[{}] Something goes wrong with detect last post in group", updatedGroup.getName());
						return;
					}

					if (ObjectUtils.equals(toLocalDate(lastPostDate), toLocalDate(lastPostInGroup))) {
						log.info("[{}] No new posts in group for parsing. Skipping.", updatedGroup.getName());
						return;
					}

					while (hasPosts) {
						// максимум по АПИ - это сто постов за раз
						log.info("[{}] Fetching next 100 posts for parsing", updatedGroup.getName());
						Pageable<SocialGroupPost> res = vkAPI.getGroupPosts(updatedGroup.getGroupId(), new Pagination(offset, 100));

						if (parseAndInsert(res.getData(), updatedGroup)) {
							log.info("[{}] Last new post found in group. Stopping.", updatedGroup.getName());
							hasPosts = false;
						} else {
							offset += 100;
						}
					}

					updatedGroup.setLastParsedPostDate(lastPostInGroup);
					parsingGroupsService.saveGroup(updatedGroup);

				} catch (APIRequestException e) {
					log.error("[{}] Client exception: {}", updatedGroup.getName(), e.getMessage());
				} catch (APIException e) {
					log.error("[{}] API exception: {}", updatedGroup.getName(), e.getMessage());
				} catch (NoSuchAlgorithmException e) {
					log.error("[{}] NoSuchAlgorithm exception: {}", updatedGroup.getName(), e.getMessage());
				} catch (IOException e) {
					log.error("[{}] IO exception: {}", updatedGroup.getName(), e.getMessage());
				} finally {
					locks.unlock(LOCK_NAME_BASE + updatedGroup.getGroupId());
				}
			});

			groupParserThread.setName("VK-ParserThread-Group-" + updatedGroup.getGroupId());
			groupParserThread.start();
		}
	}
}
