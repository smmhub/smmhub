package com.smmhub.scheduler.aspects;

import com.smmhub.scheduler.annotations.Logged;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.quartz.JobExecutionContext;
import org.springframework.stereotype.Component;

/**
 * @author nix on 26.02.2017.
 */

@Aspect
@Component
@Slf4j
public class LogTask {

	@Around(value = "execution(* com.smmhub.scheduler.jobs.*.*(..)) && @annotation(logged) && args(context)")
	public Object logTask(ProceedingJoinPoint pjp, Logged logged, JobExecutionContext context)
			throws Throwable {

		long startTime = System.currentTimeMillis();
		log.info("Job ** {} ** fired @ {}", context.getJobDetail().getKey().getName(), context.getFireTime());

		Object ret = pjp.proceed();

		long endTime = System.currentTimeMillis();
		log.info("Job {} running {}ms. Next job scheduled @ {}", context.getJobDetail().getKey().getName(),
				(endTime - startTime), context.getNextFireTime());

		return ret;
	}
}