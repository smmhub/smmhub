package com.smmhub.scheduler.services.impl;

import com.google.common.collect.Lists;
import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.commons.model.dto.category.CatalogGroupDTO;
import com.smmhub.commons.service.AbstractRunningService;
import com.smmhub.commons.service.RunningService;
import com.smmhub.scheduler.jobs.parsers.ok.OkGrabberJob;
import com.smmhub.scheduler.jobs.parsers.vk.VkGrabberJob;
import com.smmhub.scheduler.services.SchedulerService;
import com.smmhub.services.ParsingGroupsService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author nix (29.04.2017)
 */

@Service("parserManager")
@Slf4j
@DependsOn("schedulerService")
public class ParserManagerService extends AbstractRunningService implements RunningService {

	@Autowired
	private SchedulerService schedulerService;

	@Autowired
	private ParsingGroupsService parsingGroupsService;

	@Autowired
	private ModelMapper modelMapper;

	@Value("${scheduler.partition.delay:1}")
	private Integer partitionDelay;

	private Type targetListType = new TypeToken<List<CatalogGroupDTO>>() {}.getType();

	@Override
	@SuppressWarnings("Duplicates")
	public void onStartup() throws InterruptedException {
		log.info("Parser manager is initializing...");

		// Ждем пока шедулер не запустится окончательно
		// по идее, он уже должен быть запущен полностью, но перестрахуемся
		while (!schedulerService.isSuccessfullyInitialized()) {
			try {
				log.info("Parser manager wait while scheduler started...");
				Thread.sleep(1000L);
			} catch (InterruptedException e) {
				log.error("Interrupted exception when waiting while scheduler started up", e);
			}
		}

		// В один парсер будут отправляться 10 групп
		final int chunkSize = 10;

		for (SocialNetworkType type : SocialNetworkType.values()) {
			Class klass;
			switch (type) {
				case VK: {
					klass = VkGrabberJob.class;
					break;
				}
				case OK: {
					klass = OkGrabberJob.class;
					break;
				}
				default: {
					log.error("Job class not implemented for type {}", type.name());
					continue;
				}
			}

			List<CatalogGroupDTO> groups = modelMapper.map(
					Lists.newArrayList(parsingGroupsService.getGroupsByNetwork(type)),
					targetListType
			);
			int seconds = 0;

			for (List<CatalogGroupDTO> chunks : Lists.partition(groups, chunkSize)) {

				// равномерно размазываем таски по минуте
				// todo размазывать не только в пределах минуты, но и минут(часов, etc)
				seconds += partitionDelay;
				if(seconds > 59) {
					seconds = 0;
				}

				try {
					// В качестве идентификатора используем первый ID группы из пачки + 1 000 000.
					// WARNING! В случае изменения кол-ва групп нужно перешедулить все задачи этой соцсети
					// todo придумать реализацию получше
					schedulerService.initializeJob(
							klass,
							Collections.singletonMap("groups", chunks),
							Long.valueOf(chunks.get(0).getGroupId()) + 1_000_000L
					);

					schedulerService.checkAndScheduleCronExpression(
							Long.valueOf(chunks.get(0).getGroupId()) + 1_000_000L,
							type.name() + "-trigger-" + chunks.get(0).getGroupId(),
							seconds + " */5 * * * ?"
					);

				} catch (SchedulerException e) {
					log.error("Exception when initializing job", e);
				}
			}
		}
	}

	@Override
	public void onShutdown() {
		log.info("Parser manager is stopping...");

		Map<Long, JobDetail> jobDetailMap = schedulerService.getJobDetails();

		for (Long key : jobDetailMap.keySet()) {
			schedulerService.checkAndUnscheduleTrigger(key);
		}

		log.info("Parser manager stopped...");
	}

	@Override
	public void subscribe() throws Exception {

	}

	@Override
	public void run() throws Exception {

	}
}
