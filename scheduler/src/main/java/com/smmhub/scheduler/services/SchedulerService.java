package com.smmhub.scheduler.services;

import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.Trigger;

import java.util.List;
import java.util.Map;

/**
 * @author nix (26.02.2017)
 */
public interface SchedulerService {

	JobDetail buildJob(Class<?> jobClass, Long jobId);

	void getJobKeyAndCheckExist(JobDetail jobDetail, Long jobId) throws SchedulerException;

	void initializeJob(Class<?> jobClass, Long jobId) throws SchedulerException;

	void initializeJob(Class<?> jobClass, Map<String, Object> dataMap, Long jobId) throws SchedulerException;

	String checkAndUnscheduleTrigger(Long jobId);

	List<String> checkAndScheduleCronExpression(Long jobId, String triggerName, String cronExpression);

	boolean isSuccessfullyInitialized();

	String retrieveCronExpression(Long jobId);

	Map<Long, String> getCronExpressions();

	Map<Long, Trigger> getJobTriggers();

	Map<Long, JobDetail> getJobDetails();

	Map<Long, JobKey> getJobKeys();

	List<String> getCurrentlyExecutingJobs() throws SchedulerException;
}
