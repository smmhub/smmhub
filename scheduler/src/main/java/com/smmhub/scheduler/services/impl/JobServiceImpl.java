package com.smmhub.scheduler.services.impl;

import com.smmhub.commons.exceptions.JobNotFoundException;
import com.smmhub.database.dao.sql.JobDAO;
import com.smmhub.database.model.sql.Job;
import com.smmhub.scheduler.services.JobService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;

/**
 * @author Nikolay Viguro, 25.05.17
 */

@Service
public class JobServiceImpl implements JobService {

    @Autowired
    private JobDAO jobDAO;

    @Override
    @Transactional(readOnly = true)
    public List<Job> getAllJobs() {
        return (List<Job>) jobDAO.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Job> getAllEnabledJobs() {
        return jobDAO.findAllByEnabledTrue();
    }

    @Override
    @Transactional(readOnly = true)
    public Job getJob(Long id) throws JobNotFoundException {
        Job job = jobDAO.findById(id).orElseGet(null);;

        if(job == null) {
            throw new JobNotFoundException();
        }

        return job;
    }

    @Override
    @Transactional(readOnly = true)
    public Job getJob(String title) throws JobNotFoundException {
        if(StringUtils.isEmpty(title)) {
            throw new JobNotFoundException();
        }

        Job job = jobDAO.findByTitle(title);

        if(job == null) {
            throw new JobNotFoundException();
        }

        return job;
    }

    @Override
    @Transactional
    public Job saveJob(Job job) {
        return jobDAO.save(job);
    }

    @Override
    @Transactional
    public void removeJob(String title) throws JobNotFoundException {
        if(StringUtils.isEmpty(title)) {
            throw new JobNotFoundException();
        }

        Job job = jobDAO.findByTitle(title);

        if(job == null) {
            throw new JobNotFoundException();
        }

        job.setCategories(new HashSet<>());
        job = jobDAO.save(job);

        // fixme dirty hack
        jobDAO.deleteJobByQuery(job.getId());
    }
}
