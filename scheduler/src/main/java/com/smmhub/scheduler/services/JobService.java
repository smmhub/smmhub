package com.smmhub.scheduler.services;

import com.smmhub.commons.exceptions.JobNotFoundException;
import com.smmhub.database.model.sql.Job;

import java.util.List;

/**
 * @author nix (25.05.2017)
 */

public interface JobService {
	List<Job> getAllJobs();
	List<Job> getAllEnabledJobs();
	Job getJob(Long id) throws JobNotFoundException;
	Job getJob(String title) throws JobNotFoundException;
	Job saveJob(Job job);
	void removeJob(String title) throws JobNotFoundException;
}
