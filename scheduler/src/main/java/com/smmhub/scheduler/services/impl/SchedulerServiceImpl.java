package com.smmhub.scheduler.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smmhub.commons.model.jobs.JobPostingData;
import com.smmhub.scheduler.services.JobService;
import com.smmhub.scheduler.services.SchedulerService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author nix (26.02.2017)
 */

@Service("schedulerService")
@Slf4j
@DependsOn("jmsBrokerManager")
public class SchedulerServiceImpl implements SchedulerService {

	@Autowired
	@Qualifier("quartzScheduler")
	private SchedulerFactoryBean quartzScheduler;

	@Autowired
	private JobService jobService;

	@Autowired
	private ObjectMapper objectMapper;

	private Scheduler scheduler = null;
	private Map<Long, JobDetail> jobDetails = new ConcurrentHashMap<>();
	private Map<Long, Trigger> jobTriggers = new ConcurrentHashMap<>();
	private Map<Long, JobKey> jobKeys = new ConcurrentHashMap<>();
	private Map<Long, String> cronExpressions = new ConcurrentHashMap<>();

	private boolean successfullyInitialized = false;

	@PostConstruct
	private void initialize() {
		try {
			initializeScheduler();
			scheduleAllJobs();
			successfullyInitialized = true;
		} catch (SchedulerException e) {
			log.error("[scheduler] exception during initialization", e);
		}
	}

	private void initializeScheduler() throws SchedulerException {
		log.info("[scheduler] initializing scheduler...");
		scheduler = quartzScheduler.getScheduler();
		scheduler.start();
		log.info("[scheduler] scheduler initialized and started");
	}

	private void scheduleAllJobs() {
		log.info("[scheduler] scheduling all job from database...");

		for(com.smmhub.database.model.sql.Job sqlJob : jobService.getAllEnabledJobs()) {
			Map<String, Object> jobData = new HashMap<>();

			if(sqlJob.getData() != null) {
				try {
					jobData = Collections.singletonMap(
							"data",
							objectMapper.readValue(sqlJob.getData(), JobPostingData.class)
					);
				} catch (IOException ignored) {
				}
			}

			try {
				initializeJob(
						Class.forName(sqlJob.getKlass()),
						jobData,
						sqlJob.getId()
				);
			} catch (SchedulerException e) {
				log.error("[scheduler] Scheduler exception when initializing job", e);
			} catch (ClassNotFoundException e) {
				log.error("[scheduler] Job class not found when initializing job, default Job be initialized", e);
				try {
					initializeJob(
                            Job.class,
							jobData,
							sqlJob.getId()
                    );
				} catch (SchedulerException e1) {
					log.error("[scheduler] Fatal error while initializing job", e);
				}
			}

			checkAndScheduleCronExpression(
					sqlJob.getId(),
					sqlJob.getId() + "-trigger",
					sqlJob.getCron()
			);
			log.info("[scheduler] job {} scheduler for run", sqlJob.getTitle());
		}

		log.info("[scheduler] scheduling all job from database done");
	}

	@Override
	@SuppressWarnings("unchecked")
	public JobDetail buildJob(Class<?> jobClass, Long jobId) {
		JobDetail jobDetail = JobBuilder.newJob((Class<Job>) jobClass).withIdentity(jobId.toString()).storeDurably(true).build();
		jobDetails.put(jobId, jobDetail);
		return jobDetail;
	}

	@Override
	public void getJobKeyAndCheckExist(JobDetail jobDetail, Long jobId) throws SchedulerException {
		JobKey jobKey = jobDetail.getKey();
		jobKeys.put(jobId, jobKey);

		if (scheduler.checkExists(jobKey)) {
			jobDetail = scheduler.getJobDetail(jobKey);

			List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobDetail.getKey());

			if (triggers.size() > 0) {
				if (triggers.size() > 1) {
					resetTriggers(jobId, triggers);
				} else {
					Trigger trigger = triggers.get(0);
					jobTriggers.put(jobId, trigger);

					cronExpressions.put(jobId, ((CronTrigger) trigger).getCronExpression());
				}
			}
		} else {
			scheduler.addJob(jobDetail, false);
		}

	}

	@Override
	public void initializeJob(Class<?> jobClass, Long jobId) throws SchedulerException {
		log.info("[scheduler] initializing <{}>", jobId);
		JobDetail jobDetail = buildJob(jobClass, jobId);
		getJobKeyAndCheckExist(jobDetail, jobId);
		log.info("[scheduler] <{}> initialized", jobId);
	}

	@Override
	public void initializeJob(Class<?> jobClass, Map<String, Object> dataMap, Long jobId) throws SchedulerException {
		log.info("[scheduler] initializing <{}>", jobId);
		JobDetail jobDetail = buildJob(jobClass, jobId);
		jobDetail.getJobDataMap().putAll(dataMap);
		jobDetails.put(jobId, jobDetail);
		getJobKeyAndCheckExist(jobDetail, jobId);
		log.info("[scheduler] <{}> initialized", jobId);
	}

	private void resetTriggers(Long jobId, List<? extends Trigger> triggers) throws SchedulerException {
		log.info("[scheduler] resetting trigger <{}>", jobId);
		for (Trigger trigger : triggers) {
			scheduler.unscheduleJob(trigger.getKey());
		}
	}

	@Override
	public String checkAndUnscheduleTrigger(Long jobId) {
		Trigger trigger = jobTriggers.get(jobId);

		if (trigger != null) {
			TriggerKey triggerKey = trigger.getKey();

			log.info("[scheduler] unscheduling <{}>", triggerKey);

			try {
				scheduler.unscheduleJob(trigger.getKey());
				jobTriggers.remove(jobId);
				return triggerKey + " successfully unscheduled";
			} catch (SchedulerException e) {
				log.error("exception unscheduling <{}>", triggerKey);
				log.error("", e);
			}
		} else {
			log.info("[scheduler] trigger for <{}> not found", jobId);
		}
		return "[scheduler] exception unscheduling";
	}

	@Override
	public List<String> checkAndScheduleCronExpression(Long jobId, String triggerName, String cronExpression) {
		List<String> cronOutput = new ArrayList<>();
		Trigger trigger = jobTriggers.get(jobId);

		if (trigger == null) {
			trigger = TriggerBuilder.newTrigger()
					.withIdentity(triggerName)
					.withSchedule(CronScheduleBuilder.cronSchedule(cronExpression))
					.forJob(jobDetails.get(jobId)).build();

			try {
				cronOutput.add("scheduling #" + jobId + " with cron expression " + cronExpression);
				scheduler.scheduleJob(trigger);
				cronOutput.add("successfully scheduled: " + jobId);

				jobTriggers.put(jobId, trigger);
				cronExpressions.put(jobId, cronExpression);
			} catch (SchedulerException e) {
				cronOutput.add("exception scheduling: " + jobId);
				log.error("", e);
			}
		} else {
			cronOutput.add("trigger already exists for: " + jobId);
		}

		return cronOutput;
	}

	@Override
	public boolean isSuccessfullyInitialized() {
		return successfullyInitialized;
	}

	@Override
	public String retrieveCronExpression(Long jobId) {
		return cronExpressions.get(jobId);
	}

	@Override
	public Map<Long, String> getCronExpressions() {
		return cronExpressions;
	}

	@Override
	public Map<Long, Trigger> getJobTriggers() {
		return jobTriggers;
	}

	@Override
	public Map<Long, JobDetail> getJobDetails() {
		return jobDetails;
	}

	@Override
	public Map<Long, JobKey> getJobKeys() {
		return jobKeys;
	}

	@Override
	public List<String> getCurrentlyExecutingJobs() throws SchedulerException {
		List<String> jobDescriptions = new ArrayList<>();
		for (JobExecutionContext runningJobs : scheduler.getCurrentlyExecutingJobs()) {
			jobDescriptions.add(runningJobs.getJobDetail().getDescription());
		}
		return jobDescriptions;
	}
}
