package com.smmhub.scheduler.jobs;

import com.smmhub.scheduler.annotations.Logged;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

/**
 * @author nix (26.02.2017)
 */

@Component
@Slf4j
public class TestJob implements Job {
	@Logged
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.info("Test job invoked");
	}
}