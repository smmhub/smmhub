package com.smmhub.scheduler.jobs.parsers.vk;

import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.commons.model.datasources.request.ParseRequest;
import com.smmhub.commons.model.dto.category.CatalogGroupDTO;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author nix (04.03.2017)
 */

@Component
@NoArgsConstructor
@Slf4j
public class VkGrabberJob implements Job {

	@Autowired
	private JmsTemplate jmsTemplate;

	@Override
	@SuppressWarnings({"Duplicates", "unchecked"})
	public void execute(JobExecutionContext param) throws JobExecutionException {
		List<CatalogGroupDTO> groups = (List<CatalogGroupDTO>) param.getJobDetail().getJobDataMap().get("groups");
		ParseRequest request = new ParseRequest(SocialNetworkType.VK, groups);
		jmsTemplate.convertAndSend("parser.vk", request);
	}
}