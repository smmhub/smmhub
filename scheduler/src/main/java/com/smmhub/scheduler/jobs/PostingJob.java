package com.smmhub.scheduler.jobs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smmhub.commons.cache.DistributedBackendCache;
import com.smmhub.commons.enums.JobPostingResultStatus;
import com.smmhub.commons.exceptions.APIException;
import com.smmhub.commons.exceptions.APIRequestException;
import com.smmhub.commons.exceptions.JobNotFoundException;
import com.smmhub.commons.exceptions.NoPostsFoundException;
import com.smmhub.commons.exceptions.posting.EmptyPostingDataException;
import com.smmhub.commons.model.datasources.SocialGroupPost;
import com.smmhub.commons.model.dto.account.AccountDTO;
import com.smmhub.commons.model.dto.category.CatalogCategoryDTO;
import com.smmhub.commons.model.dto.job.JobDTO;
import com.smmhub.commons.model.dto.job.JobPostDTO;
import com.smmhub.commons.model.dto.social.SocialNetworkDTO;
import com.smmhub.commons.model.dto.social.SocialNetworkGroupDTO;
import com.smmhub.commons.model.jobs.JobPostingData;
import com.smmhub.database.model.elastic.Post;
import com.smmhub.database.model.sql.Job;
import com.smmhub.database.model.sql.JobPost;
import com.smmhub.datasources.SocialAPI;
import com.smmhub.scheduler.services.JobService;
import com.smmhub.scheduler.services.SchedulerService;
import com.smmhub.services.ImageService;
import com.smmhub.services.PostService;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author nix (16.11.2017)
 */

// todo переписать всё нахрен

@Component
@NoArgsConstructor
@Scope("prototype")
@Slf4j
public class PostingJob implements org.quartz.Job {

	@Value("${jobs.maximum.retries:15}")
	private Integer maxRetries;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private DistributedBackendCache<String, AccountDTO> accountsCache;

	@Autowired
	private PostService postService;

	@Autowired
	private JobService jobService;

	@Autowired
	private SchedulerService schedulerService;

	@Autowired
	private ImageService imageService;

	@Autowired
	private ApplicationContext context;

	@Override
	public void execute(JobExecutionContext param) throws JobExecutionException {
		JobPostingData jobPostingData = (JobPostingData) param.getJobDetail().getJobDataMap().get("data");

		if(jobPostingData == null || StringUtils.isEmpty(jobPostingData.getUserId())) {
			log.error("No user id attached to job data. Exiting");
			return;
		}

		AccountDTO user = accountsCache.get(jobPostingData.getUserId());

		if(user == null) {
			log.error("No user attached to job data. Exiting");
			return;
		}

		if(jobPostingData.getJobId() == null || jobPostingData.getJobId() == 0L) {
			log.error("No job id attached to job data. Exiting");
			return;
		}

		log.info("[Job#{}] Start executing post job", jobPostingData.getJobId());

		user.getSocialNetworks().parallelStream().forEach(
				network -> network.getSocialNetworkGroups().parallelStream().forEach(
						group -> {
							JobDTO job = group.getJobs()
									.parallelStream()
									.filter(groupJob -> groupJob.getId().equals(jobPostingData.getJobId()))
									.findFirst().orElse(null);

							if(job != null) {
								boolean failed = false, shouldSkipPost = false;
								JobPostingData data = null;

								try {
									data = objectMapper.readValue(job.getData(), JobPostingData.class);
									doPostingWork(job, data, group, network);
								} catch (APIException e) {
									log.error("[Job#{}] Got API error: {}", jobPostingData.getJobId(), e);
									failed = true;
								} catch (IOException e) {
									log.error("[Job#{}] Got IO error: {}", jobPostingData.getJobId(), e);
									failed = true;
								} catch (EmptyPostingDataException e) {
									log.error("[Job#{}] Got empty data error: {}", jobPostingData.getJobId(), e);
									failed = true;
									shouldSkipPost = true;
								} catch (APIRequestException e) {
									log.error("[Job#{}] Got API request error: {}", jobPostingData.getJobId(), e);
									failed = true;
								}

								Job jobDB;
								try {
									jobDB = jobService.getJob(job.getId());
								} catch (JobNotFoundException e) {
									log.error("[Job#{}] Can't get job from database - permanent error!", jobPostingData.getJobId());
									return;
								}

								if(failed) {
									// skip failed post
									if (shouldSkipPost) {
										log.error("[Job#{}] Failed execution, but skip post", jobPostingData.getJobId());
										Set<Long> catIds = job.getCategories()
												.stream()
												.map(CatalogCategoryDTO::getId)
												.collect(Collectors.toSet());

										Set<String> skipPostIds = job.getJobPosts()
												.stream()
												.map(JobPostDTO::getPostId)
												.collect(Collectors.toSet());

										// add last post to history
										jobDB.getJobPosts().add(
												JobPost.builder()
														.job(jobDB)
														.posted(new Date())
														.postId(data.getNextPostId()) // posted id
														.status(JobPostingResultStatus.SKIPPED)
														.build()
										);

										try {
											Post nextPost = postService.findRandomPostInCategories(catIds, skipPostIds);
											log.debug("[Job#{}] Next post is: #{}", jobPostingData.getJobId(), nextPost.getId());
										} catch (NoPostsFoundException e) {
											log.error("[Job#{}] Failed to found next post. Retry: {}/{}", jobPostingData.getJobId(), (jobDB.getRetries() + 1), maxRetries);
											jobDB.setRetries(jobDB.getRetries() + 1);
										}
									} else {
										log.error("[Job#{}] Failed execution. Retry: {}/{}", jobPostingData.getJobId(), (jobDB.getRetries() + 1), maxRetries);
										jobDB.setRetries(jobDB.getRetries() + 1);
									}

									if(jobDB.getRetries() > maxRetries) {
										jobDB.setRetries(0);
										jobDB.setEnabled(false); // turn off job

										// unschedule
										schedulerService.checkAndUnscheduleTrigger(jobDB.getId());
									}
								}
								else {
									// add last post to history
									jobDB.getJobPosts().add(
											JobPost.builder()
													.job(jobDB)
													.posted(new Date())
													.postId(data.getNextPostId()) // posted id
													.status(JobPostingResultStatus.SUCCESS)
													.build()
									);

									// update next post
									Set<Long> catIds = job.getCategories()
											.stream()
											.map(CatalogCategoryDTO::getId)
											.collect(Collectors.toSet());

									Set<String> skipPostIds = job.getJobPosts()
											.stream()
											.map(JobPostDTO::getPostId)
											.collect(Collectors.toSet());

									Post nextPost = null;
									try {
										nextPost = postService.findRandomPostInCategories(catIds, skipPostIds);
										log.debug("[Job#{}] Next post is: #{}", jobPostingData.getJobId(), nextPost.getId());
									} catch (NoPostsFoundException e) {
										log.error("[Job#{}] Failed to found next post. Retry: {}/{}", jobPostingData.getJobId(), (jobDB.getRetries() + 1), maxRetries);
										jobDB.setRetries(jobDB.getRetries() + 1);
									}

									try {
										if(nextPost != null) {
											data.setNextPostId(nextPost.getId());
											jobDB.setData(objectMapper.writeValueAsString(data));
										}
									} catch (IOException e) {
										log.error("[Job#{}] Got IO error: {}", jobPostingData.getJobId(), e);
									}

									if(jobDB.getRetries() > 0) {
										log.info("[Job#{}] Job restored", jobPostingData.getJobId());
										jobDB.setRetries(0);
									}
								}

								jobDB = jobService.saveJob(jobDB);

								// update in cache
								Long jobId = job.getId();
								for(SocialNetworkDTO networkDTO : user.getSocialNetworks()) {
									for(SocialNetworkGroupDTO groupDTO : networkDTO.getSocialNetworkGroups()) {
										for(JobDTO jobDTO : groupDTO.getJobs()) {
											if(jobDTO.getId().equals(jobId)) {
												Set<JobDTO> jobsWithOutOld = groupDTO.getJobs().
														stream()
														.filter(groupJob -> !groupJob.getId().equals(jobId))
														.collect(Collectors.toSet());
												jobsWithOutOld.add(modelMapper.map(jobDB, JobDTO.class));

												groupDTO.setJobs(jobsWithOutOld);
												break;
											}
										}
									}
								}

								String userId = user.getEmail() + "-" + user.getLoginType().name();
								accountsCache.put(userId, user);
							}
						}
				)
		);

		log.info("[Job#{}] Execute done", jobPostingData.getJobId());
	}

	private void doPostingWork(JobDTO job, JobPostingData jobPostingData, SocialNetworkGroupDTO group,
                               SocialNetworkDTO network) throws APIException, APIRequestException, IOException {
		if(job == null) {
			log.error("[Job#{}] No job found by id specified in job data. Exiting", jobPostingData.getJobId());
			return;
		}

		SocialAPI api;

		switch (network.getSocialNetworkType()) {
			case VK:
				api = (SocialAPI) context.getBean("vkAPI");
				api.init(network.getUserId(), network.getToken());
				break;
			case OK:
				api = (SocialAPI) context.getBean("okAPI");
				api.init(network.getUserId(), network.getToken(), true);
				break;
			default:
				log.error("No API for network type: {}", network.getSocialNetworkType());
				return;
		}

		if(!api.isInitialized()) {
			log.error("Failed to initialize API for network {}", network.getSocialNetworkType());
			return;
		}

		JobPostingData data = objectMapper.readValue(job.getData(), JobPostingData.class);
		Post post = postService.findById(data.getNextPostId());

		if(post == null) {
			log.debug("[Job#{}] No posts found for this categories. Skipping job", jobPostingData.getJobId());
			return;
		}

		log.debug("[Job#{}] Posting #{} to group", jobPostingData.getJobId(), post.getId());

		SocialGroupPost groupPost = new SocialGroupPost();
		groupPost.setGroupId(group.getSocialGroupId());
		groupPost.setText(post.getText());

		// add images
		List<SocialGroupPost.PostAttachment> attachments = new ArrayList<>();

		post.getImages().forEach(url-> {
			log.debug("[Job#{}] Loading image from {}", jobPostingData.getJobId(), url);

			ImageService.Result image = imageService.getImage(url);

			if(image == null) {
				log.error("[Job#{}] Failed to load image from {}", jobPostingData.getJobId(), url);
			} else {
				SocialGroupPost.PostAttachment attachment = new SocialGroupPost.PostAttachment();
				attachment.setAttachmentType(SocialGroupPost.AttachmentType.PHOTO);
				attachment.setAttach(image);

				log.debug("[Job#{}] Loading for {} done", jobPostingData.getJobId(), url);
				attachments.add(attachment);
			}
		});

		groupPost.setAttachments(attachments);

		try {
			api.addPost(groupPost);
		}
		finally {
			if(attachments.size() > 0) {
				log.debug("[Job#{}] Deleting images", jobPostingData.getJobId());
				attachments.forEach(attach -> {
					ImageService.Result imageResult = (ImageService.Result) attach.getAttach();
					imageService.removeImage(imageResult.getFile());
				});
				log.debug("[Job#{}] Deleting images done", jobPostingData.getJobId());
			}
		}
	}
}