package com.smmhub;

import com.smmhub.scheduler.jobs.TestJob;
import com.smmhub.scheduler.services.SchedulerService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("tests")
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class SchedulerTests {

	@Autowired
	private SchedulerService schedulerService;

	@Test
	public void testScheduleTask() throws Exception {

		Assert.assertTrue(schedulerService.isSuccessfullyInitialized());

		schedulerService.initializeJob(TestJob.class, 1L);
		schedulerService.checkAndScheduleCronExpression(1L, "TestJobTrigger", "0 * * * * ?");

		Assert.assertEquals(schedulerService.getCronExpressions().size(), 1);
	}
}