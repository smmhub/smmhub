package com.smmhub.database.dao.sql;

import com.smmhub.database.model.sql.CatalogCategory;
import org.springframework.data.repository.CrudRepository;

/**
 * @author nix (04.03.2017)
 */

public interface CatalogDAO extends CrudRepository<CatalogCategory, Long> {
	CatalogCategory findByName(String name);
}
