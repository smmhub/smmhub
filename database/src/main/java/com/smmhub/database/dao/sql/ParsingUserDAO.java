package com.smmhub.database.dao.sql;

import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.database.model.sql.ParsingUser;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

/**
 * @author nix (08.07.2017)
 */

public interface ParsingUserDAO extends CrudRepository<ParsingUser, Long> {
	Set<ParsingUser> findBySocialNetworkType(SocialNetworkType socialNetworkType);
}
