package com.smmhub.database.dao.sql;

import com.smmhub.commons.enums.MachineLearningResult;
import com.smmhub.database.model.sql.TrainingEntry;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author nix (04.03.2017)
 */

public interface MachineLearningDAO extends CrudRepository<TrainingEntry, Long> {
	List<TrainingEntry> findAllByType(MachineLearningResult type);
}
