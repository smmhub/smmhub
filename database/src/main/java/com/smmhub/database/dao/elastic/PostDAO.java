package com.smmhub.database.dao.elastic;

import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.database.model.elastic.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

/**
 * @author nix (26.02.2017)
 */
public interface PostDAO extends ElasticsearchRepository<Post, String> {
    Page<Post> findByIdIn(Set<String> ids, Pageable pageable);
	List<Post> findBySocialNetworkType(SocialNetworkType type);
	Page<Post> findByCategoryIdsInOrderByPostedDesc(Set<Long> categoryIds, Pageable pageable);
	Post findByGroupPostId(String postId);
	Post findByTextMD5(String md5);
	Long countBySocialNetworkType(SocialNetworkType type);

	@Query("{" +
			"    \"bool\": {" +
			"      \"filter\": [" +
			"        {" +
			"            \"terms\" : {\"categoryIds\" : ?0}" +
			"        }" +
			"      ]," +
			"      \"must\": {" +
			"          \"function_score\": {" +
			"            \"functions\": [" +
			"                {" +
			"                    \"random_score\": {" +
			"                        \"seed\": ?1" +
			"                    }" +
			"                }" +
			"            ]," +
			"            \"boost_mode\": \"replace\"" +
			"          }" +
			"        }" +
			"      }" +
			"}")
	Page<Post> getRandomPostByCategoryIds(@Param("ids") Set<Long> ids, @Param("seed") int seed, Pageable pageable);
}
