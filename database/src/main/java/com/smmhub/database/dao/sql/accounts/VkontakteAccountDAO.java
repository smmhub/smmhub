package com.smmhub.database.dao.sql.accounts;

import com.smmhub.database.model.sql.accounts.types.VkontakteAccount;
import org.springframework.data.repository.CrudRepository;

/**
 * @author nix (24.02.2017)
 */

public interface VkontakteAccountDAO extends CrudRepository<VkontakteAccount, Long> {
	VkontakteAccount findByEmail(String email);
	VkontakteAccount findByToken(String token);
}
