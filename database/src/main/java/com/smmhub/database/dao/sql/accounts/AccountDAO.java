package com.smmhub.database.dao.sql.accounts;

import com.smmhub.commons.enums.AccountLoginType;
import com.smmhub.database.model.sql.accounts.Account;
import org.springframework.data.repository.CrudRepository;

/**
 * @author nix (24.02.2017)
 */

public interface AccountDAO extends CrudRepository<Account, Long> {
	Account findByEmail(String email);
	Account findByEmailAndLoginType(String email, AccountLoginType type);
}
