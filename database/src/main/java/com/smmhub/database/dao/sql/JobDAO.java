package com.smmhub.database.dao.sql;

import com.smmhub.database.model.sql.Job;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author nix (25.05.2017)
 */

public interface JobDAO extends CrudRepository<Job, Long> {
	Job findByTitle(String name);
	List<Job> findAllByEnabledTrue();

	@Modifying
	@Query(value = "delete from Job j where id = ?1")
	void deleteJobByQuery(Long id);
}
