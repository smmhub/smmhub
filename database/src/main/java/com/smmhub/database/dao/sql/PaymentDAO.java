package com.smmhub.database.dao.sql;

import com.smmhub.database.model.sql.Payment;
import org.springframework.data.repository.CrudRepository;

/**
 * @author nix (24.02.2017)
 */
public interface PaymentDAO extends CrudRepository<Payment, Long> {
}
