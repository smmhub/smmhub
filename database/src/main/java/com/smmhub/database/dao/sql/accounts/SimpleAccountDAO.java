package com.smmhub.database.dao.sql.accounts;

import com.smmhub.database.model.sql.accounts.types.SimpleAccount;
import org.springframework.data.repository.CrudRepository;

/**
 * @author nix (24.02.2017)
 */

public interface SimpleAccountDAO extends CrudRepository<SimpleAccount, Long> {
	SimpleAccount findByEmail(String email);
}
