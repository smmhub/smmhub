package com.smmhub.database.dao.sql;

import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.database.model.sql.CatalogGroup;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

/**
 * @author nix (08.07.2017)
 */

public interface ParsingGroupsDAO extends CrudRepository<CatalogGroup, Long> {
	Set<CatalogGroup> findBySocialNetworkType(SocialNetworkType socialNetworkType);
	CatalogGroup findByGroupId(String groupId);
}
