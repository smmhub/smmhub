package com.smmhub.database.dao.sql.accounts;

import com.smmhub.database.model.sql.accounts.types.OkAccount;
import org.springframework.data.repository.CrudRepository;

/**
 * @author nix (01.05.2017)
 */

public interface OkAccountDAO extends CrudRepository<OkAccount, Long> {
	OkAccount findByEmail(String email);
	OkAccount findByToken(String token);
}
