package com.smmhub.database.model.sql;

import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.database.model.sql.accounts.Account;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Lycan (25.02.2017)
 */

@Entity
@Table(name = "social_networks")
@NoArgsConstructor
@Getter
@Setter
public class SocialNetwork implements Serializable {

	private static final long serialVersionUID = 82L;

	@GeneratedValue
	@Id
	private Long id;

	@Column(name = "social_network_type", columnDefinition = "Integer(2)", nullable = false)
	@Enumerated(value = EnumType.ORDINAL)
	private SocialNetworkType socialNetworkType;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_account_social_networks"), name = "account_id")
	private Account account;

	// тут хранится как зовут юзера, под которым подключена данная соцсеть
	private String connecterUserName;

	@Column(name = "user_id", nullable = false)
	@Lob
	private String userId;

	@Column(name = "token", nullable = false)
	@Lob
	private String token;

	@Temporal(TemporalType.TIMESTAMP)
	private Date tokenExpired;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "socialNetwork", orphanRemoval = true)
	private Set<SocialNetworkGroup> socialNetworkGroups = new HashSet<>();

	public SocialNetwork(SocialNetworkType socialNetworkType, String connecterUserName, String userId, String token) {
		this.socialNetworkType = socialNetworkType;
		this.connecterUserName = connecterUserName;
		this.userId = userId;
		this.token = token;
	}
}
