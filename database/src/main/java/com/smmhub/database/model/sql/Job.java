package com.smmhub.database.model.sql;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Nikolay Viguro, 25.05.17
 */

@Entity
@Table(name = "jobs")
@NoArgsConstructor
@Getter
@Setter
public class Job implements Serializable {
    private static final long serialVersionUID = 3293L;

    @GeneratedValue
    @Id
    private Long id;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(foreignKey = @ForeignKey(name = "fk_group_jobs"), name = "group_id")
    private SocialNetworkGroup socialNetworkGroup;

	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
	@JoinTable(name = "jobs_categories",
			joinColumns = @JoinColumn(name = "jobs_id"),
			inverseJoinColumns = @JoinColumn(name = "categories_id")
	)
	private Set<CatalogCategory> categories = new HashSet<>();

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "job", orphanRemoval = true)
	@OrderBy("posted DESC")
	private Set<JobPost> jobPosts = new HashSet<>();

    private String title;

    @Lob
    private String description;

    // Выполняемый класс job'а
    private String klass;

    // Данные
    @Lob
    private String data;

    // Если задачу надо запускать по расписанию
    private String cron;

    // Если задачу надо запустить только один раз
    @Temporal(TemporalType.TIMESTAMP)
    private Date oneTimeRun;

    private Boolean enabled = true;

    // Если задача - one-shot, то это признак, что она корректно отработала
    // если время oneTimeRun прошло, а тут false, то надо перезапустить в ближайшее время
    private Boolean finished = false;

    // Счетчик повторных запусков для oneTimeRun (для отсечки каких то мертвых задач)
    private Integer retries = 0;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Job job = (Job) o;

		if (id != null ? !id.equals(job.id) : job.id != null) return false;
		if (created != null ? !created.equals(job.created) : job.created != null) return false;
		if (updated != null ? !updated.equals(job.updated) : job.updated != null) return false;
		if (socialNetworkGroup != null ? !socialNetworkGroup.equals(job.socialNetworkGroup) : job.socialNetworkGroup != null)
			return false;
		if (title != null ? !title.equals(job.title) : job.title != null) return false;
		if (description != null ? !description.equals(job.description) : job.description != null) return false;
		if (klass != null ? !klass.equals(job.klass) : job.klass != null) return false;
		if (cron != null ? !cron.equals(job.cron) : job.cron != null) return false;
		if (oneTimeRun != null ? !oneTimeRun.equals(job.oneTimeRun) : job.oneTimeRun != null) return false;
		if (enabled != null ? !enabled.equals(job.enabled) : job.enabled != null) return false;
		return finished != null ? finished.equals(job.finished) : job.finished == null;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (created != null ? created.hashCode() : 0);
		result = 31 * result + (updated != null ? updated.hashCode() : 0);
		result = 31 * result + (socialNetworkGroup != null ? socialNetworkGroup.hashCode() : 0);
		result = 31 * result + (title != null ? title.hashCode() : 0);
		result = 31 * result + (description != null ? description.hashCode() : 0);
		result = 31 * result + (klass != null ? klass.hashCode() : 0);
		result = 31 * result + (cron != null ? cron.hashCode() : 0);
		result = 31 * result + (oneTimeRun != null ? oneTimeRun.hashCode() : 0);
		result = 31 * result + (enabled != null ? enabled.hashCode() : 0);
		result = 31 * result + (finished != null ? finished.hashCode() : 0);
		return result;
	}
}
