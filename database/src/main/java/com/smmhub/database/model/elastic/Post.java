package com.smmhub.database.model.elastic;

import com.smmhub.commons.enums.Language;
import com.smmhub.commons.enums.SocialNetworkType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author nix (26.02.2017)
 */

@Document(indexName = "posts", type = "post", shards = 1, replicas = 0, refreshInterval = "-1")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Post {

	@Id
	private String id;
	private Date posted;
	private SocialNetworkType socialNetworkType;
	private String text;

	private Language language;

	private Set<Long> categoryIds;

	private String groupPostId;
	private String groupId;
	private String groupName;

	private Integer likes;
	private Integer reposts;
	private Integer comments;

	// URL картинок, чтобы не кидать дубликаты
	private Set<String> images = new HashSet<>();

	// MD5 от текста для быстрого поиска дубликатов в базе
	private String textMD5;

	// признак отмодерированности
	private Boolean moderated = false;

	// признак того, что байесов фильтр пометил его, как возможный рекламный пост
	private Boolean filtered = false;

	// признак того, что юзер пометил его, как рекламный пост
	private Boolean userMarkAsSpam = false;

	public Post(SocialNetworkType type, String text) {
		this.socialNetworkType = type;
		this.text = text;
	}
}
