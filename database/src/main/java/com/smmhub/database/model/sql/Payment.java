package com.smmhub.database.model.sql;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smmhub.commons.enums.PaymentType;
import com.smmhub.commons.serialization.MoneySerialize;
import com.smmhub.database.model.sql.accounts.Account;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author nix (24.02.2017)
 */

@Entity
@Table(name = "payments")
@NoArgsConstructor
@Getter
@Setter
public class Payment implements Serializable {

	private static final long serialVersionUID = 72L;

	@GeneratedValue
	@Id
	private Long id;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date date;

	// Аккаунт платежа
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_payments_accounts"), name = "account_id")
	private Account account;

	// Тип изменения баланса
	@Enumerated(value = EnumType.STRING)
	private PaymentType type = PaymentType.UNKNOWN;

	// Изменение баланса
	@Column(name = "balancechange", precision = 10, scale = 2, nullable = false)
	@JsonSerialize(using = MoneySerialize.class)
	private BigDecimal change;

	public Payment(Account account, PaymentType type, BigDecimal change) {
		this.account = account;
		this.type = type;
		this.change = change;
	}
}
