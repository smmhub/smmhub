package com.smmhub.database.model.sql;

import com.smmhub.commons.converters.JpaConverterJson;
import com.smmhub.commons.enums.SocialNetworkType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * @author Lycan (25.02.2017)
 * @author nix (05.03.2017)
 */

@Entity
@Table(name = "social_network_groups",
	uniqueConstraints = {
		@UniqueConstraint( columnNames = { "social_group_id", "social_network_type" },
						   name = "uk_social_group_id")
})
@NoArgsConstructor
@Getter
@Setter
public class SocialNetworkGroup implements Serializable {
	private static final long serialVersionUID = -3912146089890183682L;

	@GeneratedValue
	@Id
	private Long id;

	@Column(name = "social_network_type", columnDefinition = "Integer(2)", nullable = false)
	@Enumerated(value = EnumType.ORDINAL)
	private SocialNetworkType socialNetworkType;

	@Column(name = "social_group_id", nullable = false)
	private String socialGroupId;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date lastPostedAt;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_social_network"), name = "social_network_id", nullable = false)
	private SocialNetwork socialNetwork;

	private String name;
	private String logo;

	@Temporal(TemporalType.TIMESTAMP)
	private Date tokenExpired;

	// тут будет храниться расписание, когда надо постить в группу
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "socialNetworkGroup", orphanRemoval = true)
	private Set<Job> jobs;

	@Column(name = "additional_json_data", columnDefinition = "text")
	@Convert(converter = JpaConverterJson.class)
	private Object addtionalJsonData;

	private Boolean enabled;
	private Boolean banned;

	public SocialNetworkGroup(SocialNetwork socialNetwork, SocialNetworkType socialNetworkType) {
		this.socialNetwork = socialNetwork;
		this.socialNetworkType = socialNetworkType;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SocialNetworkGroup that = (SocialNetworkGroup) o;

		if (id != null ? !id.equals(that.id) : that.id != null) return false;
		if (socialNetworkType != that.socialNetworkType) return false;
		if (socialGroupId != null ? !socialGroupId.equals(that.socialGroupId) : that.socialGroupId != null)
			return false;
		if (createdDate != null ? !createdDate.equals(that.createdDate) : that.createdDate != null) return false;
		if (socialNetwork != null ? !socialNetwork.equals(that.socialNetwork) : that.socialNetwork != null)
			return false;
		if (name != null ? !name.equals(that.name) : that.name != null) return false;
		if (logo != null ? !logo.equals(that.logo) : that.logo != null) return false;
		if (tokenExpired != null ? !tokenExpired.equals(that.tokenExpired) : that.tokenExpired != null) return false;
		return addtionalJsonData != null ? addtionalJsonData.equals(that.addtionalJsonData) : that.addtionalJsonData == null;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (socialNetworkType != null ? socialNetworkType.hashCode() : 0);
		result = 31 * result + (socialGroupId != null ? socialGroupId.hashCode() : 0);
		result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
		result = 31 * result + (socialNetwork != null ? socialNetwork.hashCode() : 0);
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (logo != null ? logo.hashCode() : 0);
		result = 31 * result + (tokenExpired != null ? tokenExpired.hashCode() : 0);
		result = 31 * result + (addtionalJsonData != null ? addtionalJsonData.hashCode() : 0);
		return result;
	}
}
