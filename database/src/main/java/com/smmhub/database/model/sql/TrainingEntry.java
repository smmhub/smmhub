package com.smmhub.database.model.sql;

import com.smmhub.commons.enums.MachineLearningResult;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Nikolay Viguro, 20.09.17
 */

@Entity
@Table(name = "machinelearning_trainingdata", uniqueConstraints = @UniqueConstraint(name = "message_unique", columnNames = "message"))
@NoArgsConstructor
@Getter
@Setter
public class TrainingEntry implements Serializable {
    private static final long serialVersionUID = 8724521982779632883L;

    @GeneratedValue
    @Id
    private Long id;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Lob
    private String message;

    @Column(name = "training_type", columnDefinition = "tinyint(1)", nullable = false)
    @Enumerated(value = EnumType.ORDINAL)
    private MachineLearningResult type;
}
