package com.smmhub.database.model.sql.accounts.types;

import com.smmhub.database.model.sql.accounts.Account;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author nix (28.03.2017)
 */

@Entity
@Table(name = "accounts_simple")
@Getter
@Setter
public class SimpleAccount extends Account {

	private static final long serialVersionUID = 421L;

	// Статус подтверждения EMail
	private boolean emailConfirmed;

	public SimpleAccount(String email) {
		super.email = email;
	}

	public SimpleAccount() {
	}
}
