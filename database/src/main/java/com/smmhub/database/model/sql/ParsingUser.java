package com.smmhub.database.model.sql;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.smmhub.commons.enums.SocialNetworkType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author nix (08.03.2017)
 */

@Entity
@Table(name = "parsing_users")
@NoArgsConstructor
@Getter
@Setter
public class ParsingUser implements Serializable {

	private static final long serialVersionUID = 85L;

	@GeneratedValue
	@Id
	private Long id;

	@Column(name = "social_network_type", columnDefinition = "Integer(2)", nullable = false)
	@Enumerated(value = EnumType.ORDINAL)
	private SocialNetworkType socialNetworkType;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@Column(name = "user_id", nullable = false)
	@Lob
	private String userId;

	@Column(name = "token", nullable = false)
	@Lob
	private String token;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date tokenExpired;

	private String name;

	public ParsingUser(SocialNetworkType socialNetworkType, String userId, String token) {
		this.socialNetworkType = socialNetworkType;
		this.userId = userId;
		this.token = token;
	}
}
