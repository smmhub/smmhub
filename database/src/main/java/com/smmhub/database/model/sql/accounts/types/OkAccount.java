package com.smmhub.database.model.sql.accounts.types;

import com.smmhub.database.model.sql.accounts.Account;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author nix (28.04.2017)
 */

@Entity
@Table(name = "accounts_odnoklassniki")
@Getter
@Setter
public class OkAccount extends Account {

	private static final long serialVersionUID = 472L;

	@Column(name = "user")
	private Long userId;

	@Column(name = "token", length = 128)
	private String token;

	public OkAccount() {
	}
}
