package com.smmhub.database.model.sql;

import com.google.common.collect.Sets;
import com.smmhub.database.model.sql.accounts.Account;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author nix (24.02.2017)
 */

@Entity
@Table(name = "catalog_categories")
@NoArgsConstructor
@Getter
@Setter
public class CatalogCategory implements Serializable {

	private static final long serialVersionUID = 52L;

	@GeneratedValue
	@Id
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_catalogs_categories_parents"), name = "parent_id")
	private CatalogCategory parent;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "parent", orphanRemoval = true)
	@OrderBy("name ASC")
	private Set<CatalogCategory> childs = Sets.newConcurrentHashSet();

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	@Column(nullable = false)
	private String name;

	// если это категория создана юзером
	@Column(nullable = false)
	private Boolean userPrivate = false;

	// аккаунт, к которому привязана приватная категория
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_catalogs_categories_account"), name = "account_id")
	private Account account;

	// это понадобится только для парсинга - тут будут храниться группы,
	// откуда будет вытаскиваться контент для данной категории
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "catalogCategory", orphanRemoval = true)
	@BatchSize(size = 100)
	@OrderBy("name ASC")
	private Set<CatalogGroup> groups = new HashSet<>();

	// связанные задачи постинга
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "categories")
	private Set<Job> jobs = new HashSet<>();

	private boolean enabled = true;

	public CatalogCategory(String name) {
		this.name = name;
	}

	public CatalogCategory(CatalogCategory parent, String name) {
		this.parent = parent;
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CatalogCategory that = (CatalogCategory) o;

		if (enabled != that.enabled) return false;
		if (id != null ? !id.equals(that.id) : that.id != null) return false;
		if (userPrivate != null ? !userPrivate.equals(that.userPrivate) : that.userPrivate != null) return false;
		if (account != null ? !account.getId().equals(that.account.getId()) : that.account.getId() != null) return false;
		if (parent != null && parent.id != null ? !parent.id.equals(that.parent.id) : that.parent != null) return false;
		if (date != null ? !date.equals(that.date) : that.date != null) return false;
		return name != null ? name.equals(that.name) : that.name == null;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (parent != null && parent.id != null ? parent.id.hashCode() : 0);
		result = 31 * result + (date != null ? date.hashCode() : 0);
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (userPrivate != null ? userPrivate.hashCode() : 0);
		result = 31 * result + (account != null ? account.hashCode() : 0);
		result = 31 * result + (enabled ? 1 : 0);
		return result;
	}
}
