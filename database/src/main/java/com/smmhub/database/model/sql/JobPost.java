package com.smmhub.database.model.sql;

import com.smmhub.commons.enums.JobPostingResultStatus;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Nikolay Viguro, 14.12.17
 */

@Entity
@Table(name = "job_posts")
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@Builder
public class JobPost implements Serializable {
	private static final long serialVersionUID = 5254514407021568398L;

	@GeneratedValue
    @Id
    private Long id;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date posted;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(foreignKey = @ForeignKey(name = "fk_job_jobpost"), name = "job_id")
    private Job job;

    private String postId;

	@Column(name = "status", columnDefinition = "Integer(2)", nullable = false)
	@Enumerated(value = EnumType.ORDINAL)
	private JobPostingResultStatus status = JobPostingResultStatus.SUCCESS;

	@Lob
	private String extendedInfo;
}
