package com.smmhub.database.model.sql.accounts;

import com.smmhub.commons.enums.AccountLoginType;
import com.smmhub.commons.enums.AccountType;
import com.smmhub.database.model.sql.Payment;
import com.smmhub.database.model.sql.SocialNetwork;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author nix (24.02.2017)
 */

@Entity
@Table(name = "accounts")
@Inheritance(strategy = InheritanceType.JOINED)
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Account implements Serializable {

	private static final long serialVersionUID = 42L;

	@GeneratedValue
	@Id
	private Long id;

	// EMail аккаунта
	@Column(name = "email", nullable = false)
	protected String email;

	@Column(name = "password", nullable = false)
	protected String password;

	@Column(name = "display_name", length = 64)
	private String displayName;

	// Тип входа
	@Enumerated(value = EnumType.ORDINAL)
	private AccountLoginType loginType;

	@Column(name = "enabled", nullable = false)
	private Boolean enabled = true;

	// Роли в системе (в бинарном виде)
	@Column(name = "binary_roles", nullable = false)
	private Long binaryRoles = 0L;

	// Дата регистрации
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date regDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date lastPasswordReset;

	private String photoUrl;
	private String birthday;

	// Текущий баланс (округляется до 2 символа после запятой, используется "банковское" округление при сериализации)
	@Column(name = "balance", precision = 10, scale = 2, nullable = false)
	private BigDecimal balance = BigDecimal.ZERO;

	// Платежная история аккаунта
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "account", orphanRemoval = true)
	@BatchSize(size = 100)
	@OrderBy("date DESC")
	private Set<Payment> payments = new HashSet<>();

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "account", orphanRemoval = true)
	private Set<SocialNetwork> socialNetworks = new HashSet<>();

	public void setAuthority(Set<AccountType> roles) {
		binaryRoles = 0L;
		for (AccountType role : roles)
			binaryRoles |= 1 << role.ordinal();
	}
}
