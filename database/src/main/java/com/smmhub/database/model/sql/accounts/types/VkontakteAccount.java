package com.smmhub.database.model.sql.accounts.types;

import com.smmhub.database.model.sql.accounts.Account;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author nix (28.03.2017)
 */

@Entity
@Table(name = "accounts_vkontakte")
@Getter
@Setter
public class VkontakteAccount extends Account {

	private static final long serialVersionUID = 422L;

	@Column(name = "user")
	private Integer userId;

	@Column(name = "token", length = 128)
	private String token;

	public VkontakteAccount() {
	}
}
