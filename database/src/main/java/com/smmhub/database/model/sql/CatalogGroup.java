package com.smmhub.database.model.sql;

import com.smmhub.commons.enums.Language;
import com.smmhub.commons.enums.SocialNetworkType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author nix (24.02.2017)
 */

@Entity
@Table(name = "catalog_groups")
@NoArgsConstructor
@Getter
@Setter
public class CatalogGroup implements Serializable {

	private static final long serialVersionUID = 62L;

	@GeneratedValue
	@Id
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_catalogs_categories_groups"), name = "catalog_category_id")
	private CatalogCategory catalogCategory;

	@Column(name = "social_network_type", columnDefinition = "Integer(2)", nullable = false)
	@Enumerated(value = EnumType.ORDINAL)
	private SocialNetworkType socialNetworkType;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private String groupId;

	@Enumerated(value = EnumType.ORDINAL)
	private Language language;

	// Дата последнего сохраненного поста
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastParsedPostDate;

	private boolean enabled = true;
}
