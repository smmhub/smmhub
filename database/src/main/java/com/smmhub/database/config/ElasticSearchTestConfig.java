package com.smmhub.database.config;

import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.node.NodeClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.node.Node;
import org.elasticsearch.plugins.Plugin;
import org.elasticsearch.transport.Netty3Plugin;
import org.elasticsearch.transport.Netty4Plugin;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author nix (26.02.2017)
 */

@Configuration
@EnableElasticsearchRepositories(basePackages = "com.smmhub.database.dao.elastic")
@ComponentScan(basePackages = {"com.smmhub.database.model.elastic"})
@Profile("tests")
@Slf4j
public class ElasticSearchTestConfig implements DisposableBean {

	private NodeClient client;

	@Bean
	public ElasticsearchTemplate elasticsearchTemplate() {
		return new ElasticsearchTemplate(esClient());
	}

	@Bean
	public Client esClient() {
		try {
			log.info("Starting Elasticsearch client");

			Settings settings = Settings.builder()
					.put("path.home", "dev/elastic")
					.put("transport.type", "local")
					.put("http.enabled", true)
					.put("http.type" , "netty4")
					.build();

			Collection<Class<? extends Plugin>> plugins = Arrays.asList(Netty3Plugin.class, Netty4Plugin.class);
			Node node = new PluginConfigurableNode(settings, plugins).start();

			this.client = (NodeClient) node.client();

			this.client = (NodeClient) node.client();
			return this.client;
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}
	}

	@Override
	@SuppressWarnings("Duplicates")
	public void destroy() throws Exception {
		if (this.client != null) {
			try {
				log.info("Closing Elasticsearch client");

				if (this.client != null) {
					this.client.close();
				}
			} catch (final Exception ex) {
				log.error("Error closing Elasticsearch client: ", ex);
			}
		}
	}
}
