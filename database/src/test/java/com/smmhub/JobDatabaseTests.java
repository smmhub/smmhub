package com.smmhub;

import com.smmhub.database.dao.sql.JobDAO;
import com.smmhub.database.model.sql.Job;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("tests")
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class JobDatabaseTests {

	@Autowired
	private JobDAO jobDAO;

	@Test
	public void saveJob() {
		Job job = new Job();
		job.setTitle("TestJob1");
		job.setKlass("test.test.JobTest.class");

		job = jobDAO.save(job);
		assertThat(job.getId()).isNotNull();
	}
}