package com.smmhub;

import com.smmhub.commons.enums.AccountLoginType;
import com.smmhub.commons.enums.PaymentType;
import com.smmhub.database.dao.sql.PaymentDAO;
import com.smmhub.database.dao.sql.accounts.SimpleAccountDAO;
import com.smmhub.database.model.sql.Payment;
import com.smmhub.database.model.sql.accounts.Account;
import com.smmhub.database.model.sql.accounts.types.SimpleAccount;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("tests")
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class ProcessingDatabaseTests {

	@Autowired
	private SimpleAccountDAO accountDAO;

	@Autowired
	private PaymentDAO paymentDAO;

	@Test
	public void addAccount() {

		SimpleAccount account = new SimpleAccount("test1@smm-hub.com");
		account.setPassword("blablabla");
		Payment add = new Payment(account, PaymentType.PLUS, new BigDecimal("100.00"));
		account.getPayments().add(add);
		account.setLoginType(AccountLoginType.FORM);
		accountDAO.save(account);

		Account fromDb = accountDAO.findByEmail("test1@smm-hub.com");
		assertEquals(fromDb.getPayments().size(), 1);

		List<Payment> payments = (List<Payment>) paymentDAO.findAll();
		assertEquals(payments.size(), 1);
	}
}