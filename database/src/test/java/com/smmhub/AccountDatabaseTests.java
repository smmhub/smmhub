package com.smmhub;

import com.smmhub.commons.enums.AccountLoginType;
import com.smmhub.database.dao.sql.accounts.SimpleAccountDAO;
import com.smmhub.database.model.sql.accounts.types.SimpleAccount;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("tests")
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class AccountDatabaseTests {

	@Autowired
	private SimpleAccountDAO accountDAO;

	@Test
	public void addAccount() {

		SimpleAccount account = new SimpleAccount("test1@smmhub.com");
		account.setPassword("blablabla");
		account.setLoginType(AccountLoginType.FORM);
		accountDAO.save(account);

		SimpleAccount fromDb = accountDAO.findByEmail("test1@smmhub.com");

		assertThat(fromDb.getEmail()).isEqualTo("test1@smmhub.com");
	}
}