package com.smmhub;

import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.database.dao.elastic.PostDAO;
import com.smmhub.database.model.elastic.Post;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.NONE)
@ActiveProfiles("tests")
public class ElasticDatabaseTests {

	@Autowired
	private PostDAO postDAO;

	@Test
	public void addPost() throws InterruptedException {

		postDAO.deleteAll();

		Post post = new Post(SocialNetworkType.VK, "test test test");
		postDAO.save(post);

		List<Post> fromDb = postDAO.findBySocialNetworkType(SocialNetworkType.VK);
		assertEquals(fromDb.size(), 1);
	}
}