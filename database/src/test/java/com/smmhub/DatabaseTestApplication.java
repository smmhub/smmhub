package com.smmhub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication()
@EnableAutoConfiguration()
public class DatabaseTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatabaseTestApplication.class, args);
	}
}