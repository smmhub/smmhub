package com.smmhub;

import com.smmhub.commons.service.RunningService;
import com.smmhub.database.config.JpaConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableWebMvc
@EnableAsync
@EnableAutoConfiguration
@Component
public class HubApplication implements ApplicationRunner {

	@Autowired
	@Qualifier("parserManager")
	private RunningService parserManagerService;

	public static void main(String[] args) throws Exception {

		SpringApplication.run(new Class<?>[]{
				HubApplication.class,
				JpaConfig.class
		}, args);
	}

	@Bean
	public TaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(20);
		executor.setQueueCapacity(50);
		return executor;
	}

	@Override
	public void run(ApplicationArguments applicationArguments) throws Exception {

	}
}
