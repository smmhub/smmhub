package com.smmhub;

import com.smmhub.commons.enums.AccountLoginType;
import com.smmhub.database.model.sql.accounts.Account;
import com.smmhub.database.model.sql.accounts.types.SimpleAccount;
import com.smmhub.services.AccountService;
import com.smmhub.web.controllers.v1.accounts.AccountController;
import com.smmhub.web.services.SecurityService;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Collection;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("tests")
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class TestAccount {

	@Autowired
	private AccountController accountController;

	@Autowired
	private AccountService accountService;

	@Autowired
	private SecurityService securityService;

	private SimpleAccount account;
	private String token;

	private MockMvc mockMvc;

	@Before
	public void setUp() throws Exception {

		account = new SimpleAccount("user1@smmhub.com");
		account.setPassword("test1");
		account.setLoginType(AccountLoginType.FORM);

		account = (SimpleAccount) accountService.addAccount(account);
		accountService.addMoney(account, new BigDecimal("100.00"));

		this.mockMvc = standaloneSetup(accountController).build();

		token = securityService.autologin(
				account.getEmail() + "-" + account.getLoginType().name(),
				account.getPassword());

		Assert.assertNotNull(token);
	}

	@After
	public void tearDown() throws Exception {
		Collection<Account> accounts = accountService.allAccounts();

		for (Account acc : accounts) {
			accountService.removeAccount(acc);
		}

		token = null;
	}

	@Test
	public void checkUserInfo() throws Exception {
		this.mockMvc.perform(get("/api/v1/account/userinfo")
				.accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.data.email", is(account.getEmail())));
	}

}