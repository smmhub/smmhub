package com.smmhub.web.aspects;

import com.smmhub.commons.model.dto.account.AccountDTO;
import com.smmhub.database.model.sql.accounts.Account;
import com.smmhub.services.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * @author nix on 18.09.2017.
 */

@Slf4j
@Aspect
@Order(1)
@Component
public class PushAccountAspect {

	@Autowired
	private AccountService accountService;

	@Around("execution(public * com.smmhub.web.controllers..*(..)) && (args(account, ..) || args(account))")
	public Object runTime(ProceedingJoinPoint pjp, Account account) throws Throwable {

		AccountDTO accountDTO = (AccountDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		account = accountService.getAccount(accountDTO.getEmail(), accountDTO.getLoginType());

		Object[] args = pjp.getArgs();
		args[0] = account;

		return pjp.proceed(args);
	}
}