package com.smmhub.web.aspects;

import com.smmhub.commons.model.web.response.BackendResponse;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author nix on 18.09.2017.
 */

@Slf4j
@Aspect
@Order // Integer.MAX_VALUE by default, so this aspect become to be runned at the end
@Component
public class RunTimeAspect {

	@Around("execution(public * com.smmhub.web.controllers..*(..))")
	public Object runTime(ProceedingJoinPoint pjp) throws Throwable {
		long startTime = System.currentTimeMillis();

		BackendResponse res = (BackendResponse) pjp.proceed();

		long endTime = System.currentTimeMillis();
		res.setTime((endTime - startTime));
		log.info("Method " + pjp.getSignature().getName() + " running time: " + (endTime - startTime) + "ms");

		return res;
	}
}