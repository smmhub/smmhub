package com.smmhub.web.aspects;

import com.smmhub.commons.model.dto.account.AccountDTO;
import com.smmhub.commons.model.web.response.BackendResponse;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * @author nix on 02.03.2017.
 */

@Slf4j
@Aspect
@Order(1)
@Component
public class PushAccountDTOAspect {

	@Around("execution(public * com.smmhub.web.controllers..*(..)) && (args(account, ..) || args(account))")
	public Object runTime(ProceedingJoinPoint pjp, AccountDTO account) throws Throwable {
		long startTime = System.currentTimeMillis();

		account = (AccountDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Object[] args = pjp.getArgs();
		args[0] = account;
		BackendResponse res = (BackendResponse) pjp.proceed(args);

		long endTime = System.currentTimeMillis();
		res.setTime((endTime - startTime));

		return res;
	}
}