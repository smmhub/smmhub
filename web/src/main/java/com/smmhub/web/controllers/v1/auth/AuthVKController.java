package com.smmhub.web.controllers.v1.auth;

import com.smmhub.commons.enums.AccountLoginType;
import com.smmhub.commons.enums.BackendAPIResponseCode;
import com.smmhub.commons.exceptions.AccountIdentifierRequiredException;
import com.smmhub.commons.exceptions.AccountNotFoundException;
import com.smmhub.commons.model.datasources.SocialProfile;
import com.smmhub.commons.model.web.request.account.OAuth2CodeRequest;
import com.smmhub.commons.model.web.response.BackendResponse;
import com.smmhub.commons.model.web.response.OAuth2ParamsResponse;
import com.smmhub.commons.model.web.response.TokenResponse;
import com.smmhub.database.model.sql.accounts.types.VkontakteAccount;
import com.smmhub.datasources.SocialAPI;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.objects.UserAuthResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mobile.device.Device;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/api/v1/auth/vk")
public class AuthVKController extends AbstractAuthController {

	@Autowired
	@Qualifier("vk")
	private SocialAPI vkAPI;

	@GetMapping("/login")
	public BackendResponse vkRegistration() throws Exception {

		OAuth2ParamsResponse response = new OAuth2ParamsResponse(
				socialCredentialsConsts.vkLoginAppID,
				socialCredentialsConsts.vkLoginCallback
		);

		return BackendResponse.of(response);
	}

	@PostMapping("/callback")
	@ResponseBody
	public BackendResponse vkRegistrationCallback(
			@RequestBody OAuth2CodeRequest codeRequest, Device device)
			throws Exception {

		VkApiClient client = (VkApiClient) vkAPI.getLowLevelAccess();
		UserAuthResponse authResponse = client
				.oauth()
				.userAuthorizationCodeFlow(
						socialCredentialsConsts.vkLoginAppID,
						socialCredentialsConsts.vkLoginAppSecret,
						socialCredentialsConsts.vkLoginCallback,
						codeRequest.getCode())
				.execute();

		if (StringUtils.isEmpty(authResponse.getEmail())) {
			return BackendResponse.of(BackendAPIResponseCode.EMAIL_NOT_SPECIFIED, "EMail not specified");
		}

		vkAPI.init(authResponse.getUserId().toString(), authResponse.getAccessToken());

		VkontakteAccount account;
		try {
			account = (VkontakteAccount) accountService.getAccount(authResponse.getEmail(), AccountLoginType.VK);
		} catch (AccountIdentifierRequiredException e) {
			log.error("No account identifier supplied!");
			return BackendResponse.of(BackendAPIResponseCode.ERROR, "No account identifier supplied!");
		} catch (AccountNotFoundException e) {
			account = new VkontakteAccount();
		}
		// такого юзера нет, создаем
		if (account == null) {
			account = new VkontakteAccount();
		}

		account.setToken(authResponse.getAccessToken());
		account.setEmail(authResponse.getEmail());
		account.setUserId(authResponse.getUserId());

		// новый юзер
		if (account.getId() == null || account.getId() == 0L) {
			SocialProfile userSettings = vkAPI.getProfileInfo();

			account.setBirthday(userSettings.getBirthday());
			account.setDisplayName(userSettings.getDisplayName());
			account.setPhotoUrl(userSettings.getPhotoUrl());
			account.setPassword(AccountLoginType.VK.name());
			account = (VkontakteAccount) accountService.addAccount(account);
		} else {
			account = (VkontakteAccount) accountService.saveAccount(account);
		}

		String token = securityService.autologin(
				accountService.getOrUpdateAccountInCache(account),
				AccountLoginType.VK.name(),
				device);

		return BackendResponse.of(new TokenResponse(token));
	}
}
