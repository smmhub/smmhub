package com.smmhub.web.controllers.v1.auth;

import com.github.mkopylec.recaptcha.validation.RecaptchaValidator;
import com.github.mkopylec.recaptcha.validation.ValidationResult;
import com.smmhub.commons.enums.AccountLoginType;
import com.smmhub.commons.enums.BackendAPIResponseCode;
import com.smmhub.commons.exceptions.AccountEmailRequiredException;
import com.smmhub.commons.exceptions.AccountNotFoundException;
import com.smmhub.commons.exceptions.AccountPasswordRequiredException;
import com.smmhub.commons.model.dto.account.AccountDTO;
import com.smmhub.commons.model.web.request.account.AuthenticationRequest;
import com.smmhub.commons.model.web.request.account.SignupRequest;
import com.smmhub.commons.model.web.response.BackendResponse;
import com.smmhub.commons.model.web.response.TokenResponse;
import com.smmhub.database.model.sql.accounts.types.SimpleAccount;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/api/v1/auth/form")
public class AuthFormController extends AbstractAuthController {

	@Autowired
	private RecaptchaValidator recaptchaValidator;

	@PostMapping("/signup")
	@ResponseBody
	public Object validateCaptcha(@RequestBody SignupRequest signupRequest, Device device)
			throws AccountNotFoundException, AccountEmailRequiredException, AccountPasswordRequiredException {

		if (usersCache.get(signupRequest.getEmail() + "-FORM") != null) {
			return BackendResponse.of(BackendAPIResponseCode.USER_ALREADY_EXISTS, "User already exists");
		}

		// Рекапча (см. https://github.com/mkopylec/recaptcha-spring-boot-starter)
		ValidationResult result = recaptchaValidator.validate(signupRequest.getRecaptchaResponse());
		if (result.isSuccess()) {
			SimpleAccount account = new SimpleAccount(signupRequest.getEmail());

			account.setLoginType(AccountLoginType.FORM);
			account.setPassword(signupRequest.getPassword());
			account.setDisplayName(signupRequest.getDisplayName());

			account = (SimpleAccount) accountService.addAccount(account);
			AccountDTO accountDTO = accountService.getOrUpdateAccountInCache(account);

			String token = securityService.autologin(accountDTO, signupRequest.getPassword(), device);
			return BackendResponse.of(new TokenResponse(token));
		} else {
			return BackendResponse.of(BackendAPIResponseCode.RECAPTCHA_FAILED, "ReCaptcha validation failed");
		}
	}

	@PostMapping("/login")
	@ResponseBody
	public Object formLogin(@RequestBody AuthenticationRequest authenticationRequest, Device device) throws Exception {

		AccountDTO account = usersCache.get(authenticationRequest.getEmail() + "-FORM");

		if (account == null)
			throw new AccountNotFoundException();

		if (!account.getLoginType().equals(AccountLoginType.FORM))
			throw new AccountNotFoundException();

		String token = securityService.autologin(account, authenticationRequest.getPassword(), device);

		return BackendResponse.of(new TokenResponse(token));
	}
}
