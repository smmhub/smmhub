package com.smmhub.web.controllers.v1.machinelearning;

import com.smmhub.commons.enums.BackendAPIResponseCode;
import com.smmhub.commons.model.web.request.posts.TrainMachineLearningRequest;
import com.smmhub.commons.model.web.response.BackendResponse;
import com.smmhub.database.model.elastic.Post;
import com.smmhub.services.MachineLearningService;
import com.smmhub.services.PostService;
import com.smmhub.web.controllers.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Nikolay Viguro, 14.11.17
 */

@RequestMapping("/api/v1/machinelearning")
public class MachineLearningController extends AbstractController {

    @Autowired
    private PostService postService;

    @Autowired
    private MachineLearningService machineLearningService;

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/train")
    public Object trainMachineLearning(@RequestBody TrainMachineLearningRequest trainMachineLearningRequest)
            throws Exception {

        Post post = postService.findByDatabasePostId(trainMachineLearningRequest.getEsPostId());

        if(post == null || StringUtils.isEmpty(post.getText())) {
            return BackendResponse.of(BackendAPIResponseCode.ERROR, "No training data text found");
        }

        machineLearningService.addTrainingData(
                post.getText(),
                trainMachineLearningRequest.getType()
        );

        return BackendResponse.of("Training data added");
    }
}
