package com.smmhub.web.controllers.v1.social;

import com.smmhub.commons.cache.DistributedBackendCache;
import com.smmhub.commons.consts.SocialCredentialsConsts;
import com.smmhub.commons.enums.BackendAPIResponseCode;
import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.commons.model.datasources.Pageable;
import com.smmhub.commons.model.datasources.Pagination;
import com.smmhub.commons.model.datasources.SocialGroup;
import com.smmhub.commons.model.datasources.SocialProfile;
import com.smmhub.commons.model.dto.account.AccountDTO;
import com.smmhub.commons.model.web.request.account.OAuth2CodeRequest;
import com.smmhub.commons.model.web.response.BackendResponse;
import com.smmhub.commons.model.web.response.OAuth2ParamsResponse;
import com.smmhub.database.model.sql.SocialNetwork;
import com.smmhub.database.model.sql.SocialNetworkGroup;
import com.smmhub.database.model.sql.accounts.Account;
import com.smmhub.datasources.SocialAPI;
import com.smmhub.services.AccountService;
import com.smmhub.web.controllers.AbstractController;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.objects.UserAuthResponse;
import com.vk.api.sdk.objects.groups.GroupFull;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@ResponseBody
@Slf4j
@RequestMapping("/api/v1/social/vk")
@SuppressWarnings("Duplicates")
public class VKSocialController extends AbstractController {

	private final SocialCredentialsConsts socialCredentialsConsts;
	private final AccountService accountService;
	private final SocialAPI vkAPI;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private DistributedBackendCache<String, AccountDTO> accountsCache;

	@Autowired
	public VKSocialController(AccountService accountService, @Qualifier("vk") SocialAPI vkApi, SocialCredentialsConsts socialCredentialsConsts) {
		this.vkAPI = vkApi;
		this.accountService = accountService;
		this.socialCredentialsConsts = socialCredentialsConsts;
	}

	@PreAuthorize("hasAuthority('REGULAR_USER')")
	@GetMapping("/connect")
	public BackendResponse vkConnect() throws Exception {
		OAuth2ParamsResponse response = new OAuth2ParamsResponse(
				socialCredentialsConsts.vkDataAppID,
				socialCredentialsConsts.vkDataCallback
		);

		return BackendResponse.of(response);
	}

	@PreAuthorize("hasAuthority('REGULAR_USER')")
	@PostMapping("/connect/callback")
	public Object addVKNetwork(Account account, @RequestBody OAuth2CodeRequest codeRequest)
			throws Exception {

		String userId = account.getEmail() + "-" + account.getLoginType().name();

		VkApiClient client = (VkApiClient) vkAPI.getLowLevelAccess();
		UserAuthResponse authResponse = client
				.oauth()
				.userAuthorizationCodeFlow(
						socialCredentialsConsts.vkDataAppID,
						socialCredentialsConsts.vkDataAppSecret,
						socialCredentialsConsts.vkDataCallback,
						codeRequest.getCode())
				.execute();

		vkAPI.init(authResponse.getUserId().toString(), authResponse.getAccessToken());

		SocialProfile profile = vkAPI.getProfileInfo();

		for (SocialNetwork socialNetwork : account.getSocialNetworks()) {
			if (Objects.equals(socialNetwork.getUserId(), profile.getUserId()) &&
					socialNetwork.getSocialNetworkType().equals(SocialNetworkType.VK)) {

				return BackendResponse.of(
						BackendAPIResponseCode.SOCIAL_ACCOUNT_ALREADY_EXISTS,
						"Already connected"
				);
			}
		}

		SocialNetwork socialNetwork = new SocialNetwork(
				SocialNetworkType.VK,
				profile.getDisplayName(),
				profile.getUserId(),
				profile.getAccessToken()
		);
		socialNetwork.setAccount(account);

		Pageable<SocialGroup> groupResp = vkAPI.getOwnedGroups(new Pagination(200));

		for (SocialGroup full : groupResp.getData()) {
			SocialNetworkGroup dbGroup = new SocialNetworkGroup(socialNetwork, SocialNetworkType.VK);
			dbGroup.setSocialGroupId(full.getId());
			dbGroup.setName(full.getName());
			dbGroup.setLogo(full.getPhoto());

			// fixme пока запихиваем весь ответ от ВК - так проще сеарилизовать/девериализовать, но избыточность
			dbGroup.setAddtionalJsonData(full.getWrappedClass());
				socialNetwork.getSocialNetworkGroups().add(dbGroup);
			}

		account.getSocialNetworks().add(socialNetwork);
		accountService.saveAccount(account);
		accountsCache.put(userId, modelMapper.map(account, AccountDTO.class));

		return BackendResponse.of(BackendAPIResponseCode.OK_SOCIAL_ACCOUNT_ADDED, "Added successfully");
	}

	@PreAuthorize("hasAuthority('REGULAR_USER')")
	@GetMapping("/groups/update")
	public Object updateGroups(Account account) throws Exception {
		String userId = account.getEmail() + "-" + account.getLoginType().name();

		for (SocialNetwork network : account.getSocialNetworks()) {

			if(network.getSocialNetworkType().equals(SocialNetworkType.VK)) {
				vkAPI.init(network.getUserId(), network.getToken());
				Pageable<SocialGroup> groupResp = vkAPI.getOwnedGroups(new Pagination(200));

				for (SocialNetworkGroup groupDb : network.getSocialNetworkGroups()) {
					for (SocialGroup full : groupResp.getData()) {
						if (groupDb.getSocialGroupId().equals(full.getId())) {
							GroupFull fromDb = (GroupFull) groupDb.getAddtionalJsonData();

							if (!fromDb.equals(full.getWrappedClass())) {
								groupDb.setSocialGroupId(full.getId());
								groupDb.setName(full.getName());
								groupDb.setLogo(full.getPhoto());

								// fixme пока запихиваем весь ответ от ВК - так проще сеарилизовать/девериализовать, но избыточность
								groupDb.setAddtionalJsonData(full.getWrappedClass());
							}
						}
					}
				}
			}

			accountService.saveAccount(account);
			accountsCache.put(userId, modelMapper.map(account, AccountDTO.class));
		}

		return BackendResponse.of(BackendAPIResponseCode.OK_SOCIAL_GROUPS_UPDATED, "Updated successfully");
	}

}
