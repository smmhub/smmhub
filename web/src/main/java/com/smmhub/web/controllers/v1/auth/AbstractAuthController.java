package com.smmhub.web.controllers.v1.auth;

import com.smmhub.commons.cache.DistributedBackendCache;
import com.smmhub.commons.consts.SocialCredentialsConsts;
import com.smmhub.commons.model.dto.account.AccountDTO;
import com.smmhub.services.AccountService;
import com.smmhub.web.controllers.AbstractController;
import com.smmhub.web.services.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;

public class AbstractAuthController extends AbstractController {

	@Autowired
	protected SocialCredentialsConsts socialCredentialsConsts;

	@Autowired
	protected AccountService accountService;

	@Autowired
	protected SecurityService securityService;

	@Autowired
	protected DistributedBackendCache<String, AccountDTO> usersCache;
}
