package com.smmhub.web.controllers.v1;

import com.smmhub.commons.enums.BackendAPIResponseCode;
import com.smmhub.commons.model.dto.account.AccountDTO;
import com.smmhub.commons.model.dto.category.CatalogCategoryAllDTO;
import com.smmhub.commons.model.dto.category.CatalogCategoryDTO;
import com.smmhub.commons.model.web.request.catalog.AddCategoryRequest;
import com.smmhub.commons.model.web.request.catalog.RemoveCategoryRequest;
import com.smmhub.commons.model.web.request.catalog.UpdateCategoryRequest;
import com.smmhub.commons.model.web.response.BackendResponse;
import com.smmhub.database.model.sql.CatalogCategory;
import com.smmhub.services.CatalogService;
import com.smmhub.web.controllers.AbstractController;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@ResponseBody
@Slf4j
@RequestMapping("/api/v1/catalog")
public class CatalogController extends AbstractController {

	@Autowired
	private CatalogService catalogService;

	@Autowired
	private ModelMapper modelMapper;

	@PreAuthorize("hasAuthority('ADMIN')")
	@PostMapping("/add")
	public Object addCategory(@RequestBody AddCategoryRequest categoryRequest)	throws Exception {

		CatalogCategory category = new CatalogCategory(categoryRequest.getName());

		if (categoryRequest.getParentid() != null) {
			CatalogCategory parent = catalogService.getCategory(categoryRequest.getParentid());
			category.setParent(parent);
		}

		catalogService.saveCategory(category);

		return BackendResponse.of(BackendAPIResponseCode.OK_CATEGORY_ADDED, "Category added successfully");
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@PostMapping("/remove")
	public Object removeCategory(@RequestBody RemoveCategoryRequest categoryRequest) throws Exception {

		CatalogCategory category = catalogService.getCategory(categoryRequest.getId());
		catalogService.removeCategory(category);

		return BackendResponse.of(BackendAPIResponseCode.OK_CATEGORY_REMOVED, "Category removed successfully");
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@PostMapping("/update")
	public Object editCategory(@RequestBody UpdateCategoryRequest categoryRequest)	throws Exception {

		CatalogCategory category = catalogService.getCategory(categoryRequest.getCategoryid());

		category.setName(categoryRequest.getName());

		if (categoryRequest.getParentid() != null) {
			CatalogCategory parent = catalogService.getCategory(category.getId());
			category.setParent(parent);
		}

		catalogService.saveCategory(category);

		return BackendResponse.of(BackendAPIResponseCode.OK_CATEGORY_UPDATED, "Category updated successfully");
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@GetMapping("/get/all")
	public Object getAllCategories() throws Exception {

		CatalogCategory root = catalogService.getAllCatalog();
		CatalogCategoryAllDTO categoryDTO = modelMapper.map(root, CatalogCategoryAllDTO.class);

		return BackendResponse.of(categoryDTO);
	}

	@PreAuthorize("hasAuthority('REGULAR_USER')")
	@GetMapping("/get")
	public Object getUserCategories(AccountDTO accountDTO) throws Exception {

		CatalogCategory root = catalogService.getAllCatalog(accountDTO);
		CatalogCategoryDTO categoryDTO = modelMapper.map(root, CatalogCategoryDTO.class);

		return BackendResponse.of(categoryDTO);
	}
}
