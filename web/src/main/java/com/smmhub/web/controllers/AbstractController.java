package com.smmhub.web.controllers;

import com.smmhub.commons.enums.BackendAPIResponseCode;
import com.smmhub.commons.exceptions.AccountNotFoundException;
import com.smmhub.commons.model.web.response.BackendResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

@Slf4j
public abstract class AbstractController {

	@ExceptionHandler({AccountNotFoundException.class, AccessDeniedException.class, BadCredentialsException.class})
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	@ResponseBody
	public BackendResponse resolveAccountNotFoundException(HttpServletRequest req, Exception ex) {
		log.error("Account problems ", ex);
		return BackendResponse.of(BackendAPIResponseCode.UNAUTHORIZED, "Invalid credentials");
	}

	@ExceptionHandler({Throwable.class})
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public BackendResponse resolveException(HttpServletRequest req, Exception ex) {
		log.error("Error: ", ex);
		return BackendResponse.of(BackendAPIResponseCode.ERROR, "Error: " + ex.getMessage());
	}
}
