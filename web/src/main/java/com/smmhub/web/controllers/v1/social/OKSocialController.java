package com.smmhub.web.controllers.v1.social;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.smmhub.commons.cache.DistributedBackendCache;
import com.smmhub.commons.consts.SocialCredentialsConsts;
import com.smmhub.commons.enums.BackendAPIResponseCode;
import com.smmhub.commons.enums.SocialNetworkType;
import com.smmhub.commons.model.datasources.Pageable;
import com.smmhub.commons.model.datasources.Pagination;
import com.smmhub.commons.model.datasources.SocialGroup;
import com.smmhub.commons.model.datasources.SocialProfile;
import com.smmhub.commons.model.dto.account.AccountDTO;
import com.smmhub.commons.model.web.request.account.OAuth2CodeRequest;
import com.smmhub.commons.model.web.response.BackendResponse;
import com.smmhub.commons.model.web.response.OAuth2ParamsResponse;
import com.smmhub.database.model.sql.SocialNetwork;
import com.smmhub.database.model.sql.SocialNetworkGroup;
import com.smmhub.database.model.sql.accounts.Account;
import com.smmhub.datasources.SocialAPI;
import com.smmhub.datasources.socialapi.ok.ScribleFixedOkAPI;
import com.smmhub.services.AccountService;
import com.smmhub.web.controllers.AbstractController;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@ResponseBody
@Slf4j
@RequestMapping("/api/v1/social/ok")
@SuppressWarnings("Duplicates")
public class OKSocialController extends AbstractController {

	@Autowired
	private SocialCredentialsConsts socialCredentialsConsts;

	@Autowired
	private AccountService accountService;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	@Qualifier("ok")
	private SocialAPI okAPI;

	@Autowired
	private DistributedBackendCache<String, AccountDTO> accountsCache;

	@PreAuthorize("hasAuthority('REGULAR_USER')")
	@GetMapping("/connect")
	public BackendResponse okConnect() throws Exception {

		OAuth2ParamsResponse response = new OAuth2ParamsResponse(
				socialCredentialsConsts.okDataAppID,
				socialCredentialsConsts.okDataCallback
		);

		return BackendResponse.of(response);
	}

	@PreAuthorize("hasAuthority('REGULAR_USER')")
	@PostMapping("/connect/callback")
	public Object addOKNetwork(Account account, @RequestBody OAuth2CodeRequest codeRequest)
			throws Exception {

		String userId = account.getEmail() + "-" + account.getLoginType().name();

		OAuth20Service service = new ServiceBuilder()
				.apiKey(socialCredentialsConsts.okDataAppID)
				.apiSecret(socialCredentialsConsts.okDataAppSecret)
				.callback(socialCredentialsConsts.okDataCallback)
				.scope("GET_EMAIL;VALUABLE_ACCESS;LONG_ACCESS_TOKEN;GROUP_CONTENT;PHOTO_CONTENT")
				.build(ScribleFixedOkAPI.instance());

		okAPI.init(null, service, codeRequest.getCode());

		SocialProfile profile = okAPI.getProfileInfo();

		for (SocialNetwork socialNetwork : account.getSocialNetworks()) {
			if (Objects.equals(socialNetwork.getUserId(), profile.getUserId()) &&
					socialNetwork.getSocialNetworkType().equals(SocialNetworkType.OK)) {

				// update token
				socialNetwork.setToken(profile.getAccessToken());
				account = accountService.saveAccount(account);
				accountsCache.put(userId, modelMapper.map(account, AccountDTO.class));

				return BackendResponse.of(
						BackendAPIResponseCode.SOCIAL_ACCOUNT_ALREADY_EXISTS,
						"Already connected"
				);
			}
		}

		SocialNetwork socialNetwork = new SocialNetwork(
				SocialNetworkType.OK,
				profile.getDisplayName(),
				profile.getUserId(),
				profile.getAccessToken()
		);
		socialNetwork.setAccount(account);

		Pageable<SocialGroup> groupResp = okAPI.getOwnedGroups(new Pagination(50));

		for (SocialGroup full : groupResp.getData()) {
			SocialNetworkGroup dbGroup = new SocialNetworkGroup(socialNetwork, SocialNetworkType.OK);
			dbGroup.setSocialGroupId(full.getId());
			dbGroup.setName(full.getName());
			dbGroup.setLogo(full.getPhoto());

			dbGroup.setAddtionalJsonData(full.getWrappedClass());
			socialNetwork.getSocialNetworkGroups().add(dbGroup);
		}

		account.getSocialNetworks().add(socialNetwork);
		account = accountService.saveAccount(account);
		accountsCache.put(userId, modelMapper.map(account, AccountDTO.class));

		return BackendResponse.of(BackendAPIResponseCode.OK_SOCIAL_ACCOUNT_ADDED, "Added successfully");
	}

	@PreAuthorize("hasAuthority('REGULAR_USER')")
	@GetMapping("/groups/update")
	public Object updateGroups(Account account) throws Exception {

		for (SocialNetwork network : account.getSocialNetworks()) {

			if (network.getSocialNetworkType().equals(SocialNetworkType.OK)) {
				okAPI.init(network.getUserId(), network.getToken());
				Pageable<SocialGroup> groupResp = okAPI.getOwnedGroups(new Pagination(50));

				for (SocialNetworkGroup groupDb : network.getSocialNetworkGroups()) {
					for (SocialGroup full : groupResp.getData()) {
						if (groupDb.getSocialGroupId().equals(full.getId())) {
							SocialGroup fromDb = (SocialGroup) groupDb.getAddtionalJsonData();

							if (!fromDb.equals(full.getWrappedClass())) {
								groupDb.setSocialGroupId(full.getId());
								groupDb.setName(full.getName());
								groupDb.setLogo(full.getPhoto());

								// fixme пока запихиваем весь ответ от OК - так проще сеарилизовать/девериализовать, но избыточность
								groupDb.setAddtionalJsonData(full.getWrappedClass());
							}
						}
					}
				}
			}

			accountService.saveAccount(account);
		}

		return BackendResponse.of(BackendAPIResponseCode.OK_SOCIAL_GROUPS_UPDATED, "Updated successfully");
	}
}
