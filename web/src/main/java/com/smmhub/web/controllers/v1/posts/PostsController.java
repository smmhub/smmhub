package com.smmhub.web.controllers.v1.posts;

import com.smmhub.commons.model.dto.post.PostDTO;
import com.smmhub.commons.model.web.request.posts.GetPostsByIdRequest;
import com.smmhub.commons.model.web.request.posts.GetPostsRequest;
import com.smmhub.commons.model.web.request.posts.GetSinglePostByIdRequest;
import com.smmhub.commons.model.web.response.BackendResponse;
import com.smmhub.database.model.elastic.Post;
import com.smmhub.services.PostService;
import com.smmhub.web.controllers.AbstractController;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.List;

@RestController
@ResponseBody
@Slf4j
@RequestMapping("/api/v1/posts")
public class PostsController extends AbstractController {

	@Autowired
	private  PostService postService;

	@Autowired
	private ModelMapper modelMapper;

	private final static Type targetListType = new TypeToken<List<PostDTO>>() {}.getType();

	@PreAuthorize("hasAuthority('REGULAR_USER')")
	@PostMapping("/get")
	public Object getPosts(@RequestBody GetPostsRequest getPostsRequest) {
		Page<Post> posts = postService.findPostByCategories(
				getPostsRequest.getCategoryIds(),
                PageRequest.of(getPostsRequest.getPage(), getPostsRequest.getPostsInPage()));

        List<PostDTO> postsDTO = modelMapper.map(posts.getContent(), targetListType);
        return BackendResponse.of(postsDTO);
    }

    @PreAuthorize("hasAuthority('REGULAR_USER')")
    @PostMapping("/get/id")
    public Object getSinglePost(@RequestBody GetSinglePostByIdRequest getPostRequest) {
        Post post = postService.findById(getPostRequest.getPostId());
        PostDTO postDTO = modelMapper.map(post, PostDTO.class);
        return BackendResponse.of(postDTO);
    }

    @PreAuthorize("hasAuthority('REGULAR_USER')")
    @PostMapping("/get/ids")
    public Object getPost(@RequestBody GetPostsByIdRequest getPostsByIdRequest) {
        Page<Post> posts = postService.findByIds(getPostsByIdRequest.getPostIds(),
                PageRequest.of(getPostsByIdRequest.getPage(), getPostsByIdRequest.getPostsInPage()));
        List<PostDTO> postsDTO = modelMapper.map(posts.getContent(), targetListType);
		return BackendResponse.of(postsDTO);
	}

}
