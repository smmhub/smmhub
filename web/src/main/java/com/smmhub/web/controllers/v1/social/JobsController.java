package com.smmhub.web.controllers.v1.social;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smmhub.commons.cache.DistributedBackendCache;
import com.smmhub.commons.enums.BackendAPIResponseCode;
import com.smmhub.commons.exceptions.CatalogCategoryNotFoundException;
import com.smmhub.commons.exceptions.JobNotFoundException;
import com.smmhub.commons.model.dto.account.AccountDTO;
import com.smmhub.commons.model.dto.job.JobDTO;
import com.smmhub.commons.model.jobs.JobPostingData;
import com.smmhub.commons.model.web.request.social.RemoveJobRequest;
import com.smmhub.commons.model.web.request.social.UpdateJobRequest;
import com.smmhub.commons.model.web.response.BackendResponse;
import com.smmhub.database.model.elastic.Post;
import com.smmhub.database.model.sql.*;
import com.smmhub.database.model.sql.accounts.Account;
import com.smmhub.scheduler.services.JobService;
import com.smmhub.scheduler.services.SchedulerService;
import com.smmhub.services.CatalogService;
import com.smmhub.services.PostService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Nikolay Viguro, 16.11.17
 */

@RestController
@ResponseBody
@Slf4j
@RequestMapping("/api/v1/social/jobs")
public class JobsController {

    @Autowired
    private JobService jobService;

    @Autowired
    private SchedulerService schedulerService;

	@Autowired
	private CatalogService catalogService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PostService postService;

    @Autowired
    private DistributedBackendCache<String, AccountDTO> accountsCache;

    @PreAuthorize("hasAuthority('REGULAR_USER')")
    @PostMapping("/update")
    @SuppressWarnings("Duplicates")
    public Object updateJob(Account account, @RequestBody UpdateJobRequest updateJobRequest) throws Exception {
        BackendAPIResponseCode responseCode;
        boolean updated = false, created = false;
        long jobId = 0L;

        if (updateJobRequest.getJob() == null) {
            return BackendResponse.of(BackendAPIResponseCode.ERROR_JOB_DATA_NOT_FILLED, "No job data in request");
        }

        if (CollectionUtils.isEmpty(updateJobRequest.getJob().getCategories())) {
            return BackendResponse.of(BackendAPIResponseCode.ERROR_CATEGORIES_NOT_FILLED, "No categories specified in job data");
        }

        if(updateJobRequest.getJob().getId() != null && updateJobRequest.getJob().getId() != 0L) {
            for (SocialNetwork network : account.getSocialNetworks()) {
                for (SocialNetworkGroup group : network.getSocialNetworkGroups()) {
                    for (Job job : group.getJobs()) {
                        if (Objects.equals(updateJobRequest.getJob().getId(), job.getId())) {
                            Job jobDB = jobService.getJob(job.getId());
                            JobDTO jobFromFrontend = updateJobRequest.getJob();
                            JobDTO jobFromDB = modelMapper.map(jobDB, JobDTO.class);

                            if (!jobFromFrontend.equals(jobFromDB)) {
                                Job updatedJob = modelMapper.map(jobFromFrontend, Job.class);

	                            Set<CatalogCategory> attachedCategories = updatedJob.getCategories().stream().map(catalogCategory -> {
		                            try {
			                            return catalogService.getCategory(catalogCategory.getId());
		                            } catch (CatalogCategoryNotFoundException e) {
			                            log.error("No category #{} found in database!", catalogCategory.getId());
			                            return null;
		                            }
	                            }).collect(Collectors.toSet());

                                jobDB.setCategories(attachedCategories);
                                jobDB.setCron(updatedJob.getCron());
                                jobDB.setDescription(updatedJob.getDescription());
                                jobDB.setEnabled(updatedJob.getEnabled());
                                jobDB.setOneTimeRun(updatedJob.getOneTimeRun());
                                jobDB.setTitle(updatedJob.getTitle());
                                jobDB.setKlass("com.smmhub.scheduler.jobs.PostingJob");
                                jobDB.setRetries(0);
                                jobDB.setFinished(false);
                                jobDB.setUpdated(new Date());

                                // если таску выключили
                                if(jobDB.getEnabled() && !updatedJob.getEnabled()) {
                                    jobDB.setEnabled(updatedJob.getEnabled());
                                    schedulerService.checkAndUnscheduleTrigger(jobDB.getId());
                                }

                                // save user id to job data
                                // update next post
                                Set<Long> catIds = jobDB.getCategories()
                                        .stream()
                                        .map(CatalogCategory::getId)
                                        .collect(Collectors.toSet());

                                Set<String> skipPostIds = job.getJobPosts()
                                        .stream()
                                        .map(JobPost::getPostId)
                                        .collect(Collectors.toSet());

                                Post nextPost = postService.findRandomPostInCategories(catIds, skipPostIds);

                                String userId = account.getEmail() + "-" + account.getLoginType().name();
                                JobPostingData data = new JobPostingData(userId, jobDB.getId(), nextPost.getId());
                                jobDB.setData(objectMapper.writeValueAsString(data));

                                jobDB = jobService.saveJob(jobDB);
                                jobId = jobDB.getId();

                                // если таска включена
                                if(jobDB.getEnabled()) {
                                    rescheduleJob(jobDB);
                                }

                                // add new job and refresh cache
                                Long jobDBId = jobDB.getId();
                                group.setJobs(
                                        group.getJobs().stream()
                                        .filter(groupJob -> !Objects.equals(groupJob.getId(), jobDBId))
                                        .collect(Collectors.toSet())
                                );
                                group.getJobs().add(jobDB);
                                accountsCache.put(userId, modelMapper.map(account, AccountDTO.class));

                                updated = true;
                            }
                        }
                    }
                }
            }
        }
        // create new job
        else {
            for (SocialNetwork network : account.getSocialNetworks()) {
                for (SocialNetworkGroup group : network.getSocialNetworkGroups()) {
                    if (Objects.equals(updateJobRequest.getGroupid(), group.getSocialGroupId())) {
                        Job newJob = modelMapper.map(updateJobRequest.getJob(), Job.class);

	                    Set<CatalogCategory> attachedCategories = newJob.getCategories().stream().map(catalogCategory -> {
		                    try {
			                    return catalogService.getCategory(catalogCategory.getId());
		                    } catch (CatalogCategoryNotFoundException e) {
			                    log.error("No category #{} found in database!", catalogCategory.getId());
			                    return null;
		                    }
	                    }).collect(Collectors.toSet());

	                    newJob.setCategories(attachedCategories);
                        newJob.setRetries(0);
                        newJob.setFinished(false);
                        newJob.setUpdated(new Date());
                        newJob.setCreated(new Date());
                        newJob.setKlass("com.smmhub.scheduler.jobs.PostingJob");
                        newJob.setSocialNetworkGroup(group);

                        newJob = jobService.saveJob(newJob);
                        jobId = newJob.getId();

                        // save user id to job data
                        // update next post
                        Set<Long> catIds = newJob.getCategories()
                                .stream()
                                .map(CatalogCategory::getId)
                                .collect(Collectors.toSet());

                        Post nextPost = postService.findRandomPostInCategories(catIds, Collections.emptySet());
                        String userId = account.getEmail() + "-" + account.getLoginType().name();
                        JobPostingData data = new JobPostingData(userId, jobId, nextPost.getId());
                        newJob.setData(objectMapper.writeValueAsString(data));

                        newJob = jobService.saveJob(newJob);

                        // если таска включена
                        if(newJob.getEnabled()) {
                            rescheduleJob(newJob);
                        }

                        // add new job and refresh cache
                        group.getJobs().add(newJob);
                        accountsCache.put(userId, modelMapper.map(account, AccountDTO.class));

                        created = true;
                    }
                }
            }
        }

        if (updated) {
            responseCode = BackendAPIResponseCode.OK_JOB_UPDATED;
        } else if (created){
            responseCode = BackendAPIResponseCode.OK_JOB_CREATED;
        } else {
            responseCode = BackendAPIResponseCode.OK_JOB_NOT_UPDATED;
        }

        return BackendResponse.of(responseCode, "Executed", jobId);
    }

    @PreAuthorize("hasAuthority('REGULAR_USER')")
    @PostMapping("/remove")
    public Object updateJob(Account account, @RequestBody RemoveJobRequest removeJobRequest) throws JobNotFoundException {
        BackendAPIResponseCode responseCode;
        boolean removed = false;

        if (removeJobRequest.getJobid() != null && removeJobRequest.getJobid() != 0L) {
            for (SocialNetwork network : account.getSocialNetworks()) {
                for (SocialNetworkGroup group : network.getSocialNetworkGroups()) {
                    for (Job job : group.getJobs()) {
                        if (Objects.equals(removeJobRequest.getJobid(), job.getId())) {
                            Job jobDB = jobService.getJob(job.getId());
                            jobService.removeJob(jobDB.getTitle());
                            schedulerService.checkAndUnscheduleTrigger(jobDB.getId());
                            removed = true;
                        }
                    }
                }
            }
        }

        if(removed) {
            responseCode = BackendAPIResponseCode.OK_JOB_REMOVED;
        } else {
            responseCode = BackendAPIResponseCode.ERROR_JOB_NOT_FOUND;
        }

        return BackendResponse.of(responseCode, "Executed");
    }

    private void rescheduleJob(Job jobDB) throws ClassNotFoundException, SchedulerException, IOException {
        // unschedule
        schedulerService.checkAndUnscheduleTrigger(jobDB.getId());

        // init with data
        schedulerService.initializeJob(Class.forName(
                jobDB.getKlass()),
                Collections.singletonMap("data", objectMapper.readValue(jobDB.getData(), JobPostingData.class)),
                jobDB.getId());

        // schedule to run
        schedulerService.checkAndScheduleCronExpression(
                jobDB.getId(),
                jobDB.getTitle() + "-trigger",
                jobDB.getCron());

    }
}
