package com.smmhub.web.controllers.v1.accounts;

import com.smmhub.commons.enums.BackendAPIResponseCode;
import com.smmhub.commons.model.dto.account.AccountDTO;
import com.smmhub.commons.model.web.response.BackendResponse;
import com.smmhub.web.controllers.AbstractController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ResponseBody
@RequestMapping("/api/v1/account")
public class AccountController extends AbstractController {

	@PreAuthorize("hasAuthority('REGULAR_USER')")
	@GetMapping("/userinfo")
	public BackendResponse getUser(AccountDTO accountDTO) throws Exception {

		if (accountDTO == null) {
			return BackendResponse.of(BackendAPIResponseCode.ERROR, "Account type is unknown");
		}

		return BackendResponse.of(accountDTO);
	}

}
