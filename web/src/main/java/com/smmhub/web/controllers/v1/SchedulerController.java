package com.smmhub.web.controllers.v1;

import com.smmhub.commons.model.dto.job.JobDTO;
import com.smmhub.commons.model.web.response.BackendResponse;
import com.smmhub.commons.service.RunningService;
import com.smmhub.scheduler.services.JobService;
import com.smmhub.scheduler.services.SchedulerService;
import com.smmhub.web.controllers.AbstractController;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Type;
import java.util.List;

@RestController
@ResponseBody
@Slf4j
@RequestMapping("/api/v1/scheduler")
public class SchedulerController extends AbstractController {

	@Autowired
	@Qualifier("parserManager")
	private RunningService parserManagerService;

	@Autowired
	private SchedulerService schedulerService;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private JobService jobService;

	private final static Type targetListType = new TypeToken<List<JobDTO>>() {}.getType();

	@PreAuthorize("hasAuthority('ADMIN')")
	@ResponseBody
	@GetMapping("/reload")
	public Object reload(HttpServletRequest request, HttpServletResponse response) throws Exception {

		parserManagerService.onShutdown();
		parserManagerService.onStartup();

		return BackendResponse.of("Rescheduled");
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@ResponseBody
	@GetMapping("/jobs")
	public Object getAllJobs(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return BackendResponse.of(modelMapper.map(jobService.getAllJobs(), targetListType));
	}
}
