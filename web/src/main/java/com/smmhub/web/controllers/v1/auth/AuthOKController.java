package com.smmhub.web.controllers.v1.auth;

import com.smmhub.commons.enums.AccountLoginType;
import com.smmhub.commons.enums.BackendAPIResponseCode;
import com.smmhub.commons.exceptions.AccountIdentifierRequiredException;
import com.smmhub.commons.exceptions.AccountNotFoundException;
import com.smmhub.commons.model.datasources.SocialProfile;
import com.smmhub.commons.model.web.request.account.OAuth2CodeRequest;
import com.smmhub.commons.model.web.response.BackendResponse;
import com.smmhub.commons.model.web.response.OAuth2ParamsResponse;
import com.smmhub.commons.model.web.response.TokenResponse;
import com.smmhub.database.model.sql.accounts.types.OkAccount;
import com.smmhub.datasources.SocialAPI;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mobile.device.Device;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/api/v1/auth/ok")
public class AuthOKController extends AbstractAuthController {

	@Autowired
	@Qualifier("ok")
	private SocialAPI okAPI;

	@GetMapping("/login")
	public BackendResponse okRegistration() throws Exception {

		OAuth2ParamsResponse response = new OAuth2ParamsResponse(
				socialCredentialsConsts.okLoginAppID,
				socialCredentialsConsts.okLoginCallback
		);

		return BackendResponse.of(response);
	}

	@PostMapping("/callback")
	@ResponseBody
	public BackendResponse okRegistrationCallback(
			@RequestBody OAuth2CodeRequest codeRequest, Device device) throws Exception {

		okAPI.init(null, codeRequest.getCode());

		SocialProfile profile = okAPI.getProfileInfo();

		if (StringUtils.isEmpty(profile.getEmail())) {
			return BackendResponse.of(BackendAPIResponseCode.EMAIL_NOT_SPECIFIED, "EMail not specified");
		}

		OkAccount account;
		try {
			account = (OkAccount) accountService.getAccount(profile.getEmail(), AccountLoginType.OK);
		} catch (AccountIdentifierRequiredException e) {
			log.error("No account identifier supplied!");
			return BackendResponse.of(BackendAPIResponseCode.ERROR, "No account identifier supplied!");
		} catch (AccountNotFoundException e) {
			account = new OkAccount();
		}

		account.setToken(profile.getAccessToken());
		account.setEmail(profile.getEmail());
		account.setUserId(Long.valueOf(profile.getUserId()));

		// новый юзер
		if (account.getId() == null || account.getId() == 0L) {
			account.setBirthday(profile.getBirthday());
			account.setDisplayName(profile.getDisplayName());
			account.setPhotoUrl(profile.getPhotoUrl());
			account.setPassword(AccountLoginType.OK.name());
			account = (OkAccount) accountService.addAccount(account);
		} else {
			account = (OkAccount) accountService.saveAccount(account);
		}

		String token = securityService.autologin(
				accountService.getOrUpdateAccountInCache(account),
				AccountLoginType.OK.name(),
				device);

		return BackendResponse.of(new TokenResponse(token));
	}
}
