package com.smmhub.web.services;

import com.smmhub.commons.model.dto.account.AccountDTO;
import com.smmhub.services.TokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DevicePlatform;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

/**
 * @author Nikolay Viguro at 29.03.17
 */

@Service
@Slf4j
public class SecurityServiceImpl implements SecurityService {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private TokenUtils tokenUtilsImpl;

	@Override
	public String findLoggedInUsername() {
		Object userDetails = SecurityContextHolder.getContext().getAuthentication().getDetails();
		if (userDetails instanceof UserDetails) {
			return ((UserDetails)userDetails).getUsername();
		}

		return null;
	}

	@Override
	public String autologin(String username, String password) {
		UserDetails userDetails = userDetailsService.loadUserByUsername(username);
		UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());

		authenticationManager.authenticate(usernamePasswordAuthenticationToken);

		if (usernamePasswordAuthenticationToken.isAuthenticated()) {
			SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
			log.debug(String.format("Auto login %s successfully!", username));
		}
		else {
			log.error("Auto login %s failed!", username);
		}

		Device device = new Device() {
			@Override
			public boolean isNormal() {
				return true;
			}

			@Override
			public boolean isMobile() {
				return false;
			}

			@Override
			public boolean isTablet() {
				return false;
			}

			@Override
			public DevicePlatform getDevicePlatform() {
				return DevicePlatform.UNKNOWN;
			}
		};

		// Reload password post-authentication so we can generate token
		return tokenUtilsImpl.generateToken(userDetails, device);
	}

	@Override
	public String autologin(AccountDTO account, String pass, Device device) {
		// Perform the authentication
		Authentication authentication = this.authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(
						account.getEmail() + "-" + account.getLoginType().name(),
						pass
				)
		);
		SecurityContextHolder.getContext().setAuthentication(authentication);

		// Reload password post-authentication so we can generate token
		AccountDTO userDetails = (AccountDTO) userDetailsService.loadUserByUsername(account.getEmail() + "-" + account.getLoginType().name());
		return tokenUtilsImpl.generateToken(userDetails, device);
	}
}
