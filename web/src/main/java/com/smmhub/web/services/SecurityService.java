package com.smmhub.web.services;

import com.smmhub.commons.model.dto.account.AccountDTO;
import org.springframework.mobile.device.Device;

/**
 * @author Nikolay Viguro at 29.03.17
 */
public interface SecurityService {
	String findLoggedInUsername();
	String autologin(String username, String password);
    String autologin(AccountDTO account, String pass, Device device);
}
